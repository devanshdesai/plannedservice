﻿using Newtonsoft.Json;
using PlannedServices.Common;
using PlannedServices.Common.Paging;
using PlannedServices.Entities.Contract;
using PlannedServices.Entities.V1;
using PlannedServices.Services.Contract;
using PlannedServicesProviderPortal.Pages;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using static PlannedServicesProviderPortal.Infrastructure.Enums;

namespace PlannedServicesProviderPortal.Controllers
{
    public class AccountController : Controller
    {
        #region Fields
        private readonly AbstractProviderServices abstractProviderServices;
        private readonly AbstractLookupStateServices abstractLookupStateServices;

        #endregion

        #region Ctor
        public AccountController(AbstractProviderServices abstractProviderServices,
            AbstractLookupStateServices abstractLookupStateServices)
        {
            this.abstractProviderServices = abstractProviderServices;
            this.abstractLookupStateServices = abstractLookupStateServices;
        }
        #endregion


        #region Methods
        // GET: Account
        public ActionResult Index()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            return View();
        }

        [HttpGet]
        [ActionName(Actions.LogIn)]
        public ActionResult Login(string stremail = "")
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];

            Provider abstractProvider = new Provider();
            if (stremail != null)
            {
                string decryptedEmail = Convert.ToString(ConvertTo.Base64Decode(stremail));
                if (decryptedEmail != null && decryptedEmail != "")
                {
                    abstractProvider.Email = decryptedEmail;
                    var modal = abstractProviderServices.Provider_Active(decryptedEmail);
                }
            }
            Session.Clear();
            Session.Abandon();

            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();

            if (Request.Cookies["ProviderLogin"] != null)
            {
                var c = new HttpCookie("ProviderLogin");
                c.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(c);
            }
            return View(abstractProvider);
        }

        [HttpGet]
        [ActionName(Actions.Home)]
        public ActionResult Home()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            return View();
        }

        [HttpGet]
        [ActionName(Actions.SignUp)]
        public ActionResult SingUp()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            ViewBag.States = BindStateDropdown();
            return View();
        }

        public ActionResult AccountSetUp()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            return View();
        }

        public ActionResult ResetPassword(string stremail = "")
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];

            Provider abstractProvider = new Provider();
            if (stremail != null)
            {
                string decryptedEmail = Convert.ToString(ConvertTo.Base64Decode(stremail));
                if (decryptedEmail != null && decryptedEmail != "")
                {
                    abstractProvider.Email = decryptedEmail;
                    abstractProvider.CompanyEmail = decryptedEmail;
                    ViewBag.userId = decryptedEmail;
                }
            }
            return View(abstractProvider);
        }

        [HttpGet]
        [ActionName(Actions.ForgotPassword)]
        public ActionResult ForgotPassword()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];

            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.SendEmail)]
        public ActionResult SendEmail(Provider provider)
        {
            SuccessResult<AbstractProvider> result1 = abstractProviderServices.Provider_ByEmail(provider.Email, 1);
            if (result1 != null && result1.Code == 200 && result1.Item != null)
            {
                var sendGridClient = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"]);

                var sendGridMessage = new SendGridMessage();
                sendGridMessage.SetFrom(ConfigurationManager.AppSettings["SendGridFromEmail"], "My Request");
                sendGridMessage.AddTo(provider.Email, "user test"); // user email
                //sendGridMessage.AddTo("parthdobariya09@gmail.com", "user test"); // user email
                sendGridMessage.SetTemplateId("d-792766f0166e4c75924feee836dc5aac");

                sendGridMessage.SetTemplateData(new EmailObj
                {
                    UName = result1.Item.CompanyName != null ? result1.Item.CompanyName : result1.Item.Name + ' ' + result1.Item.LastName,
                    email = ConvertTo.Base64Encode(Convert.ToString(provider.Email)),

                });

                var response = sendGridClient.SendEmailAsync(sendGridMessage).Result;
                var result = response;
                if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), "Please check your email we send you reset password link.!");
                }
            }

            return RedirectToAction(Actions.ForgotPassword, Pages.Controllers.Account);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.LogIn)]
        public ActionResult LogIn(Provider objmodel)
        {
            SuccessResult<AbstractProvider> userData = abstractProviderServices.Provider_Login(objmodel);
            if (userData != null && userData.Code == 200 && userData.Item != null)
            {
                Session.Clear();
                if (userData.Item.UserType == 1)
                {
                   
                    PageParam pageParam = new PageParam();
                    pageParam.Offset = 0;
                    pageParam.Limit = 10000;
                    var parent = abstractProviderServices.CheckSubProvider(pageParam, ConvertTo.Integer(userData.Item.CreatedBy)).Values;
                    if (parent.Count == 1)
                    {
                        ProjectSession.ProviderId = userData.Item.Id;
                        ProjectSession.LoginUserName = userData.Item.Name + " " + userData.Item.LastName;
                        ProjectSession.ParentId = ConvertTo.Integer(userData.Item.CreatedBy);
                        ProjectSession.ProviderName = parent[0].Name + " " + parent[0].LastName;
                        ProjectSession.Email = parent[0].Email;
                        ProjectSession.SetupUserType = Convert.ToString(parent[0].UserType);
                    }
                    else
                    {
                        ProjectSession.ProviderId = userData.Item.Id;
                        ProjectSession.ParentId = userData.Item.Id;
                        ProjectSession.Email = userData.Item.Email;
                        ProjectSession.ProviderName = userData.Item.Name + " " + userData.Item.LastName;
                    }
                }
                else
                {
                    ProjectSession.ParentId = userData.Item.Id;
                    ProjectSession.LoginUserName = userData.Item.Name + " " + userData.Item.LastName;
                    ProjectSession.ProviderId = userData.Item.Id;
                    ProjectSession.ProviderName = userData.Item.Name + " " + userData.Item.LastName;
                    ProjectSession.Email = userData.Item.Email;
                }

              HttpCookie cookie = new HttpCookie("ProviderLogin");
                cookie.Values.Add("Id", userData.Item.Id.ToString());
                cookie.Values.Add("UserName", ProjectSession.ProviderName);
                cookie.Values.Add("Email", ProjectSession.Email);
                cookie.Expires = DateTime.Now.AddHours(1);

                Response.Cookies.Add(cookie);
                return RedirectToAction(Actions.Index, Pages.Controllers.Dashboard);
            }
            else
            {
                ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), userData.Message);
            }
            return View();
        }

        [AllowAnonymous]
        [ActionName(Actions.Logout)]
        public ActionResult Logout()
        {
            Session.Clear();
            Session.Abandon();

            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
            return RedirectToAction(Actions.LogIn, PlannedServicesProviderPortal.Pages.Controllers.Account);
        }


        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.UserSignUp)]
        public ActionResult UserSignUp(Provider provider)
        {
            try
            {
                provider.UserType = 1;
                HttpClient client = new HttpClient();

                var stringTask = client.GetStringAsync("https://maps.googleapis.com/maps/api/geocode/json?address=" + provider.AddressLine1 + ",Aus&key=AIzaSyB97EhOhMCYhK1tZIofUkpKcGW61fdgKjg");
                var msg = stringTask;

                var s = JsonConvert.DeserializeObject<Root>(msg.Result);
                if (s.results.Count > 0)
                {
                    provider.Lat = ConvertTo.String(s.results[0].geometry.location.lat);
                    provider.Long = ConvertTo.String(s.results[0].geometry.location.lng);
                }
                SuccessResult<AbstractProvider> result = abstractProviderServices.Provider_Upsert(provider);
                if (result.Item != null && result.Code == 200)
                {
                    ProjectSession.ProviderId = result.Item.Id;
                    ProjectSession.ParentId = result.Item.ParentId;
                    ProjectSession.ProviderName = result.Item.Name;
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), result.Message);
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                    return RedirectToAction(Actions.SignUp, Pages.Controllers.Account);
                }
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "");
            }
            return RedirectToAction(Actions.Index, Pages.Controllers.Dashboard);
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.SetPassword)]
        public ActionResult SetPassword(Provider provider)
        {
            int Id = 0;
            try
            {
                provider.Email = provider.CompanyEmail;
                SuccessResult<AbstractProvider> result = abstractProviderServices.Provider_RestPassword(provider);
                if (result != null && result.Code == 200 && result.Item != null)
                {
                    //Id = result.Item.Id;
                    SuccessResult<AbstractProvider> userData = abstractProviderServices.Provider_Login(provider);
                    if (userData != null && userData.Code == 200 && userData.Item != null)
                    {
                        Session.Clear();
                        ProjectSession.ProviderId = userData.Item.Id;
                        ProjectSession.ProviderName = userData.Item.Name;
                        ProjectSession.ParentId = userData.Item.Id;
                        //ProjectSession.ParentId = userData.Item.ParentId;
                        HttpCookie cookie = new HttpCookie("ProviderLogin");
                        cookie.Values.Add("UserName", provider.Id.ToString());
                        cookie.Values.Add("Password", provider.Id.ToString());
                        cookie.Expires = DateTime.Now.AddHours(1);

                        Response.Cookies.Add(cookie);
                        return RedirectToAction(Actions.Index, Pages.Controllers.Dashboard);
                    }
                    else
                    {
                        ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), userData.Message);
                    }
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "");
            }
            return RedirectToAction(Actions.ResetPassword, Pages.Controllers.User, new { Area = "", strEmail = ConvertTo.Base64Encode(Convert.ToString(provider.Email)) });
        }
        public IList<SelectListItem> BindStateDropdown()
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 100000;
            var model = abstractLookupStateServices.LookupState_All(pageParam, "").Values;

            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in model)
            {
                items.Add(new SelectListItem() { Text = item.Name.ToString(), Value = item.Id.ToString() });
            }

            return items;
        }

        private class EmailObj
        {
            [JsonProperty("username")]
            public string username { get; set; }

            [JsonProperty("email")]
            public string email { get; set; }

            [JsonProperty("UName")]
            public string UName { get; set; }

            [JsonProperty("PName")]
            public string PName { get; set; }

            [JsonProperty("password")]
            public string password { get; set; }
        }

        #endregion
    }

    public class AddressComponent
    {
        public string long_name { get; set; }
        public string short_name { get; set; }
        public List<string> types { get; set; }
    }

    public class Northeast
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Southwest
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Bounds
    {
        public Northeast northeast { get; set; }
        public Southwest southwest { get; set; }
    }

    public class Location
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Viewport
    {
        public Northeast northeast { get; set; }
        public Southwest southwest { get; set; }
    }

    public class Geometry
    {
        public Bounds bounds { get; set; }
        public Location location { get; set; }
        public string location_type { get; set; }
        public Viewport viewport { get; set; }
    }

    public class Result
    {
        public List<AddressComponent> address_components { get; set; }
        public string formatted_address { get; set; }
        public Geometry geometry { get; set; }
        public string place_id { get; set; }
        public List<string> types { get; set; }
    }

    public class Root
    {
        public List<Result> results { get; set; }
        public string status { get; set; }
    }
}