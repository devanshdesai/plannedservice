﻿using DataTables.Mvc;
using PlannedServices.Common.Paging;
using PlannedServices.Services.Contract;
using PlannedServicesAdmin.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PlannedServicesAdmin.Infrastructure;
using PlannedServices.Entities.Contract;
using PlannedServices.Common;
using PlannedServices.Entities.V1;

namespace PlannedServicesAdmin.Controllers
{
    public class PurpleLeopardController : Controller
    {
        #region Fields
        private readonly AbstractUserServices abstractUserServices;
        private readonly AbstractServicesServices abstractServices;
        private readonly AbstractLookupStateServices abstractLookupStateServices;
        #endregion

        public PurpleLeopardController(AbstractUserServices abstractUserServices,
            AbstractLookupStateServices abstractLookupStateServices, AbstractServicesServices abstractServices)
        {
            this.abstractUserServices = abstractUserServices;
            this.abstractLookupStateServices = abstractLookupStateServices;
            this.abstractServices = abstractServices;
        }

        public ActionResult SignUp()
        {
           
            ViewBag.ErrorMSG = TempData["errorMSG"];
            AbstractUser abstractUser = new User();

            ViewBag.PlanManagerId = 1108;
            abstractUser.PlanManagerId = 1108;
            var planM = abstractUserServices.PlanManager_ById(abstractUser.PlanManagerId, 0).Item;
            ViewBag.ImageUrl = planM.CompanyLogoURL;

            ViewBag.userId = 0;
            ViewBag.States = BindStateDropdown();

            return View(abstractUser);
        }

        public ActionResult SignUpStep2()
        {
            //if (TempData["openPopup"] != null)
            //    ViewBag.openPopup = TempData["openPopup"];

            ViewBag.ErrorMSG = TempData["errorMSG"];
            List<SelectListItem> items = new List<SelectListItem>();
            ViewBag.States = items;
            ViewBag.Citys = items;
            ViewBag.BStates = items;
            ViewBag.BCitys = items;
            AbstractUser abstractUser = new User();
            ViewBag.PlanManagerId = 1108;
            abstractUser.PlanManagerId = 1108;
            var planM2 = abstractUserServices.PlanManager_ById(abstractUser.PlanManagerId, 0).Item;
            ViewBag.ImageUrl = planM2.CompanyLogoURL;

            if (ProjectSession.SetupUserID > 0)
            {
                int decryptedId = ProjectSession.SetupUserID;
                if (decryptedId > 0)
                {
                    abstractUser = abstractUserServices.User_ById(decryptedId).Item;
                    var planM = abstractUserServices.PlanManager_ById(abstractUser.PlanManagerId, 0).Item;
                    abstractUser.PlanManagerName = planM.CompanyName;
                    abstractUser.PlanManagerAddress = planM.AddressLine1;
                    abstractUser.CompanyLogoURL = planM.CompanyLogoURL;
                }
            }
            return View(abstractUser);
        }

        public IList<SelectListItem> BindStateDropdown()
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 100000;
            var model = abstractLookupStateServices.LookupState_All(pageParam, "").Values;

            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in model)
            {
                items.Add(new SelectListItem() { Text = item.Name.ToString(), Value = item.Id.ToString() });
            }

            return items;
        }


    }
}