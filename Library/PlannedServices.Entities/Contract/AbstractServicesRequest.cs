﻿using PlannedServices.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlannedServices.Entities.Contract
{
    public abstract class AbstractServicesRequest
    {
        public int Id { get; set; }
        public string DrpId { get; set; }

        public string Name { get; set; }
        public string ProviderName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string ProviderEmail { get; set; }
        public int UserId { get; set; }
        public int CategoryId { get; set; }
        public int BudgetId { get; set; }
        public int RegistrationGroupId { get; set; }
        public int ServiceRequestId { get; set; }
        public int SupportItemId { get; set; }
        public int SIMId { get; set; }
        public int CustomerId { get; set; }
        public int ServiceRequestSupportItemProviderId { get; set; }
        public int ServiceRequestChildId { get; set; }
        public int ServiceChildId { get; set; }
        public int SRequestId { get; set; }
        public string SupportItemIds { get; set; }
        public string ProviderIds { get; set; }
        public int ProviderId { get; set; }
        public string Distance { get; set; }
        public string Comment { get; set; }
        public string SComment { get; set; }
        public string Message { get; set; }
        public string CategoryName { get; set; }
        public string RegistrationGroupName { get; set; }
        public string Surburb { get; set; }
        public string StateName { get; set; }
        public string PinCode { get; set; }
        public int Status { get; set; }
        public int MainStatus { get; set; }
        public int PServiceStatus { get; set; }
        public int TotalCount { get; set; }
        public int NewRequestsCount { get; set; }
        public int ActiveCount { get; set; }
        public int ApporveCount { get; set; }
        public int CompletedCount { get; set; }
        public int RejectedCount { get; set; }
        public int PRejectedCount { get; set; }
        public int PActiveCount { get; set; }
        public int PApporveCount { get; set; }
        public int RequestSendCount { get; set; }
        public int RepliesCount { get; set; }
        public int IsNew { get; set; }
        public int IsRead { get; set; }
        public int IsApprovedRead { get; set; }
        public int IsRejectedRead { get; set; }
        public int IsProviderRead { get; set; }
        public string TextStatus { get; set; }
        public string Description { get; set; }
        public string BookingRequestedDate { get; set; }
        public DateTime AcceptDate { get; set; }
        public DateTime CancelDate { get; set; }
        public string Quote { get; set; }
        public string Unit { get; set; }
        public string FileURL { get; set; }
        public string FileName { get; set; }
        public string ReasonId { get; set; }
        public string ContactName { get; set; }
        public double Rate { get; set; }
        public int ProviderCount { get; set; }
        public bool IsProvider { get; set; }
        public string PCName { get; set; }
        public string PCNumber { get; set; }
        public string PCEmail { get; set; }
        public int IsProviderContact { get; set; }
        public int IsHideContactDetails { get; set; }
        public int IsCapacity { get; set; }
        public List<AbstractProvider> Provider { get; set; }
        public DateTime CreatedDate { get; set; }

        public int CreatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }
        public DateTime ReadDate { get; set; }
        public DateTime MessageDate { get; set; }
        public DateTime ProviderRead { get; set; }
        public DateTime UserRead { get; set; }

        public int UpdatedBy { get; set; }

        public String CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd MMM yyyy") : "-";
        public String CDateStr => CreatedDate != null ? CreatedDate.ToString("hh:mm") : "-";
        public String ReadDateStr => ReadDate != null ? ReadDate.ToString("dd MMM yyyy") : "-";
        public String MessageDateStr => MessageDate != null ? MessageDate.ToString("dd MMM yyyy") : "-";
        public String ProviderReadStr => ProviderRead != null ? ProviderRead.ToString("dd MMM yyyy") : "-";
        public String UserReadStr => UserRead != null ? UserRead.ToString("dd MMM yyyy") : "-";

        public String UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd MMM yyyy") : "-";
        public String AcceptDateStr => AcceptDate != null ? AcceptDate.ToString("dd MMM yyyy hh:mm") : "-";
        public String CancelDateStr => CancelDate != null ? CancelDate.ToString("dd MMM yyyy") : "-";

        public string S3ImageURL { get; set; }
        public string ImageUrl { get; set; }

        [NotMapped]
        public string AvatarFolder { get; set; }
        [NotMapped]
        public string DocumentPath { get; set; }
        [NotMapped]
        public string Path { get; set; }
        public string ServiceNumber { get; set; }

        public AbstractProvider SelectProvider { get; set; }
        public AbstractUser SelectUser { get; set; }
        public AbstractPlanManager SelectPlanManager { get; set; }
        public Services serviceObj { get; set; }
        public List<AbstractProvider> Providers { get; set; }
        public List<AbstractServicesRequest> ServicesRequests { get; set; }
        public List<AbstractServicesRequest> ServicesRequestCommunication { get; set; }
    }
}
