﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PlannedServices.Common;
using PlannedServices.Entities.V1;
using PlannedServices.Services.Contract;
using PlannedServicesAdmin.Infrastructure;

namespace PlannedServicesAdmin.Controllers
{
    public class DashboardController : BaseController
    {
        #region Fields
        private readonly AbstractServicesRequestServices abstractServicesRequest;
        private readonly AbstractUserServices abstractUserServices;


        #endregion

        #region Ctor
        public DashboardController(
            AbstractServicesRequestServices abstractServicesRequest,
            AbstractUserServices abstractUserServices
            )
        {
            this.abstractServicesRequest = abstractServicesRequest;
            this.abstractUserServices = abstractUserServices;
        }
        #endregion
        // GET: Dashboard
        public ActionResult Index(string userId = "")
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];

            //if (userId == "")
            //{
            //    var model = abstractServicesRequest.GetParticipantsChartData(ProjectSession.UserID);
            //    var model1 = abstractUserServices.User_ById(ProjectSession.UserID);
            //    ProjectSession.SetupUserType = ConvertTo.String(model1.Item.ManagerType);
            //    ViewBag.CData = model.Values;
            //    ViewBag.UserName = model1.Item.FirstName + " " + model1.Item.LastName;
            //}
            //else
            //{
            //    int decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(userId));
            //    var model = abstractServicesRequest.GetParticipantsChartData(decryptedId);
            //    var model1 = abstractUserServices.User_ById(decryptedId);
            //    //ProjectSession.SetupUserType = ConvertTo.String(model1.Item.ManagerType);
            //    ProjectSession.ManagaerUserId = decryptedId;
            //    ProjectSession.UserID = decryptedId;
            //    ViewBag.CData = model.Values;
            //    ViewBag.UserName = model1.Item.FirstName +" "+ model1.Item.LastName;
            //}
            //return View();


            ServicesRequest servicesRequest = new ServicesRequest();

            servicesRequest.ServicesRequests = abstractServicesRequest.ServiceRequests_AllByCustomerId(ProjectSession.UserID).Values;
            if (servicesRequest.ServicesRequests.Count > 0)
            {
                ViewBag.ACount = servicesRequest.ServicesRequests[0].ActiveCount.ToString("00");
                ViewBag.CCount = servicesRequest.ServicesRequests[0].CompletedCount.ToString("00");
            }
            else
            {
                ViewBag.ACount = "00";
                ViewBag.CCount = "00";
            }
            return View(servicesRequest);
        }
        
        // GET: Dashboard
        public ActionResult ManagerDashboard()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];

           var model1 = abstractUserServices.User_ById(ProjectSession.LoginUserID);
           var model = abstractUserServices.ManagerUser_byUserId(ProjectSession.LoginUserID).Values;
            ProjectSession.SetupUserType = ConvertTo.String(3);
            ViewBag.ManagerUsers = model;
            return View();
        }
        public ActionResult Help()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            return View();
        }
    }
}