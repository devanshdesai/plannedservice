﻿using PlannedServices.Common;
using PlannedServices.Common.Paging;
using PlannedServices.Entities.Contract;
using PlannedServices.Entities.V1;
using PlannedServices.Services.Contract;
using PlannedServicesAdmin.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static PlannedServicesAdmin.Infrastructure.Enums;
using PlannedServicesAdmin.Infrastructure;
using System.Configuration;
using SendGrid;
using SendGrid.Helpers.Mail;
using Newtonsoft.Json;

namespace PlannedServicesAdmin.Controllers
{
    public class ServiceRequestController : BaseController
    {
        private readonly AbstractServicesServices abstractServices;
        private readonly AbstractProviderServices abstractProviderServices;
        private readonly AbstractServicesRequestServices abstractServicesRequest;
        public ServiceRequestController(AbstractServicesServices abstractServices,
            AbstractProviderServices abstractProviderServices, AbstractServicesRequestServices abstractServicesRequest)
        {
            this.abstractServices = abstractServices;
            this.abstractProviderServices = abstractProviderServices;
            this.abstractServicesRequest = abstractServicesRequest;
        }

        // GET: ServiceRequest
        public ActionResult Index()
        {
            Services services = new Services();
            //if (TempData["ServiceData"] != null)
            //{
            //    ViewBag.Provider = TempData["ServiceData"];
            //    services = ViewBag.Provider;
            //}
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];

            List<SelectListItem> items = new List<SelectListItem>();
            ViewBag.RegistrationGroup = BindRegistrationGroupDropdown();
            ViewBag.SupportCategoryMaster = items;
            ViewBag.SupportItem = items;
            ViewBag.Distance = BindDistanceDropdown();
            return View(services);
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.Search)]
        public ActionResult Search(Services model)
        {
           
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 10;
            model.UserId = ProjectSession.UserID;
            //PagedList<AbstractProvider> result = abstractProviderServices.Provider_All(pageParam, "", 0, "", "", "");
            SuccessResult<AbstractServices> result2 = abstractServices.ServiceRequest_Upsert(model);
            if (result2 != null && result2.Code == 200 && result2.Item != null)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), "Your request has been sent to 32 Providers");
                var sendGridClient = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"]);

                var sendGridMessage = new SendGridMessage();
                sendGridMessage.SetFrom(ConfigurationManager.AppSettings["SendGridFromEmail"], "rushkar");
                sendGridMessage.AddTo("parth@rushkar.com", "user test"); // user email
                sendGridMessage.SetTemplateId("d-54877013eb81443094b3901d66f8e67a");

                var response = sendGridClient.SendEmailAsync(sendGridMessage).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
                {

                }
            }
            else
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result2.Message);
            }
            //Services s = new Services();
            ///s.Provider = result.Values;
            // s.ServiceRequestId = result2.Item.Id;
            // TempData["ServiceData"] = s;
            return RedirectToAction(Actions.Index, Pages.Controllers.ServiceRequest);
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.SendRequest)]
        public ActionResult SendRequest(Services model)
        {
            //PageParam pageParam = new PageParam();
            //pageParam.Offset = 0;
            //pageParam.Limit = 10;

            //PagedList<AbstractProvider> result = abstractProviderServices.Provider_All(pageParam, "", 0, "", "", "");

            //Services services = new Services();
            //services.Provider = result.Values;
            //ViewBag.Provider = result.Values;
            //TempData["Provider"] = result.Values;

            SuccessResult<AbstractServices> result = abstractServices.ServiceRequestChild_Upsert(model);

           
            TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), result.Message);
            return RedirectToAction(Actions.Index, Pages.Controllers.Dashboard);
        }

        //[HttpGet]
        //[ActionName(Actions.RegistrationGroupByCategoryId)]
        //public ActionResult RegistrationGroupByCategoryId(int Type, int RId, int CId)
        //{
        //    PageParam pageParam = new PageParam();
        //    pageParam.Offset = 0;
        //    pageParam.Limit = 100000;
        //    List<SelectListItem> items = new List<SelectListItem>();
        //    if (Type == 0)
        //    {
        //        var model = abstractServices.SupportCategoryMasterByRegistrationGroupId(pageParam, "", RId);
        //        foreach (var item in model.Values)
        //        {
        //            items.Add(new SelectListItem() { Text = item.Name.ToString(), Value = item.Id.ToString() });
        //        }
        //    }
        //    else
        //    {
        //        var model = abstractServices.SupportItemMasterByCategoryName(pageParam, "", RId,CId);
        //        foreach (var item in model.Values)
        //        {
        //            items.Add(new SelectListItem() { Text = item.Name.ToString(), Value = item.Id.ToString() });
        //        }
        //    }
           
        //    return Json(items, JsonRequestBehavior.AllowGet);
        //}

        public IList<SelectListItem> BindRegistrationGroupDropdown()
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 100000;
            var model = abstractServices.RegistrationGroupMaster_All(pageParam, "","").Values;

            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in model)
            {
                items.Add(new SelectListItem() { Text = item.Name.ToString(), Value = item.Id.ToString() });
            }

            return items;
        }


        public IList<SelectListItem> BindDistanceDropdown()
        {
           
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "5km", Value = "5km" });
            items.Add(new SelectListItem() { Text = "10km", Value = "10km" });
            items.Add(new SelectListItem() { Text = "15km", Value = "15km" });
            items.Add(new SelectListItem() { Text = "20km", Value = "20km" });

            return items;
        }

        public ActionResult ViewServiceRequest(string ServiceId = "",string ProviderId = "")
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            ViewBag.userId = 0;
            if (ServiceId != null)
            {
                int decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(ServiceId));
                int decryptedId1 = Convert.ToInt32(ConvertTo.Base64Decode(ProviderId));
                if (decryptedId > 0)
                {
                    PageParam pageParam = new PageParam();
                    pageParam.Offset = 0;
                    pageParam.Limit = 1000000;
                    var model = this.abstractServicesRequest.ServicesRequest_ById(pageParam, "", decryptedId, decryptedId1).Values;
                    var model2 = this.abstractServicesRequest.Communication_AllByProviderId(pageParam, "", 0, decryptedId1, model[0].ServiceRequestChildId).Values;
                    ViewBag.ServiceRequestId = decryptedId;
                    ViewBag.ProviderId = decryptedId1;
                    ViewBag.Status = model[0].Status;
                    ViewBag.ServiceRequestChildId = model[0].ServiceRequestChildId;
                    ViewBag.ServiceData = model;
                    ViewBag.CommunicationData = model2;

                }
            }
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.AddCommunication)]
        public ActionResult AddCommunication(ServicesRequest servicesRequest, IEnumerable<HttpPostedFileBase> images)
        {
            servicesRequest.CustomerId = ProjectSession.UserID;
            //var result = abstractServicesRequest.Communication_Upsert(servicesRequest.ServiceRequestId, ProjectSession.ProviderId, 0, servicesRequest.ServiceRequestChildId, servicesRequest.Message);
            var result = abstractServicesRequest.Communication_Upsert(servicesRequest,images);
            if (result != null && result.Code == 200)
            {
                var sendGridClient = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"]);

                var sendGridMessage = new SendGridMessage();
                sendGridMessage.SetFrom(ConfigurationManager.AppSettings["SendGridFromEmail"], "rushkar");
                sendGridMessage.AddTo("parth@rushkar.com", "user test"); // user email
                sendGridMessage.SetTemplateId("d-a0ea4e6f63684fafb5f980d3af212aa5");

                sendGridMessage.SetTemplateData(new EmailObj
                {
                    name = result.Item.Name,
                    message = result.Item.Message

                });

                var response = sendGridClient.SendEmailAsync(sendGridMessage).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
                {

                }
            }
            return RedirectToAction("ViewServiceRequest", new { ServiceId = ConvertTo.Base64Encode(Convert.ToString(servicesRequest.SRequestId)), ProviderId = ConvertTo.Base64Encode(Convert.ToString(servicesRequest.ProviderId)) });
            // Json(result, JsonRequestBehavior.AllowGet);
        }

        private class EmailObj
        {
            [JsonProperty("name")]
            public string name { get; set; }

            [JsonProperty("status")]
            public string status { get; set; }

            [JsonProperty("message")]
            public string message { get; set; }
        }
    }
}