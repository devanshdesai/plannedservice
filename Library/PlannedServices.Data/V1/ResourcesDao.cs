﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlannedServices.Common;
using PlannedServices.Common.Paging;
using PlannedServices.Data.Contract;
using PlannedServices.Entities.Contract;
using PlannedServices.Entities.V1;
using Dapper;

namespace PlannedServices.Data.V1
{
    public class ResourcesDao : AbstractResourcesDao
    {

        public override SuccessResult<AbstractResources> Resources_Upsert(AbstractResources abstractResources)
        {
            SuccessResult<AbstractResources> Resources = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractResources.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", abstractResources.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractResources.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractResources.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Resources_Upsert, param, commandType: CommandType.StoredProcedure);
                Resources = task.Read<SuccessResult<AbstractResources>>().SingleOrDefault();
                Resources.Item = task.Read<Resources>().SingleOrDefault();
            }

            return Resources;
        }

        public override SuccessResult<AbstractResources> Resources_ById(long Id)
        {
            SuccessResult<AbstractResources> Resources = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Resources_ById, param, commandType: CommandType.StoredProcedure);
                Resources = task.Read<SuccessResult<AbstractResources>>().SingleOrDefault();
                Resources.Item = task.Read<Resources>().SingleOrDefault();
            }

            return Resources;
        }

        public override PagedList<AbstractResources> Resources_All(PageParam pageParam, string search)
        {
            PagedList<AbstractResources> Resources = new PagedList<AbstractResources>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Resources_All, param, commandType: CommandType.StoredProcedure);
                Resources.Values.AddRange(task.Read<Resources>());
                Resources.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Resources;
        }




    }
}
