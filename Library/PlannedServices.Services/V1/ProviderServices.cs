﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using PlannedServices.Common;
using PlannedServices.Common.Paging;
using PlannedServices.Data.Contract;
using PlannedServices.Entities.Contract;
using PlannedServices.Entities.V1;
using PlannedServices.Services.Contract;

   namespace PlannedServices.Services.V1
   {
       public class ProviderServices : AbstractProviderServices
       {
           private AbstractProviderDao abstractProviderDao;

           public ProviderServices(AbstractProviderDao abstractProviderDao)
           {
               this.abstractProviderDao = abstractProviderDao;
           }

           public override SuccessResult<AbstractProvider> Provider_Upsert(AbstractProvider abstractProvider)
           {
               return this.abstractProviderDao.Provider_Upsert(abstractProvider);
           }
           public override SuccessResult<AbstractProvider> ChangeProviderLocation(AbstractProvider abstractProvider)
           {
               return this.abstractProviderDao.ChangeProviderLocation(abstractProvider);
           }
        public override SuccessResult<AbstractProvider> ProviderService_Upsert(string Ids, int PId, int Type)
        {
            return this.abstractProviderDao.ProviderService_Upsert(Ids,PId,Type);
        }
        public override SuccessResult<AbstractProvider> ProviderServiceArea_Upsert(int Type, string value, int PId)
        {
            return this.abstractProviderDao.ProviderServiceArea_Upsert(Type,value,PId);
        }
        public override SuccessResult<AbstractProvider> SubProvider_Upsert(AbstractProvider abstractProvider)
        {
               return this.abstractProviderDao.SubProvider_Upsert(abstractProvider);
           }

           public override SuccessResult<AbstractProvider> Provider_ById(long Id, long UserId = 0)
           {
               return this.abstractProviderDao.Provider_ById(Id,UserId);
           }
           public override PagedList<AbstractProvider> GetProviderByCreatedDate(string SDate, string EDate)
        {
               return this.abstractProviderDao.GetProviderByCreatedDate(SDate,EDate);
           }

           public override PagedList<AbstractProvider> Provider_All(PageParam pageParam, string search, long LookupStateId, string Name, string PostCode, string State)
           {
               return this.abstractProviderDao.Provider_All(pageParam, search, LookupStateId,Name,PostCode,State);
           }
        public override PagedList<AbstractProvider> Provider_AllByParentId(PageParam pageParam, string search, long LookupStateId, long ParentId, string Name, string PostCode, string State)
        {
            return this.abstractProviderDao.Provider_AllByParentId(pageParam, search,  LookupStateId, ParentId, Name, PostCode, State);
        }
        public override PagedList<AbstractProvider> Provider_AllByServiceRequestId(PageParam pageParam, string search, int ServiceRequestId)
        {
            return this.abstractProviderDao.Provider_AllByServiceRequestId(pageParam, search, ServiceRequestId);
        }
        public override PagedList<AbstractProvider> ClosedServiceRequestProviderAll(PageParam pageParam, string search, int ServiceRequestId)
        {
            return this.abstractProviderDao.ClosedServiceRequestProviderAll(pageParam, search, ServiceRequestId);
        }
        public override PagedList<AbstractProvider> CheckSubProvider(PageParam pageParam, int Id)
        {
            return this.abstractProviderDao.CheckSubProvider(pageParam, Id);
        }
        public override SuccessResult<AbstractProvider> Provider_ChangePassword(AbstractProvider abstractProvider)
           {
            return this.abstractProviderDao.Provider_ChangePassword(abstractProvider);
           }
        public override SuccessResult<AbstractProvider> Provider_RestPassword(AbstractProvider abstractProvider)
           {
            return this.abstractProviderDao.Provider_RestPassword(abstractProvider);
           }

           public override bool Provider_Logout(long Id)
           {
            return this.abstractProviderDao.Provider_Logout(Id);
           }
        public override bool DeleteProviderLocation(int LId, int ProviderId)
           {
            return this.abstractProviderDao.DeleteProviderLocation(LId,ProviderId);
           }
        public override bool DeleteProvider(int id, int DeletedBy)
        {
            return this.abstractProviderDao.DeleteProvider(id,DeletedBy);
        }
        public override SuccessResult<AbstractProvider> Provider_Login(AbstractProvider abstractProvider)
           {
            return this.abstractProviderDao.Provider_Login(abstractProvider);
           }

           public override SuccessResult<AbstractProvider> Provider_ActInactive(long Id)
           {
            return this.abstractProviderDao.Provider_ActInactive (Id);
           }
        public override PagedList<AbstractProvider> ChildProvider_AllByParentId(PageParam pageParam, string search, int ParentId)
        {
            return this.abstractProviderDao.ChildProvider_AllByParentId(pageParam, search, ParentId);
        }
        public override SuccessResult<AbstractProvider> AddLocation(AbstractProvider abstractProvider)
        {
            return this.abstractProviderDao.AddLocation(abstractProvider);
        }
        public override SuccessResult<AbstractProvider> ProviderLocationPostCodeMapping_Upsert(long Id)
        {
            return this.abstractProviderDao.ProviderLocationPostCodeMapping_Upsert(Id);
        }
        public override PagedList<AbstractProvider> ProviderLocation_All(long PId)
        {
            return this.abstractProviderDao.ProviderLocation_All(PId);
        }
        public override SuccessResult<AbstractProvider> ProviderLocation_ById(long Id)
        {
            return this.abstractProviderDao.ProviderLocation_ById(Id);
        }
        public override SuccessResult<AbstractProvider> Provider_Active(string Email)
        {
            return this.abstractProviderDao.Provider_Active(Email);
        }
        public override SuccessResult<AbstractProvider> Provider_ByEmail(string Email, int Type = 0)
        {
            return this.abstractProviderDao.Provider_ByEmail(Email,Type);
        }
        public override SuccessResult<AbstractProvider> Provider_EmailVerified(string Email)
        {
            return this.abstractProviderDao.Provider_EmailVerified(Email);
        }
        public override bool Provider_IsEmailExist(string Email)
        {
            return this.abstractProviderDao.Provider_IsEmailExist(Email);
        }
        public override bool GetProvider_ByEmail(string Email)
        {
            return this.abstractProviderDao.GetProvider_ByEmail(Email);
        }

        public override SuccessResult<AbstractServicesRequest> CompanyLogoUpload(HttpFileCollectionBase files)
        {
            SuccessResult<AbstractServicesRequest> Result = new SuccessResult<AbstractServicesRequest>();
            AbstractServicesRequest servicesRequest = new ServicesRequest();
            Result.Code = 400;
            if (files != null)
            {
                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFileBase file = files[i];

                    //}
                    //foreach (var selectfile in files) {
                    servicesRequest.Path = "Content/doc/";
                    servicesRequest.AvatarFolder = HttpContext.Current.Server.MapPath("~/Content/doc/");
                    //  var file = selectfile;
                    string imgName = string.Empty;

                    if (!Directory.Exists(servicesRequest.AvatarFolder))
                    {
                        Directory.CreateDirectory(servicesRequest.AvatarFolder);
                    }

                    if (file != null && file.ContentLength > 0)
                    {
                        imgName = DateTime.Now.ToString("ddMMyyyyhhmmss") + file.FileName;
                    }
                    file.SaveAs(servicesRequest.AvatarFolder + imgName);
                    string avatarpath = servicesRequest.AvatarFolder + imgName;
                    servicesRequest.DocumentPath = servicesRequest.Path + imgName;

                    servicesRequest.ImageUrl = servicesRequest.DocumentPath;
                    servicesRequest.FileName = file.FileName;
                    //servicesRequest.Id = Result.Item.Id;
                    if (!string.IsNullOrWhiteSpace(avatarpath))
                    {
                        S3FileUpload(avatarpath, servicesRequest.DocumentPath);
                    }
                }

                Result.Code = 200;
                Result.Item = servicesRequest;
            }
            return Result;
        }

    }
   }
