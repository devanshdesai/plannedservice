﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlannedServices.Common;
using PlannedServices.Common.Paging;
using PlannedServices.Entities.Contract;


namespace PlannedServices.Data.Contract
{
    public abstract class AbstractResourcesDao
    {
        public abstract SuccessResult<AbstractResources> Resources_Upsert(AbstractResources abstractResources);
        public abstract SuccessResult<AbstractResources> Resources_ById(long Id);
        public abstract PagedList<AbstractResources> Resources_All(PageParam pageParam, string search);

    }
}
