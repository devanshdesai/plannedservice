﻿namespace PlannedServicesProviderPortal.Pages
{
    public class Actions
    {
        public const string Index = "Index";


        #region Authentication
        public const string Home = "Home";
        public const string LogIn = "LogIn";
        public const string SignUp = "SignUp";
        public const string UserSignUp = "UserSignUp";
        public const string SignUpPlan = "SignUpPlan";
        public const string SignUpSuccess = "SignUpSuccess";
        public const string RecoverPassword = "RecoverPassword";
        public const string Logout = "Logout";
        public const string UserProfile = "UserProfile";
        public const string EditUserProfile = "EditUserProfile";
        public const string AddUsers = "AddUsers";
        public const string AddSubUsers = "AddUsers";
        public const string ResetPassword = "ResetPassword";
        public const string SetPassword = "SetPassword";
        public const string ForgotPassword = "ForgotPassword";
        public const string GetState = "GetState";
        #endregion

        #region Provider
        public const string FindProvider = "FindProvider";
        public const string AddSubProvider = "AddSubProvider";
        public const string SendInvite = "SendInvite";
        public const string UploadCompanyLogo = "UploadCompanyLogo";
        public const string SendUserInvite = "SendUserInvite";
        public const string InviteUsers = "InviteUsers";
        public const string SendEmail = "SendEmail";
        public const string VerifyEmail = "VerifyEmail";
        public const string ChangeLocation = "ChangeLocation";

        #endregion

        #region ServiceRequest
        public const string ViewServiceRequest = "ViewServiceRequest";

        public const string AddServiceRequest = "AddServiceRequest";
        public const string AddServiceRequestSupport = "AddServiceRequestSupport";
        public const string DeleteServiceRequestSupport = "DeleteServiceRequestSupport";
        public const string AddCommunication = "AddCommunication";

        public const string SendRequest = "SendRequest";
        public const string ServiceResponse = "ServiceResponse";
        public const string RegistrationGroupByCategoryId = "RegistrationGroupByCategoryId";
        public const string ServiceDetails = "ServiceDetails";
        public const string UpdateProviderIsCapacity = "UpdateProviderIsCapacity";
        public const string DeleteServiceRequest = "DeleteServiceRequest";
        public const string DeleteServiceRequestProvider = "DeleteServiceRequestProvider";
        public const string DeleteProviderLocation = "DeleteProviderLocation";
        public const string ChangeServiceStatus = "ChangeServiceStatus";
        public const string PlanManagerDetails = "PlanManagerDetails";
        public const string ChangePassword = "ChangePassword";
        public const string DeleteUser = "DeleteUser";
        public const string DeleteProvider = "DeleteProvider";
        public const string PSettings = "PSettings";
        public const string CPSettings = "CPSettings";
        public const string PDSettings = "PDSettings";
        public const string PaymentDetails = "PaymentDetails";
        public const string PMDSettings = "PMDSettings";
        public const string UserList = "UserList";
        public const string AddProvider = "AddProvider";
        public const string AddProviderService = "AddProviderService";
        public const string AddProviderServiceArea = "AddProviderServiceArea";
        public const string ProviderServices = "ProviderServices";
        public const string ProviderServicesArea = "ProviderServicesArea";
        public const string FilesUpload = "FilesUpload";
        public const string AddLocation = "AddLocation";
        public const string AddProviderLocation = "AddProviderLocation";

        #endregion

    }
}