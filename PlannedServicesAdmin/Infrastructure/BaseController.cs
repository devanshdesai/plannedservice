﻿using PlannedServices.Common;
using PlannedServicesAdmin.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;
using System.Threading;
using System.Data.SqlClient;
using System.Data;
//using PlannedServices.Entities.Contract;
//using PlannedServices.Services.Contract;
//using PlannedServices.Common.Paging;
//using PlannedServices.Entities.V1;

namespace PlannedServicesAdmin.Infrastructure
{

    public class BaseController : Controller
    {
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
        }


        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                if (ProjectSession.UserID == null || ProjectSession.UserID == 0)
                {
                    filterContext.Result = new RedirectResult("~/Authentication/SignIn");
                    return;
                }
                base.OnActionExecuting(filterContext);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                throw ex;
            }
        }

    }
}