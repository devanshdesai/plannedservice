﻿using System.Web;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Globalization;

namespace PlannedServices.Common
{
    public class ProjectSession
    {
        public static int ProviderId
        {
            get
            {
                if (HttpContext.Current.Session["ProviderId"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Integer(HttpContext.Current.Session["ProviderId"]);
                }
            }

            set
            {
                HttpContext.Current.Session["ProviderId"] = value;
            }
        }

        public static int ParentId
        {
            get
            {
                if (HttpContext.Current.Session["ParentId"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Integer(HttpContext.Current.Session["ParentId"]);
                }
            }

            set
            {
                HttpContext.Current.Session["ParentId"] = value;
            }
        }

        public static string ProviderName
        {
            get
            {
                if (HttpContext.Current.Session["ProviderName"] == "")
                {
                    return "";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["ProviderName"]);
                }
            }

            set
            {
                HttpContext.Current.Session["ProviderName"] = value;
            }
        }

        public static int SetupUserID
        {
            get
            {
                if (HttpContext.Current.Session["SetupUserID"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Integer(HttpContext.Current.Session["SetupUserID"]);
                }
            }

            set
            {
                HttpContext.Current.Session["SetupUserID"] = value;
            }
        }

        public static int SetupPlanManagerID
        {
            get
            {
                if (HttpContext.Current.Session["SetupPlanManagerID"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Integer(HttpContext.Current.Session["SetupPlanManagerID"]);
                }
            }

            set
            {
                HttpContext.Current.Session["SetupPlanManagerID"] = value;
            }
        }

        public static int SetupPaymentID
        {
            get
            {
                if (HttpContext.Current.Session["SetupPaymentID"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Integer(HttpContext.Current.Session["SetupPaymentID"]);
                }
            }

            set
            {
                HttpContext.Current.Session["SetupPaymentID"] = value;
            }
        }
        public static int ManagaerUserId
        {
            get
            {
                if (HttpContext.Current.Session["ManagaerUserId"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Integer(HttpContext.Current.Session["ManagaerUserId"]);
                }
            }

            set
            {
                HttpContext.Current.Session["ManagaerUserId"] = value;
            }
        }
        public static string SetupUserType
        {
            get
            {
                if (HttpContext.Current.Session["SetupUserType"] == "")
                {
                    return "";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["SetupUserType"]);
                }
            }

            set
            {
                HttpContext.Current.Session["SetupUserType"] = value;
            }
        }
        public static int UserID
        {
            get
            {
                if (HttpContext.Current.Session["UserID"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Integer(HttpContext.Current.Session["UserID"]);
                }
            }

            set
            {
                HttpContext.Current.Session["UserID"] = value;
            }
        }
        public static int ExpoUserID
        {
            get
            {
                if (HttpContext.Current.Session["ExpoUserID"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Integer(HttpContext.Current.Session["ExpoUserID"]);
                }
            }

            set
            {
                HttpContext.Current.Session["ExpoUserID"] = value;
            }
        }

        public static string UserName
        {
            get
            {
                if (HttpContext.Current.Session["UserName"] == "")
                {
                    return "";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["UserName"]);
                }
            }

            set
            {
                HttpContext.Current.Session["UserName"] = value;
            }
        }
        public static string SetupUserName
        {
            get
            {
                if (HttpContext.Current.Session["SetupUserName"] == "")
                {
                    return "";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["SetupUserName"]);
                }
            }

            set
            {
                HttpContext.Current.Session["SetupUserName"] = value;
            }
        }

        public static int LoginUserID
        {
            get
            {
                if (HttpContext.Current.Session["LoginUserID"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Integer(HttpContext.Current.Session["LoginUserID"]);
                }
            }

            set
            {
                HttpContext.Current.Session["LoginUserID"] = value;
            }
        }
        public static int ManagerUserCount
        {
            get
            {
                if (HttpContext.Current.Session["ManagerUserCount"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Integer(HttpContext.Current.Session["ManagerUserCount"]);
                }
            }

            set
            {
                HttpContext.Current.Session["ManagerUserCount"] = value;
            }
        }

        public static string LoginUserName
        {
            get
            {
                if (HttpContext.Current.Session["LoginUserName"] == "")
                {
                    return "";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["LoginUserName"]);
                }
            }

            set
            {
                HttpContext.Current.Session["LoginUserName"] = value;
            }
        }
        public static string Email
        {
            get
            {
                if (HttpContext.Current.Session["Email"] == "")
                {
                    return "";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["Email"]);
                }
            }

            set
            {
                HttpContext.Current.Session["Email"] = value;
            }
        }

    }
}