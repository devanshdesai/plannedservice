﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlannedServices.Common;
using PlannedServices.Common.Paging;
using PlannedServices.Data.Contract;
using PlannedServices.Entities.Contract;
using PlannedServices.Entities.V1;


namespace PlannedServices.Services.Contract
{
    public abstract class AbstractResourcesServices
    {
        public abstract SuccessResult<AbstractResources> Resources_Upsert(AbstractResources abstractResources);
        public abstract SuccessResult<AbstractResources> Resources_ById(long Id);
        public abstract PagedList<AbstractResources> Resources_All(PageParam pageParam, string search);

    }
}
