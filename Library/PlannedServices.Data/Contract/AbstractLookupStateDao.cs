﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlannedServices.Common;
using PlannedServices.Common.Paging;
using PlannedServices.Entities.Contract;


namespace PlannedServices.Data.Contract
{
    public abstract class AbstractLookupStateDao
    {
        public abstract SuccessResult<AbstractLookupState> LookupState_Upsert(AbstractLookupState abstractLookupState);
        public abstract SuccessResult<AbstractLookupState> LookupState_ById(long Id);
        public abstract PagedList<AbstractLookupState> LookupState_All(PageParam pageParam, string search);

    }
}
