﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Net.Http;
using Newtonsoft.Json;
using System.Data;

namespace ConsoleforLatLong
{
    class Program
    {
        private static readonly HttpClient client = new HttpClient();

        static async Task Main(string[] args)
        {
            SqlConnection conn = new SqlConnection("Data Source=database-zeki.cgyhfqhnqm3z.eu-west-2.rds.amazonaws.com;Initial Catalog=PlnnedSrvices;Persist Security Info=True;User ID=sa;Password=*aA123123;MultipleActiveResultSets=True;Encrypt=True;TrustServerCertificate=True");
            SqlDataAdapter ad = new SqlDataAdapter(@"select * from provider", conn);
            DataTable dt = new DataTable();
            ad.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                //for (int i = 0; i < dt.Rows.Count; i++)
                //{
                //   var id = dt.Rows[i]["Id"];
                //   var address = dt.Rows[i]["PinCode"];
                //    var stringTask = client.GetStringAsync("https://maps.googleapis.com/maps/api/geocode/json?address="+ address + "&key=AIzaSyB97EhOhMCYhK1tZIofUkpKcGW61fdgKjg");

                //    var msg = await stringTask;

                //    var s = JsonConvert.DeserializeObject<Root>(msg);
                //    if (s.results.Count > 0)
                //    {
                //        var lat = s.results[0].geometry.location.lat;
                //        var lng = s.results[0].geometry.location.lng;

                //        string sql = "update provider set Lat = " + lat + ",Long = " + lng + " where Id = " + id + "";
                //        SqlCommand cmd = new SqlCommand(sql, conn);
                //        SqlDataReader dr = cmd.ExecuteReader();
                //    }
                //}
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    SqlDataAdapter add = new SqlDataAdapter(@"select * from ProviderLocation where PinCode ="+ dt.Rows[i]["PinCode"], conn);
                    DataTable dtt = new DataTable();
                    add.Fill(dtt);
                    if (dtt.Rows.Count > 0)
                    {
                        for (int j = 0; j < dtt.Rows.Count; j++)
                        {
                            var id = dtt.Rows[j]["Id"];
                            using (SqlCommand cmd = new SqlCommand("ProviderLocationPostCodeMapping_Upsert", conn))
                            {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@ProviderLocationId", id);
                                SqlDataReader dr;
                                dr = cmd.ExecuteReader();
                                if (!dr.Read())
                                {
                                    Console.Write("Record not found ");
                                }
                                else
                                {
                                    Console.ReadLine();
                                }
                            }
                        }
                    }

                }


                conn.Close();
            }

                 

            Console.Write("");
        }
    }

    public class AddressComponent
    {
        public string long_name { get; set; }
        public string short_name { get; set; }
        public List<string> types { get; set; }
    }

    public class Northeast
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Southwest
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Bounds
    {
        public Northeast northeast { get; set; }
        public Southwest southwest { get; set; }
    }

    public class Location
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Viewport
    {
        public Northeast northeast { get; set; }
        public Southwest southwest { get; set; }
    }

    public class Geometry
    {
        public Bounds bounds { get; set; }
        public Location location { get; set; }
        public string location_type { get; set; }
        public Viewport viewport { get; set; }
    }

    public class Result
    {
        public List<AddressComponent> address_components { get; set; }
        public string formatted_address { get; set; }
        public Geometry geometry { get; set; }
        public string place_id { get; set; }
        public List<string> types { get; set; }
    }

    public class Root
    {
        public List<Result> results { get; set; }
        public string status { get; set; }
    }
}
