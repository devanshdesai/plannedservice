﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using PlannedServices.Common;
using PlannedServices.Common.Paging;
using PlannedServices.Data.Contract;
using PlannedServices.Entities.Contract;
using PlannedServices.Entities.V1;
using PlannedServices.Services.Contract;

namespace PlannedServices.Services.V1
{
    public class ServicesRequestServices : AbstractServicesRequestServices
    {
        private AbstractServicesRequestDao abstractServicesRequestsDao;

        public ServicesRequestServices(AbstractServicesRequestDao abstractServicesRequestsDao)
        {
            this.abstractServicesRequestsDao = abstractServicesRequestsDao;
        }


        public override PagedList<AbstractServicesRequest> ServicesRequest_All(PageParam pageParam, string search, int ProviderId)
        {
            return this.abstractServicesRequestsDao.ServicesRequest_All(pageParam, search, ProviderId);
        }
        public override PagedList<AbstractServicesRequest> ClosedServiceRequestParticipateAll(PageParam pageParam, string search, int ProviderId)
        {
            return this.abstractServicesRequestsDao.ClosedServiceRequestParticipateAll(pageParam, search, ProviderId);
        }
        public override PagedList<AbstractServicesRequest> ServiceRequest_AllByCustomerId(PageParam pageParam, string search, int CustomerId)
        {
            return this.abstractServicesRequestsDao.ServiceRequest_AllByCustomerId(pageParam, search, CustomerId);
        }
        public override PagedList<AbstractServicesRequest> ServicesRequest_ById(PageParam pageParam, string search, int Id, int ProviderId)
        {
            return this.abstractServicesRequestsDao.ServicesRequest_ById(pageParam, search, Id, ProviderId);
        }
        public override SuccessResult<AbstractServicesRequest> ServiceRequestSupportItemProvider_Upsert(AbstractServicesRequest abstractServices)
        {
            return this.abstractServicesRequestsDao.ServiceRequestSupportItemProvider_Upsert(abstractServices);
        }
        public override SuccessResult<AbstractServicesRequest> UpdateServiceRequestChildByProviderId(int ServiceRequestId, int ProviderId, int Status, string Comment, string ReasonId)
        {
            return this.abstractServicesRequestsDao.UpdateServiceRequestChildByProviderId(ServiceRequestId, ProviderId, Status, Comment, ReasonId);
        }
        public override SuccessResult<AbstractServicesRequest> Communication_Upsert(AbstractServicesRequest servicesRequest, IEnumerable<System.Web.HttpPostedFileBase> images = null)
        {
            SuccessResult<AbstractServicesRequest> Communication = new SuccessResult<AbstractServicesRequest>();
            var Result = this.abstractServicesRequestsDao.Communication_Upsert(servicesRequest);
            if (images != null)
            {
                if (images.ElementAt(0) != null && Result.Item != null)
                {
                    servicesRequest.Path = "Content/images/doc/";
                    servicesRequest.AvatarFolder = HttpContext.Current.Server.MapPath("~/Content/images/doc/");
                    var file = images.ElementAt(0);
                    string imgName = string.Empty;

                    if (!Directory.Exists(servicesRequest.AvatarFolder))
                    {
                        Directory.CreateDirectory(servicesRequest.AvatarFolder);
                    }

                    if (file != null && file.ContentLength > 0)
                    {
                        imgName = DateTime.Now.ToString("ddMMyyyyhhmmss") + file.FileName;
                    }
                    file.SaveAs(servicesRequest.AvatarFolder + imgName);
                    string avatarpath = servicesRequest.AvatarFolder + imgName;
                    servicesRequest.DocumentPath = servicesRequest.Path + imgName;

                    servicesRequest.ImageUrl = servicesRequest.DocumentPath;
                    servicesRequest.Id = Result.Item.Id;
                    if (!string.IsNullOrWhiteSpace(avatarpath))
                    {
                        S3FileUpload(avatarpath, servicesRequest.DocumentPath);
                    }
                    Communication = this.abstractServicesRequestsDao.Communication_Upsert(servicesRequest);

                }
            }
            return Result;
        }
        public override SuccessResult<AbstractServicesRequest> FilesUpload(HttpFileCollectionBase files)
        {
            SuccessResult<AbstractServicesRequest> Result = new SuccessResult<AbstractServicesRequest>();
            AbstractServicesRequest servicesRequest = new ServicesRequest();
            Result.Code = 400;
            if (files != null)
            {
                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFileBase file = files[i];

                //}
                //foreach (var selectfile in files) {
                    servicesRequest.Path = "Content/doc/";
                    servicesRequest.AvatarFolder = HttpContext.Current.Server.MapPath("~/Content/doc/");
                  //  var file = selectfile;
                    string imgName = string.Empty;

                    if (!Directory.Exists(servicesRequest.AvatarFolder))
                    {
                        Directory.CreateDirectory(servicesRequest.AvatarFolder);
                    }

                    if (file != null && file.ContentLength > 0)
                    {
                        imgName = DateTime.Now.ToString("ddMMyyyyhhmmss") + file.FileName;
                    }
                    file.SaveAs(servicesRequest.AvatarFolder + imgName);
                    string avatarpath = servicesRequest.AvatarFolder + imgName;
                    servicesRequest.DocumentPath = servicesRequest.Path + imgName;

                    servicesRequest.ImageUrl += servicesRequest.DocumentPath +",";
                    servicesRequest.FileName += file.FileName + ",";
                    //servicesRequest.Id = Result.Item.Id;
                    if (!string.IsNullOrWhiteSpace(avatarpath))
                    {
                        S3FileUpload(avatarpath, servicesRequest.DocumentPath);
                    }
                }
                
                Result.Code = 200;
                Result.Item = servicesRequest;
            }
            return Result;
        }
        public override PagedList<AbstractServicesRequest> Communication_AllByProviderId(PageParam pageParam, string search, int CustomerId, int ProviderId, int ServiceRequestChildId)
        {
            return this.abstractServicesRequestsDao.Communication_AllByProviderId(pageParam, search, CustomerId, ProviderId, ServiceRequestChildId);
        }

        public override PagedList<AbstractServicesRequest> ServiceRequests_AllByCustomerId(int CustomerId)
        {


            PagedList<AbstractServicesRequest> servicesRequest = new PagedList<AbstractServicesRequest>();

            var servicesRequestData = this.abstractServicesRequestsDao.ServiceRequests_AllByCustomerId(CustomerId);

            if (servicesRequestData.Item != null)
            {
                if (servicesRequestData.Item.ServicesRequests != null && servicesRequestData.Item.Providers != null)
                {

                    foreach (var sRequest in servicesRequestData.Item.ServicesRequests)
                    {
                        var selectedProviders = servicesRequestData.Item.Providers.Where(x => x.ServiceRequestId == sRequest.Id).ToList();
                        sRequest.Providers = selectedProviders;
                        servicesRequest.Values.Add(sRequest);
                    }
                }
            }
            return servicesRequest;
        }
        public override PagedList<AbstractServicesRequest> ClosedServiceRequestByCustomerId(int CustomerId)
        {


            PagedList<AbstractServicesRequest> servicesRequest = new PagedList<AbstractServicesRequest>();

            var servicesRequestData = this.abstractServicesRequestsDao.ClosedServiceRequestByCustomerId(CustomerId);

            if (servicesRequestData.Item != null)
            {
                if (servicesRequestData.Item.ServicesRequests != null && servicesRequestData.Item.Providers != null)
                {

                    foreach (var sRequest in servicesRequestData.Item.ServicesRequests)
                    {
                        var selectedProviders = servicesRequestData.Item.Providers.Where(x => x.ServiceRequestId == sRequest.Id).ToList();
                        sRequest.Providers = selectedProviders;
                        servicesRequest.Values.Add(sRequest);
                    }
                }
            }
            return servicesRequest;
        }
        public override SuccessResult<AbstractServicesRequest> CommunicationIsRead(int ProviderId, int CustomerId, int ServiceRequestChildId)
        {
            return this.abstractServicesRequestsDao.CommunicationIsRead(ProviderId, CustomerId, ServiceRequestChildId);
        }
        public override bool DeleteServiceRequestSupportItemProvider(int id)
        {
            return this.abstractServicesRequestsDao.DeleteServiceRequestSupportItemProvider(id);
        }

        public override PagedList<AbstractServicesRequest> GetParticipantsChartData(int Id, int Type = 0)
        {
            return this.abstractServicesRequestsDao.GetParticipantsChartData(Id,Type);
        }
        public override PagedList<AbstractServicesRequest> GetProviderChartData(int Id)
        {
            return this.abstractServicesRequestsDao.GetProviderChartData(Id);
        }
    }
}
