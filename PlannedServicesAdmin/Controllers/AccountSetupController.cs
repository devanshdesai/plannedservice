﻿using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;
using OfficeOpenXml;
using PlannedServices.Common;
using PlannedServices.Common.Paging;
using PlannedServices.Entities.Contract;
using PlannedServices.Entities.V1;
using PlannedServices.Services.Contract;
using PlannedServicesAdmin.Pages;
using SendGrid;
using SendGrid.Helpers.Mail;
using Stripe;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using static PlannedServicesAdmin.Infrastructure.Enums;

namespace PlannedServicesAdmin.Controllers
{
    public class AccountSetupController : Controller
    {
        #region Fields
        private readonly AbstractUserServices abstractUserServices;
        private readonly AbstractServicesServices abstractServices;
        private readonly AbstractLookupStateServices abstractLookupStateServices;
        #endregion

        #region Ctor
        public AccountSetupController(AbstractUserServices abstractUserServices,
            AbstractLookupStateServices abstractLookupStateServices, AbstractServicesServices abstractServices)
        {
            this.abstractUserServices = abstractUserServices;
            this.abstractLookupStateServices = abstractLookupStateServices;
            this.abstractServices = abstractServices;
        }
        #endregion

        // GET: AccountSetup
        public ActionResult Index()
        {
            ProjectSession.UserID = ProjectSession.SetupUserID;
            ProjectSession.LoginUserID = ProjectSession.SetupUserID;
            ProjectSession.UserName = ProjectSession.SetupUserName;
            AbstractUser abstractUser = null;
            if (ProjectSession.SetupUserID > 0)
            {
                int decryptedId = ProjectSession.SetupUserID;
                if (decryptedId > 0)
                {
                    abstractUser = abstractUserServices.User_ById(decryptedId).Item;
                    ViewBag.userId = decryptedId;
                }
            }
            return View(abstractUser);
        }

        public ActionResult PDInviteSettings(string stremail = "")
        {
            //if (TempData["openPopup"] != null)
            //    ViewBag.openPopup = TempData["openPopup"];
            ViewBag.ErrorMSG = TempData["errorMSG"];
            User user = new User();
            List<SelectListItem> items = new List<SelectListItem>();
            ViewBag.States = items;
            ViewBag.Citys = items;
            ViewBag.BStates = items;
            ViewBag.BCitys = items;

            if (!string.IsNullOrWhiteSpace(stremail))
            {

                string decryptedEmail = stremail;
                if (!string.IsNullOrWhiteSpace(decryptedEmail))
                {
                    user.Email = decryptedEmail;
                }
            }
            return View(user);
        }
        public ActionResult PDSettings(string UserType)
        {
            //if (TempData["openPopup"] != null)
            //    ViewBag.openPopup = TempData["openPopup"];
            ViewBag.ErrorMSG = TempData["errorMSG"];
            ProjectSession.SetupUserType = UserType;
            AbstractUser abstractUser = new User();
            ViewBag.userId = 0;
            ViewBag.States = BindStateDropdown();
            if (ProjectSession.SetupUserID > 0)
            {
                int decryptedId = ProjectSession.SetupUserID;
                if (decryptedId > 0)
                {
                    abstractUser = abstractUserServices.User_ById(decryptedId).Item;
                    ViewBag.userId = decryptedId;
                }
            }
            return View(abstractUser);
        }

        public ActionResult PDStep2()
        {
            //if (TempData["openPopup"] != null)
            //    ViewBag.openPopup = TempData["openPopup"];
            ViewBag.ErrorMSG = TempData["errorMSG"];
            List<SelectListItem> items = new List<SelectListItem>();
            ViewBag.States = items;
            ViewBag.Citys = items;
            ViewBag.BStates = items;
            ViewBag.BCitys = items;
            AbstractUser abstractPlanManager = new User();
            if (ProjectSession.SetupUserID > 0)
            {
                int decryptedId = ProjectSession.SetupUserID;
                if (decryptedId > 0)
                {
                    abstractPlanManager = abstractUserServices.User_ById(decryptedId).Item;
                }
            }
            return View(abstractPlanManager);
        }

        public ActionResult CompanyParticipate(string planManager)
        {
            //if (TempData["openPopup"] != null)
            //    ViewBag.openPopup = TempData["openPopup"];
            ViewBag.ErrorMSG = TempData["errorMSG"];

            AbstractUser abstractUser = new User();

            if (planManager != null)
            {
                int decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(planManager));
                if (decryptedId > 0)
                {
                    ViewBag.PlanManagerId = decryptedId;
                    abstractUser.PlanManagerId = decryptedId;
                    var planM = abstractUserServices.PlanManager_ById(abstractUser.PlanManagerId, 0).Item;
                    ViewBag.ImageUrl = planM.CompanyLogoURL;
                }
            }
            ViewBag.userId = 0;
            ViewBag.States = BindStateDropdown();

            return View(abstractUser);
        }

        public ActionResult CompanyParicipateStep2()
        {
            //if (TempData["openPopup"] != null)
            //    ViewBag.openPopup = TempData["openPopup"];
            ViewBag.ErrorMSG = TempData["errorMSG"];
            List<SelectListItem> items = new List<SelectListItem>();
            ViewBag.States = items;
            ViewBag.Citys = items;
            ViewBag.BStates = items;
            ViewBag.BCitys = items;
            AbstractUser abstractUser = new User();
            if (ProjectSession.SetupUserID > 0)
            {
                int decryptedId = ProjectSession.SetupUserID;
                if (decryptedId > 0)
                {
                    abstractUser = abstractUserServices.User_ById(decryptedId).Item;
                    var planM = abstractUserServices.PlanManager_ById(abstractUser.PlanManagerId, 0).Item;
                    abstractUser.PlanManagerName = planM.CompanyName;
                    abstractUser.PlanManagerAddress = planM.AddressLine1;
                    abstractUser.CompanyLogoURL = planM.CompanyLogoURL;
                }
            }
            return View(abstractUser);
        }

        public ActionResult PDManagerStep2()
        {
            //if (TempData["openPopup"] != null)
            //    ViewBag.openPopup = TempData["openPopup"];
            ViewBag.ErrorMSG = TempData["errorMSG"];
            List<SelectListItem> items = new List<SelectListItem>();
            ViewBag.States = items;
            ViewBag.Citys = items;
            ViewBag.BStates = items;
            ViewBag.BCitys = items;
            AbstractUser abstractPlanManager = new User();
            //if (ProjectSession.SetupUserID > 0)
            //{
            //    int decryptedId = ProjectSession.SetupUserID;
            //    if (decryptedId > 0)
            //    {
            //        abstractPlanManager = abstractUserServices.User_ById(decryptedId).Item;
            //    }
            //}
            return View(abstractPlanManager);
        }
        public ActionResult PMDSettings()
        {
            //if (TempData["openPopup"] != null)
            //    ViewBag.openPopup = TempData["openPopup"];

            AbstractUser abstractPlanManager = new User();
            ViewBag.States = BindStateDropdown();
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000000;
            ViewBag.PlanManagerList = abstractUserServices.PlanManager_All(pageParam, "").Values;
            if (ProjectSession.SetupUserID > 0)
            {
                int decryptedId = ProjectSession.SetupUserID;
                if (decryptedId > 0)
                {
                    abstractPlanManager = abstractUserServices.User_ById(decryptedId).Item;
                }
            }
            return View(abstractPlanManager);
        }

        public ActionResult UserList()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];

            AbstractUser abstractUser = new User();
            if (ProjectSession.SetupUserID > 0)
            {
                int decryptedId = ProjectSession.SetupUserID;
                if (decryptedId > 0)
                {
                    PageParam pageParam = new PageParam();
                    var model = abstractUserServices.ChildUsers_AllByParentId(pageParam, "", decryptedId).Values;
                    if (model.Count > 0)
                    {
                        abstractUser.Users = model;
                    }
                }
            }
            return View(abstractUser);
        }

        public ActionResult AddUser()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            return View();
        }
        public ActionResult UploadCSV()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            return View();
        }
        public ActionResult NService()
        {
            ViewBag.Section = 1;
            List<SelectListItem> items = new List<SelectListItem>();
            ViewBag.BudgetMaster = BindBudgetMasterDropdown();
            ViewBag.RegistrationGroup = items;
            ViewBag.SupportCategoryMaster = items;
            ViewBag.SupportItem = items;
            ViewBag.userdata = abstractUserServices.UsersPaymentDetails_ById(ProjectSession.UserID).Item;
            ViewBag.States = items;
            ViewBag.Citys = items;
            return View();
        }

        public ActionResult Location()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];

            List<SelectListItem> items = new List<SelectListItem>();
            //ViewBag.States = BindStateDropdown();
            ViewBag.States = items;
            ViewBag.Citys = items;
            AbstractUser abstractUser = null;
            if (ProjectSession.SetupUserID > 0)
            {
                int decryptedId = ProjectSession.SetupUserID;
                if (decryptedId > 0)
                {
                    abstractUser = abstractUserServices.User_ById(decryptedId).Item;
                    ViewBag.userId = decryptedId;
                    ViewBag.States = BindStateCityDropdown(0, ConvertTo.Integer(abstractUser.PinCode), 0);
                    ViewBag.Citys = BindStateCityDropdown(1, ConvertTo.Integer(abstractUser.PinCode), abstractUser.LookupStateId);

                }
            }
            return View(abstractUser);
        }
        public ActionResult ManageType()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            AbstractUser abstractUser = null;
            if (ProjectSession.SetupUserID > 0)
            {
                int decryptedId = ProjectSession.SetupUserID;
                if (decryptedId > 0)
                {
                    abstractUser = abstractUserServices.User_ById(decryptedId).Item;
                    ViewBag.userId = decryptedId;
                }
            }
            return View(abstractUser);
        }
        public ActionResult PaymentDetails()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            List<SelectListItem> items = new List<SelectListItem>();
            //ViewBag.States = BindStateDropdown();
            ViewBag.States = items;
            ViewBag.Citys = items;
            AbstractUser abstractUser = null;
            if (ProjectSession.SetupUserID > 0)
            {
                int decryptedId = ProjectSession.SetupUserID;
                if (decryptedId > 0)
                {
                    abstractUser = abstractUserServices.UsersPaymentDetails_ById(decryptedId).Item;
                }
            }
            return View(abstractUser);
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.AddUsers)]
        public ActionResult AddUsers(User user)
        {
            int Id = 0;
            try
            {
                user.CreatedBy = ProjectSession.SetupUserID;
                //HttpClient client = new HttpClient();

                //var stringTask = client.GetStringAsync("https://maps.googleapis.com/maps/api/geocode/json?address=" + user.AddressLine1 + "&key=AIzaSyB97EhOhMCYhK1tZIofUkpKcGW61fdgKjg");
                //var msg = stringTask;

                //var s = JsonConvert.DeserializeObject<Root>(msg.Result);
                //if (s.results.Count > 0)
                //{
                //    user.lat = s.results[0].geometry.location.lat;
                //    user.lng = s.results[0].geometry.location.lng;
                //}
                //user.Password = "1234567890";
                //user.ManagerType = Convert.ToInt32(ProjectSession.SetupUserType);
                var userdata = abstractUserServices.User_ByEmail(user.Email, user.Password);
                if (userdata.Item != null)
                {
                    user.Id = userdata.Item.Id;
                }
                user.UserType = user.IsUserMultiple == 1 ? user.UserType : 3;
                SuccessResult<AbstractUser> result = abstractUserServices.User_Upsert(user);
                if (result != null && result.Code == 200 && result.Item != null)
                {
                    var emailVerifiedUserData = abstractUserServices.Users_EmailVerified(user.Email);
                    Id = result.Item.Id;
                    ProjectSession.SetupUserID = result.Item.Id;
                    ProjectSession.Email = result.Item.Email;
                    ProjectSession.SetupUserName = result.Item.FirstName + " " + result.Item.LastName;
                    //TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), result.Message);
                    return RedirectToAction(Actions.PDStep2, Pages.Controllers.AccountSetup, new { Area = "" });
                    //if (user.IsUserMultiple == 1)
                    //{
                    //    return RedirectToAction(Actions.PDStep2, Pages.Controllers.AccountSetup, new { Area = "" });
                    //}
                    //else
                    //{
                    //    return RedirectToAction(Actions.PDManagerStep2, Pages.Controllers.AccountSetup, new { Area = "" });
                    //}
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                    TempData["errorMSG"] = result.Message;
                }
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "");
            }
            return RedirectToAction(Actions.PDSettings, Pages.Controllers.AccountSetup, new { Area = "", UserType = ProjectSession.SetupUserType });
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.AddComapanyUsers)]
        public ActionResult AddComapanyUsers(User user)
        {
            int Id = 0;
            try
            {
                user.CreatedBy = ProjectSession.SetupUserID;
                var userdata = abstractUserServices.User_ByEmail(user.Email, user.Password);
                if (userdata.Item != null)
                {
                    user.Id = userdata.Item.Id;
                }
                user.UserType = user.IsUserMultiple == 1 ? user.UserType : 3;
                user.ManagerType = 1;

                SuccessResult<AbstractUser> result = abstractUserServices.User_Upsert(user);
                SuccessResult<AbstractUser> result2 = abstractUserServices.UpdatePlanManager(result.Item.Id, user.PlanManagerId);
                if (result != null && result.Code == 200 && result.Item != null)
                {
                    var emailVerifiedUserData = abstractUserServices.Users_EmailVerified(user.Email);
                    Id = result.Item.Id;
                    ProjectSession.SetupUserID = result.Item.Id;
                    ProjectSession.Email = result.Item.Email;
                    ProjectSession.SetupUserName = result.Item.FirstName + " " + result.Item.LastName;
                    return RedirectToAction(Actions.SignUpStep2, Pages.Controllers.PurpleLeopard, new { Area = "" });
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                    TempData["errorMSG"] = result.Message;
                }
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "");
            }
            return RedirectToAction(Actions.SignUp, Pages.Controllers.PurpleLeopard, new { Area = "", UserType = ProjectSession.SetupUserType });
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.AddInviteUsers)]
        public ActionResult AddInviteUsers(User user)
        {
            int Id = 0;
            try
            {
                user.CreatedBy = ProjectSession.SetupUserID;

                var userdata = abstractUserServices.User_ByEmail(user.Email, user.Password);
                if (userdata.Item != null)
                {
                    user.Id = userdata.Item.Id;
                }
                SuccessResult<AbstractUser> result = abstractUserServices.User_Upsert(user);
                if (result != null && result.Code == 200 && result.Item != null)
                {
                    var emailVerifiedUserData = abstractUserServices.Users_EmailVerified(user.Email);
                    Id = result.Item.Id;
                    if (user.ManagerType == 2)
                    {
                        if (user.IsPlan == 1)
                        {
                            Stripe.StripeConfiguration.SetApiKey(Configurations.StripePublishableKey);
                            Stripe.StripeConfiguration.ApiKey = Configurations.StripeSecretKey;

                            var myCharge = new Stripe.ChargeCreateOptions();
                            // always set these properties
                            myCharge.Amount = ConvertTo.Long(44000);
                            myCharge.Currency = "AUD";
                            myCharge.ReceiptEmail = user.stripeEmail;
                            myCharge.Description = "NDIS";
                            myCharge.Source = user.stripeToken;
                            myCharge.Capture = true;
                            var chargeService = new Stripe.ChargeService();
                            Charge stripeCharge = chargeService.Create(myCharge);

                            user.TransactionDate = stripeCharge.Created;
                            user.TransactionEndDate = stripeCharge.Created.AddYears(1).ToString();
                            user.TransactionId = stripeCharge.BalanceTransactionId;
                            user.CardNumber = stripeCharge.PaymentMethodDetails.Card.Last4;
                            user.ExpiryDate = stripeCharge.PaymentMethodDetails.Card.ExpMonth.ToString() + '/' + stripeCharge.PaymentMethodDetails.Card.ExpYear;
                        }


                        user.Id = 0;
                        user.PinCode = user.BPinCode;
                        user.LookupStateId = user.BLookupStateId;
                        user.City = user.BCity;
                        user.UserId = Id;

                        SuccessResult<AbstractUser> result2 = abstractUserServices.UsersPaymentDetails_Upsert(user);
                        if (result2 != null && result2.Code == 200 && result2.Item != null)
                        {
                            ProjectSession.SetupUserID = result.Item.Id;
                            ProjectSession.SetupUserName = result.Item.FirstName + " " + result.Item.LastName;
                            ProjectSession.UserID = ProjectSession.SetupUserID;
                            ProjectSession.LoginUserID = ProjectSession.SetupUserID;
                            ProjectSession.UserName = ProjectSession.SetupUserName;
                        }
                    }
                    else
                    {
                        SuccessResult<AbstractUser> result2 = abstractUserServices.UpdatePlanManager(result.Item.Id, user.PlanManagerId);
                        ProjectSession.SetupUserID = result.Item.Id;
                        ProjectSession.SetupUserName = result.Item.FirstName + " " + result.Item.LastName;
                        ProjectSession.UserID = ProjectSession.SetupUserID;
                        ProjectSession.LoginUserID = ProjectSession.SetupUserID;
                        ProjectSession.UserName = ProjectSession.SetupUserName;
                    }


                    TempData["displayModal"] = "myModal2";
                    //TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), result.Message);
                    return RedirectToAction("PDInviteSettings", Pages.Controllers.AccountSetup, new { Area = "" });
                    //if (ProjectSession.SetupUserType == "1")
                    //{
                    //    return RedirectToAction(Actions.PMDSettings, Pages.Controllers.AccountSetup, new { Area = "" });
                    //}
                    //else
                    //{
                    //    return RedirectToAction(Actions.PaymentDetails, Pages.Controllers.AccountSetup, new { Area = "" });
                    //}
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                    TempData["errorMSG"] = result.Message;
                }
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "");
            }
            return RedirectToAction("PDInviteSettings", Pages.Controllers.AccountSetup, new { Area = "" });
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.AddUpdateUsers)]
        public ActionResult AddUpdateUsers(User user)
        {
            int Id = 0;
            try
            {
                user.CreatedBy = ProjectSession.SetupUserID;

                var userdata = abstractUserServices.User_ById(ProjectSession.SetupUserID);
                if (userdata.Item != null)
                {
                    user.Id = userdata.Item.Id;
                    user.Email = userdata.Item.Email;
                }
                SuccessResult<AbstractUser> result = abstractUserServices.User_Upsert(user);
                if (result != null && result.Code == 200 && result.Item != null)
                {
                    var emailVerifiedUserData = abstractUserServices.Users_EmailVerified(user.Email);
                    Id = result.Item.Id;
                    if (user.ManagerType == 2)
                    {
                        if (user.IsPlan == 1)
                        {
                            Stripe.StripeConfiguration.SetApiKey(Configurations.StripePublishableKey);
                            Stripe.StripeConfiguration.ApiKey = Configurations.StripeSecretKey;

                            var myCharge = new Stripe.ChargeCreateOptions();
                            // always set these properties
                            myCharge.Amount = ConvertTo.Long(44000);
                            myCharge.Currency = "AUD";
                            myCharge.ReceiptEmail = user.stripeEmail;
                            myCharge.Description = "NDIS";
                            myCharge.Source = user.stripeToken;
                            myCharge.Capture = true;
                            var chargeService = new Stripe.ChargeService();
                            Charge stripeCharge = chargeService.Create(myCharge);

                            user.TransactionDate = stripeCharge.Created;
                            user.TransactionEndDate = stripeCharge.Created.AddYears(1).ToString();
                            user.TransactionId = stripeCharge.BalanceTransactionId;
                            user.CardNumber = stripeCharge.PaymentMethodDetails.Card.Last4;
                            user.ExpiryDate = stripeCharge.PaymentMethodDetails.Card.ExpMonth.ToString() + '/' + stripeCharge.PaymentMethodDetails.Card.ExpYear;



                        }

                        user.Id = 0;
                        user.PinCode = user.BPinCode;
                        user.LookupStateId = user.BLookupStateId;
                        user.City = user.BCity;
                        user.UserId = ProjectSession.SetupUserID;

                        SuccessResult<AbstractUser> result2 = abstractUserServices.UsersPaymentDetails_Upsert(user);
                        if (result2 != null && result2.Code == 200 && result2.Item != null)
                        {
                            ProjectSession.SetupUserID = result.Item.Id;
                            ProjectSession.SetupUserName = result.Item.FirstName + " " + result.Item.LastName;
                            ProjectSession.UserID = ProjectSession.SetupUserID;
                            ProjectSession.LoginUserID = ProjectSession.SetupUserID;
                            ProjectSession.UserName = ProjectSession.SetupUserName;

                            var userPaymentData = abstractUserServices.UsersPaymentDetails_ById(ProjectSession.UserID).Item;
                            var userPaymentData1 = abstractUserServices.UsersPaymentDetails_All();
                            var SelectedUser = abstractUserServices.User_ById(ProjectSession.UserID).Item;

                            if (user.ManagerType == 2)
                            {
                                if (user.IsPlan == 1)
                                {
                                    byte[] bytes = null;
                                    string strDeviceInfo = null;
                                    string strMimeType = null;
                                    string strEncoding;
                                    string strExtension;
                                    string[] strStreams = null;
                                    Warning[] warnings;

                                    try
                                    {
                                        DataTable dttest = new DataTable();
                                        dttest.Columns.Add("id");
                                        dttest.Columns.Add("fname");
                                        dttest.Columns.Add("lname");
                                        dttest.Columns.Add("userid");
                                        dttest.Columns.Add("city");
                                        dttest.Columns.Add("state");
                                        dttest.Columns.Add("postcode");
                                        dttest.Columns.Add("invoicedate");
                                        dttest.Columns.Add("invoicenumber");
                                        dttest.Columns.Add("add");

                                        dttest.Rows.Add("1", SelectedUser.NomineeFirstName + " " + SelectedUser.NomineeLastName, SelectedUser.FirstName + " " + SelectedUser.LastName, SelectedUser.Id.ToString(), userPaymentData.City + " " + userPaymentData.StateName + " " + userPaymentData.PinCode,
                                            userPaymentData.StateName, userPaymentData.PinCode.ToString(), userPaymentData.TransactionDateStr,
                                            userPaymentData1.TotalRecords.ToString(), userPaymentData.AddressLine1);
                                        ReportViewer rptViewer1 = new ReportViewer();


                                        rptViewer1.ProcessingMode = ProcessingMode.Local;
                                        rptViewer1.LocalReport.ReportPath = Server.MapPath("~/report/Report.rdlc");

                                        ReportDataSource rdc = new ReportDataSource("DataSet1", dttest);
                                        rptViewer1.LocalReport.DataSources.Add(rdc);


                                        rptViewer1.LocalReport.Refresh();

                                        bytes = rptViewer1.LocalReport.Render("PDF", strDeviceInfo, out strMimeType, out strEncoding, out strExtension, out strStreams, out warnings);



                                        using (FileStream fs = new FileStream(Server.MapPath("~/Content/doc/Invoice" + SelectedUser.Id + ".pdf"), FileMode.Create))
                                        {
                                            fs.Write(bytes, 0, bytes.Length);
                                        }

                                        invoicesend("Invoice" + SelectedUser.Id + ".pdf", bytes, SelectedUser.Email);


                                    }// send it to the client to download
                                    catch (Exception ex)
                                    {
                                        //fs.Write(bytes, 0, bytes.Length);
                                    }


                                }
                            }

                        }
                    }
                    else
                    {
                        var planManagerUpdate = abstractUserServices.UpdatePlanManager(ProjectSession.SetupUserID, user.PlanManagerId);
                        ProjectSession.SetupUserID = result.Item.Id;
                        ProjectSession.SetupUserName = result.Item.FirstName + " " + result.Item.LastName;
                        ProjectSession.UserID = ProjectSession.SetupUserID;
                        ProjectSession.LoginUserID = ProjectSession.SetupUserID;
                        ProjectSession.UserName = ProjectSession.SetupUserName;
                    }


                    TempData["displayModal"] = "myModal2";
                    //TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), result.Message);
                    return RedirectToAction("PDStep2", Pages.Controllers.AccountSetup, new { Area = "" });
                    //if (ProjectSession.SetupUserType == "1")
                    //{
                    //    return RedirectToAction(Actions.PMDSettings, Pages.Controllers.AccountSetup, new { Area = "" });
                    //}
                    //else
                    //{
                    //    return RedirectToAction(Actions.PaymentDetails, Pages.Controllers.AccountSetup, new { Area = "" });
                    //}
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                    TempData["errorMSG"] = result.Message;
                }
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "");
            }
            return RedirectToAction("PDInviteSettings", Pages.Controllers.AccountSetup, new { Area = "" });
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.AddUpdateComapanyUsers)]
        public ActionResult AddUpdateComapanyUsers(User user)
        {
            int Id = 0;
            try
            {
                user.CreatedBy = ProjectSession.SetupUserID;

                var userdata = abstractUserServices.User_ById(ProjectSession.SetupUserID);
                if (userdata.Item != null)
                {
                    user.Id = userdata.Item.Id;
                    user.Email = userdata.Item.Email;
                }
                SuccessResult<AbstractUser> result = abstractUserServices.User_Upsert(user);
                if (result != null && result.Code == 200 && result.Item != null)
                {
                    var emailVerifiedUserData = abstractUserServices.Users_EmailVerified(user.Email);
                    Id = result.Item.Id;

                    //var planManagerUpdate = abstractUserServices.UpdatePlanManager(ProjectSession.SetupUserID, user.PlanManagerId);
                    ProjectSession.SetupUserID = result.Item.Id;
                    ProjectSession.SetupUserName = result.Item.FirstName + " " + result.Item.LastName;
                    ProjectSession.UserID = ProjectSession.SetupUserID;
                    ProjectSession.LoginUserID = ProjectSession.SetupUserID;
                    ProjectSession.UserName = ProjectSession.SetupUserName;

                    TempData["displayModal"] = "myModal2";
                    //TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), result.Message);
                    return RedirectToAction("SignUpStep2", Pages.Controllers.PurpleLeopard, new { Area = "" });

                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                    TempData["errorMSG"] = result.Message;
                }
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "");
            }
            return RedirectToAction("SignUpStep2", Pages.Controllers.PurpleLeopard, new { Area = "" });
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.AddUpdateManagerUsers)]
        public ActionResult AddUpdateManagerUsers(User user)
        {
            int Id = 0;
            try
            {
                user.CreatedBy = ProjectSession.SetupUserID;

                var userdata = abstractUserServices.User_ById(ProjectSession.SetupUserID);
                if (userdata.Item != null)
                {
                    user.UserId = userdata.Item.Id;
                    user.Email = userdata.Item.Email;
                }
                SuccessResult<AbstractUser> result = abstractUserServices.ManagerUsers_Upsert(user);
                if (result != null && result.Code == 200 && result.Item != null)
                {
                    var emailVerifiedUserData = abstractUserServices.Users_EmailVerified(user.Email);
                    Id = result.Item.Id;

                    ProjectSession.ManagerUserCount = result.Item.ManagerUserCount;
                    if (user.ManagerType == 2)
                    {

                    }
                    else
                    {
                        var planManagerUpdate = abstractUserServices.UpdatePlanManager(result.Item.Id, user.PlanManagerId);
                        //ProjectSession.SetupUserID = result.Item.Id;
                        //ProjectSession.SetupUserName = result.Item.FirstName + " " + result.Item.LastName;
                        //ProjectSession.UserID = ProjectSession.SetupUserID;
                        //ProjectSession.LoginUserID = ProjectSession.SetupUserID;
                        //ProjectSession.UserName = ProjectSession.SetupUserName;
                    }


                    TempData["displayModal"] = "myModal";
                    //TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), result.Message);
                    return RedirectToAction("PDManagerStep2", Pages.Controllers.AccountSetup, new { Area = "" });
                    //if (ProjectSession.SetupUserType == "1")
                    //{
                    //    return RedirectToAction(Actions.PMDSettings, Pages.Controllers.AccountSetup, new { Area = "" });
                    //}
                    //else
                    //{
                    //    return RedirectToAction(Actions.PaymentDetails, Pages.Controllers.AccountSetup, new { Area = "" });
                    //}
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                    TempData["errorMSG"] = result.Message;
                }
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "");
            }
            return RedirectToAction("PDInviteSettings", Pages.Controllers.AccountSetup, new { Area = "" });
        }


        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.PlanManagerDetails)]
        public ActionResult PlanManagerDetails(PlanManager planManager)
        {
            int Id = 0;
            try
            {
                planManager.CreatedBy = ProjectSession.SetupUserID;
                planManager.Password = "1234567890";
                planManager.Type = 0;
                SuccessResult<AbstractPlanManager> result = abstractUserServices.PlanManager_Upsert(planManager);
                if (result != null && result.Code == 200 && result.Item != null)
                {
                    Id = result.Item.Id;
                    ProjectSession.SetupPlanManagerID = result.Item.Id;
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), result.Message);
                    return RedirectToAction(Actions.UserList, Pages.Controllers.AccountSetup, new { Area = "" });
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "");
            }
            return RedirectToAction(Actions.PMDSettings, Pages.Controllers.AccountSetup, new { Area = "" });
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.AddSubUsers)]
        public ActionResult AddSubUsers(User user)
        {
            int Id = 0;
            try
            {
                user.ParentId = ProjectSession.SetupUserID;
                SuccessResult<AbstractUser> result = abstractUserServices.SubUser_Upsert(user);
                if (result != null && result.Code == 200 && result.Item != null)
                {
                    Id = result.Item.Id;
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), result.Message);
                    return RedirectToAction(Actions.UserList, Pages.Controllers.AccountSetup, new { Area = "" });
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "");
            }
            //return RedirectToAction("EditUserProfile", Pages.Controllers.User, new { Area = "", userId = ConvertTo.Base64Encode(Convert.ToString(ProjectSession.UserID)) });
            return RedirectToAction("AddUser", Pages.Controllers.AccountSetup, new { Area = "" });
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.AddPaymentDetails)]
        public ActionResult AddPaymentDetails(User user)
        {
            int Id = 0;
            try
            {
                Stripe.StripeConfiguration.SetApiKey(Configurations.StripePublishableKey);
                Stripe.StripeConfiguration.ApiKey = Configurations.StripeSecretKey;

                var myCharge = new Stripe.ChargeCreateOptions();
                // always set these properties
                myCharge.Amount = ConvertTo.Long(44000);
                myCharge.Currency = "AUD";
                myCharge.ReceiptEmail = user.stripeEmail;
                myCharge.Description = "NDIS";
                myCharge.Source = user.stripeToken;
                myCharge.Capture = true;
                var chargeService = new Stripe.ChargeService();
                Charge stripeCharge = chargeService.Create(myCharge);

                user.UserId = ProjectSession.SetupUserID;
                user.TransactionDate = stripeCharge.Created;
                user.TransactionEndDate = stripeCharge.Created.AddYears(1).ToString();
                user.TransactionId = stripeCharge.BalanceTransactionId;
                user.CardNumber = stripeCharge.PaymentMethodDetails.Card.Last4;
                user.ExpiryDate = stripeCharge.PaymentMethodDetails.Card.ExpMonth.ToString() + '/' + stripeCharge.PaymentMethodDetails.Card.ExpYear;
                user.EmailForInvoice = user.stripeEmail;
                SuccessResult<AbstractUser> result = abstractUserServices.UsersPaymentDetails_Upsert(user);
                if (result != null && result.Code == 200 && result.Item != null)
                {
                    Id = result.Item.Id;
                    ProjectSession.SetupPaymentID = result.Item.Id;
                    //TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), result.Message);
                    if (ProjectSession.SetupUserID > 0)
                    {
                        ProjectSession.UserID = ProjectSession.SetupUserID;
                        ProjectSession.LoginUserID = ProjectSession.SetupUserID;
                        ProjectSession.UserName = ProjectSession.SetupUserName;
                        TempData["displayModal"] = "myModal2";
                    }
                    else
                    {
                        result.Code = 400;
                    }
                    return RedirectToAction(Actions.PaymentDetails, Pages.Controllers.AccountSetup, new { Area = "" });
                    //return RedirectToAction(Actions.Index, Pages.Controllers.AccountSetup, new { Area = "" });
                    //return RedirectToAction(Actions.UserList, Pages.Controllers.AccountSetup, new { Area = "" });
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "");
            }
            return RedirectToAction(Actions.PaymentDetails, Pages.Controllers.AccountSetup, new { Area = "" });
        }


        [HttpGet]
        [ActionName(Actions.DeleteUser)]
        public ActionResult DeleteUser(int UId)
        {
            var result = abstractUserServices.DeleteUser(UId, ProjectSession.SetupUserID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ActionName(Actions.UpdatePlanType)]
        public JsonResult UpdatePlanType(int Type)
        {
            ProjectSession.SetupUserType = ConvertTo.String(Type);
            SuccessResult<AbstractPlanManager> result = abstractUserServices.UpdatePlanType(ProjectSession.SetupUserID, Type);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.AddManageType)]
        public ActionResult AddManageType(User user)
        {
            int Id = 0;
            try
            {
                user.CreatedBy = ProjectSession.SetupUserID;
                ProjectSession.SetupUserType = ConvertTo.String(user.ManagerType);

                SuccessResult<AbstractUser> result = abstractUserServices.User_Upsert(user);
                if (result != null && result.Code == 200 && result.Item != null)
                {
                    Id = result.Item.Id;
                    ProjectSession.SetupUserID = result.Item.Id;
                    ProjectSession.SetupUserName = result.Item.FirstName + " " + result.Item.LastName;
                    //TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), result.Message);
                    return RedirectToAction(Actions.Location, Pages.Controllers.AccountSetup, new { Area = "" });


                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "");
            }
            return RedirectToAction(Actions.ManageType, Pages.Controllers.AccountSetup, new { Area = "", UserType = ProjectSession.SetupUserType });

        }


        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.AddLocation)]
        public ActionResult AddLocation(User user)
        {
            int Id = 0;
            try
            {
                user.CreatedBy = ProjectSession.SetupUserID;


                HttpClient client = new HttpClient();

                var stringTask = client.GetStringAsync("https://maps.googleapis.com/maps/api/geocode/json?address=" + user.PinCode + ",Aus&key=AIzaSyB97EhOhMCYhK1tZIofUkpKcGW61fdgKjg");
                var msg = stringTask;

                var s = JsonConvert.DeserializeObject<Root>(msg.Result);
                if (s.results.Count > 0)
                {
                    user.lat = s.results[0].geometry.location.lat;
                    user.lng = s.results[0].geometry.location.lng;
                }
                //user.Password = "1234567890";
                //user.ManagerType = Convert.ToInt32(ProjectSession.SetupUserType);
                SuccessResult<AbstractUser> result = abstractUserServices.User_Upsert(user);
                if (result != null && result.Code == 200 && result.Item != null)
                {
                    Id = result.Item.Id;
                    ProjectSession.SetupUserID = result.Item.Id;
                    ProjectSession.SetupUserName = result.Item.FirstName + " " + result.Item.LastName;
                    //TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), result.Message);
                    if (ProjectSession.SetupUserType == "1")
                    {
                        return RedirectToAction(Actions.PMDSettings, Pages.Controllers.AccountSetup, new { Area = "" });
                    }
                    else
                    {
                        return RedirectToAction(Actions.PaymentDetails, Pages.Controllers.AccountSetup, new { Area = "" });
                    }
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "");
            }
            return RedirectToAction(Actions.Location, Pages.Controllers.AccountSetup, new { Area = "", UserType = ProjectSession.SetupUserType });

        }

        [HttpPost]
        [ActionName(Actions.UpdatePlanManager)]
        public JsonResult UpdatePlanManager(int PlanManagerId)
        {
            SuccessResult<AbstractUser> result = abstractUserServices.UpdatePlanManager(ProjectSession.SetupUserID, PlanManagerId);
            if (result != null && result.Code == 200 && result.Item != null)
            {
                if (ProjectSession.SetupUserID > 0)
                {
                    ProjectSession.UserID = ProjectSession.SetupUserID;
                    ProjectSession.LoginUserID = ProjectSession.SetupUserID;
                    ProjectSession.UserName = ProjectSession.SetupUserName;
                }
                else
                {
                    result.Code = 400;
                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ActionName(Actions.Search)]
        public JsonResult Search(string Name)
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000000;
            var result = abstractUserServices.PlanManager_All(pageParam, Name).Values;
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ActionName(Actions.SendEmail)]
        public JsonResult SendEmail()
        {
            var userdata = abstractUserServices.User_ById(ProjectSession.SetupUserID).Item;
            string Email = userdata.Email;
            var sendGridClient = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"]);

            var sendGridMessage = new SendGridMessage();
            sendGridMessage.SetFrom(ConfigurationManager.AppSettings["SendGridFromEmail"], "My Request");
            sendGridMessage.AddTo(Email, "user test"); // user email
                                                       // sendGridMessage.AddTo("parthdobariya09@gmail.com", "user test"); // user email
            sendGridMessage.SetTemplateId("d-020a9fe7fbf74aab9bf80250f082ac3f");

            sendGridMessage.SetTemplateData(new EmailObj
            {
                UName = ProjectSession.SetupUserName,
                email = ConvertTo.Base64Encode(Convert.ToString(Email)),

            });

            var response = sendGridClient.SendEmailAsync(sendGridMessage).Result;
            var result = response;
            if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
            {

            }
            var modal = abstractUserServices.Users_Active(Email);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ActionName(Actions.SendManagerEmail)]
        public JsonResult SendManagerEmail()
        {
            var userdata = abstractUserServices.User_ById(ProjectSession.SetupUserID).Item;
            string Email = userdata.Email;
            var sendGridClient = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"]);

            var sendGridMessage = new SendGridMessage();
            sendGridMessage.SetFrom(ConfigurationManager.AppSettings["SendGridFromEmail"], "My Request");
            sendGridMessage.AddTo(Email, "user test"); // user email
                                                       // sendGridMessage.AddTo("parthdobariya09@gmail.com", "user test"); // user email
            sendGridMessage.SetTemplateId("d-020a9fe7fbf74aab9bf80250f082ac3f");

            sendGridMessage.SetTemplateData(new EmailObj
            {
                UName = ProjectSession.SetupUserName,
                email = ConvertTo.Base64Encode(Convert.ToString(Email)),

            });

            var response = sendGridClient.SendEmailAsync(sendGridMessage).Result;
            var result = response;
            if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
            {
                ProjectSession.LoginUserID = ProjectSession.SetupUserID;
                ProjectSession.UserID = ProjectSession.SetupUserID;
                ProjectSession.Email = userdata.Email;
                ProjectSession.SetupUserType = Convert.ToString(userdata.ManagerType);
                HttpCookie cookie = new HttpCookie("UserLogin");
                cookie.Values.Add("Id", userdata.Id.ToString());
                cookie.Values.Add("UserName", ProjectSession.UserName);
                cookie.Values.Add("Email", ProjectSession.Email);
                cookie.Expires = DateTime.Now.AddHours(1);
                Response.Cookies.Add(cookie);
            }
            var modal = abstractUserServices.Users_Active(Email);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ActionName(Actions.VerifyEmail)]
        public JsonResult VerifyEmail(string Email)
        {
            var modal = abstractUserServices.Users_IsEmailExist(Email);
            SuccessResult<AbstractUser> result = new SuccessResult<AbstractUser>();
            var userdata = abstractUserServices.User_ByEmail(Email, "");
            var isVerify = false;
            if (userdata.Item != null)
            {
                isVerify = userdata.Item.IsEmailVerified;
            }
            if (!modal && !isVerify)
            {
                var sendGridClient = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"]);

                var sendGridMessage = new SendGridMessage();
                sendGridMessage.SetFrom(ConfigurationManager.AppSettings["SendGridFromEmail"], "My Request");
                sendGridMessage.AddTo(Email, "user test"); // user email
                //sendGridMessage.AddTo("parthdobariya09@gmail.com", "user test"); // user email
                sendGridMessage.SetTemplateId("d-0bddf5d12367494f8dd47c39f83541c6");

                Random generator = new Random();
                String r = generator.Next(0, 1000000).ToString("D6");

                sendGridMessage.SetTemplateData(new EmailObj
                {
                    UName = ProjectSession.SetupUserName,
                    email = ConvertTo.Base64Encode(Convert.ToString(Email)),
                    Code = r
                });

                var response = sendGridClient.SendEmailAsync(sendGridMessage).Result;

                User user = new User();
                result.Item = user;
                result.Item.ECode = r;
                if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
                {
                    result.Code = 200;
                }
                else
                {
                    result.Code = 400;
                }
                return Json(result, JsonRequestBehavior.AllowGet);

            }
            else
            {
                result.Code = isVerify ? 401 : 403;
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [ActionName(Actions.GetState)]
        public ActionResult GetState(int Type, int PostCode, int StateId = 0)
        {
            List<ServicesRequest> items = new List<ServicesRequest>();


            if (Type == 0)
            {
                var model = abstractServices.State_AllByPostCode(PostCode);
                foreach (var item in model.Values)
                {
                    items.Add(new ServicesRequest() { Name = item.Name.ToString(), DrpId = item.Id.ToString() });
                }
            }
            else
            {
                var model = abstractServices.City_AllByPostCode(PostCode, StateId);
                foreach (var item in model.Values)
                {
                    items.Add(new ServicesRequest() { Name = item.Name.ToString(), DrpId = item.Name.ToString() });
                }
            }

            return Json(items, JsonRequestBehavior.AllowGet);
        }

        public void invoicesend(string FileName, byte[] bytes, string Email)
        {
            var sendGridClient = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"]);

            var sendGridMessage = new SendGridMessage();
            sendGridMessage.SetFrom(ConfigurationManager.AppSettings["SendGridFromEmail"], "My Request");
            //sendGridMessage.AddTo(Email, "user"); // user email
            sendGridMessage.AddTo("anthony@plannedservices.com.au", "user test"); // user email
            sendGridMessage.SetTemplateId("d-4970d7b1cb5a401782d894def1390958");

            // sendGridMessage.AddAttachment(Server.MapPath(FileName), null);

            var file = Convert.ToBase64String(bytes);
            sendGridMessage.AddAttachment(FileName, file);

            var response = sendGridClient.SendEmailAsync(sendGridMessage).Result;
            if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
            {

            }
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.Upload)]
        public ActionResult Upload()
        {
            if (Request != null)
            {
                HttpPostedFileBase file = Request.Files["UploadedFile"];
                if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
                {
                    string fileName = file.FileName;
                    string fileContentType = file.ContentType;
                    byte[] fileBytes = new byte[file.ContentLength];
                    var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                    User user = new User();

                    ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                    //using (ExcelPackage package = new ExcelPackage(new FileInfo(fileName), "password"))
                    //{
                    using (var package = new ExcelPackage(file.InputStream))
                    {

                        var currentSheet = package.Workbook.Worksheets;
                        var workSheet = currentSheet.First();
                        var noOfCol = workSheet.Dimension.End.Column;
                        var noOfRow = workSheet.Dimension.End.Row;
                        for (int rowIterator = 2; rowIterator <= noOfRow; rowIterator++)
                        {
                            user.FirstName = workSheet.Cells[rowIterator, 1].Value.ToString();
                            user.Email = workSheet.Cells[rowIterator, 2].Value.ToString();

                            var sendGridClient = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"]);

                            var sendGridMessage = new SendGridMessage();
                            sendGridMessage.SetFrom(ConfigurationManager.AppSettings["SendGridFromEmail"], "My Request");
                            sendGridMessage.AddTo(user.Email, "user test"); // user email
                            sendGridMessage.SetTemplateId("d-db542ec5595e451b8288106071ff8375");

                            sendGridMessage.SetTemplateData(new EmailObj
                            {
                                UName = user.FirstName,
                                email = ConvertTo.Base64Encode(Convert.ToString(user.Email)),

                            });

                            var response = sendGridClient.SendEmailAsync(sendGridMessage).Result;
                            if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
                            {

                            }
                        }
                    }
                }
            }
            return RedirectToAction("UploadCSV", Pages.Controllers.AccountSetup, new { Area = "" });
        }

        [HttpGet]
        [ActionName(Actions.RegistrationGroupByCategoryId)]
        public ActionResult RegistrationGroupByCategoryId(int Type, int RId, int CId, string GName = "", int BId = 0)
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 100000;
            List<ServicesRequest> items = new List<ServicesRequest>();
            string bName = "";
            if (BId == 1)
            {
                bName = "Capital";
            }
            else if (BId == 2)
            {
                bName = "Core";
            }
            else if (BId == 3)
            {
                bName = "Capacity Building";
            }

            if (Type == 1)
            {
                ViewBag.Section = 2;
                //var model = abstractServices.RegistrationGroupMaster_All(pageParam, "", GName);
                var model = abstractServices.RegistrationGroupMasterByCategoryId(pageParam, "", GName, bName, CId);
                foreach (var item in model.Values)
                {
                    items.Add(new ServicesRequest() { Name = item.Name.ToString(), DrpId = item.Id.ToString() });
                }
            }
            else if (Type == 0)
            {
                ViewBag.Section = 3;
                var model = abstractServices.SupportCategoryMasterByRegistrationGroupId(pageParam, "", GName, bName);
                foreach (var item in model.Values)
                {
                    items.Add(new ServicesRequest() { Name = item.Name.ToString(), DrpId = item.Id.ToString(), Description = ConvertTo.String(item.Description) });
                }
            }
            else if (Type == 3)
            {
                ViewBag.Section = 5;
                var model = abstractServices.SupportGoal_AllByBudget(pageParam, "", bName);
                foreach (var item in model.Values)
                {
                    items.Add(new ServicesRequest() { Name = item.OutcomeSort.ToString(), DrpId = item.OutcomeSort.ToString() });
                }
            }
            else
            {
                ViewBag.Section = 4;
                var model = abstractServices.SupportItemMasterByCategoryName(pageParam, "", CId, RId, GName, bName);
                foreach (var item in model.Values)
                {
                    items.Add(new ServicesRequest() { Name = item.Name.ToString(), DrpId = item.Id.ToString(), Description = ConvertTo.String(item.Description), Unit = ConvertTo.String(item.Unit) });
                }
            }

            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ActionName(Actions.GetTrialSendRequestProviderCount)]
        public JsonResult GetTrialSendRequestProviderCount(int RId, int Pcode)
        {
            var result = abstractServices.GetTrialSendRequestProviderCount(RId, Pcode);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public IList<SelectListItem> BindStateDropdown()
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 100000;
            var model = abstractLookupStateServices.LookupState_All(pageParam, "").Values;

            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in model)
            {
                items.Add(new SelectListItem() { Text = item.Name.ToString(), Value = item.Id.ToString() });
            }

            return items;
        }
        public IList<SelectListItem> BindStateCityDropdown(int Type, int PostCode, int StateId = 0)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            if (Type == 0)
            {
                var model = abstractServices.State_AllByPostCode(PostCode);
                foreach (var item in model.Values)
                {
                    items.Add(new SelectListItem() { Text = item.Name.ToString(), Value = item.Id.ToString() });
                }
            }
            else
            {
                var model = abstractServices.City_AllByPostCode(PostCode, StateId);
                foreach (var item in model.Values)
                {
                    items.Add(new SelectListItem() { Text = item.Name.ToString(), Value = item.Name.ToString() });
                }
            }

            return items;
        }
        public IList<ServicesRequest> BindBudgetMasterDropdown()
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 100000;
            var model = abstractServices.BudgetMaster_All(pageParam, "").Values;

            List<ServicesRequest> items = new List<ServicesRequest>();

            foreach (var item in model)
            {
                items.Add(new ServicesRequest() { Name = item.Name.ToString(), Description = item.Description.ToString(), Id = item.Id });
            }

            return items;
        }
        private class EmailObj
        {
            [JsonProperty("username")]
            public string username { get; set; }

            [JsonProperty("email")]
            public string email { get; set; }

            [JsonProperty("UName")]
            public string UName { get; set; }

            [JsonProperty("PName")]
            public string PName { get; set; }

            [JsonProperty("password")]
            public string password { get; set; }

            [JsonProperty("Code")]
            public string Code { get; set; }
        }
    }
}