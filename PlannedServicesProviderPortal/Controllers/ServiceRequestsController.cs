﻿using PlannedServices.Common;
using PlannedServices.Common.Paging;
using PlannedServices.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PlannedServicesProviderPortal.Infrastructure;
using PlannedServicesProviderPortal.Pages;
using PlannedServices.Entities.Contract;
using PlannedServices.Entities.V1;
using SendGrid.Helpers.Mail;
using static PlannedServicesProviderPortal.Infrastructure.Enums;
using SendGrid;
using System.Configuration;
using System.Dynamic;
using Newtonsoft.Json;

namespace PlannedServicesProviderPortal.Controllers
{
    public class ServiceRequestsController : BaseController
    {
        #region Fields
        private readonly AbstractServicesServices abstractServices;
        private readonly AbstractServicesRequestServices abstractServicesRequest;
        private readonly AbstractProviderServices abstractProvider;
        private readonly AbstractUserServices abstractUserServices;

        #endregion

        #region Ctor
        public ServiceRequestsController(AbstractServicesServices abstractServices,
            AbstractServicesRequestServices abstractServicesRequest, 
            AbstractProviderServices abstractProvider,
            AbstractUserServices abstractUserServices)
        {
            this.abstractServices = abstractServices;
            this.abstractServicesRequest = abstractServicesRequest;
            this.abstractProvider = abstractProvider;
            this.abstractUserServices = abstractUserServices;
        }
        #endregion

        #region Methods
        // GET: ServiceRequests

        public ActionResult Service()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];

            ServicesRequest servicesRequest = new ServicesRequest();
            var model = abstractServicesRequest.GetProviderChartData(ProjectSession.ProviderId);
            ViewBag.CData = model.Values;
            return View(servicesRequest);
        }
        public ActionResult AService()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            ServicesRequest servicesRequest = new ServicesRequest();
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000000;
            servicesRequest.ServicesRequests = abstractServicesRequest.ServicesRequest_All(pageParam, "", ProjectSession.ProviderId).Values;

            if (servicesRequest.ServicesRequests.Count > 0)
            {
                servicesRequest.SelectUser = abstractUserServices.User_ById(servicesRequest.ServicesRequests[0].UserId).Item;
                servicesRequest.ServicesRequests = servicesRequest.ServicesRequests.Where(x => x.Status == 1 && x.PServiceStatus != 2).ToList();
            }
            //ViewBag.ACount = servicesRequest.ServicesRequests[0].ActiveCount.ToString("00");
            //ViewBag.APCount = servicesRequest.ServicesRequests[0].ApporveCount.ToString("00");
            //ViewBag.CCount = servicesRequest.ServicesRequests[0].RejectedCount.ToString("00");
            return View(servicesRequest);
        }
        public ActionResult CService()
        {
            ServicesRequest servicesRequest = new ServicesRequest();
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000000;
            servicesRequest.ServicesRequests = abstractServicesRequest.ServicesRequest_All(pageParam, "", ProjectSession.ProviderId).Values;
            if(servicesRequest.ServicesRequests.Count > 0)
            {
                //servicesRequest.SelectUser = abstractUserServices.User_ById(servicesRequest.ServicesRequests[0].UserId).Item;
                servicesRequest.ServicesRequests = servicesRequest.ServicesRequests.Where(x => x.Status == 0 && x.PServiceStatus != 2).ToList();
            }
            //ViewBag.ACount = servicesRequest.ServicesRequests[0].ActiveCount.ToString("00");
            //ViewBag.APCount = servicesRequest.ServicesRequests[0].ApporveCount.ToString("00");
            //ViewBag.CCount = servicesRequest.ServicesRequests[0].RejectedCount.ToString("00");
            return View(servicesRequest);
        }

        public ActionResult RService()
        {
            ServicesRequest servicesRequest = new ServicesRequest();
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000000;
            servicesRequest.ServicesRequests = abstractServicesRequest.ClosedServiceRequestParticipateAll(pageParam, "", ProjectSession.ProviderId).Values;
            if(servicesRequest.ServicesRequests.Count > 0)
            {
                servicesRequest.SelectUser = abstractUserServices.User_ById(servicesRequest.ServicesRequests[0].UserId).Item;
                //servicesRequest.ServicesRequests = servicesRequest.ServicesRequests.Where(x => x.PServiceStatus == 2).ToList();
            }
            //ViewBag.ACount = servicesRequest.ServicesRequests[0].ActiveCount.ToString("00");
            //ViewBag.APCount = servicesRequest.ServicesRequests[0].ApporveCount.ToString("00");
            //ViewBag.CCount = servicesRequest.ServicesRequests[0].RejectedCount.ToString("00");
            return View(servicesRequest);
        }

        public ActionResult ServiceDetails(string ServiceId = "", string UserId = "")
        {
            ServicesRequest abstractServices = new ServicesRequest();
            if (ServiceId != null)
            {
                ViewBag.ReasonData = BindReasonData();
                int decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(ServiceId));
                int decryptedId1 = Convert.ToInt32(ConvertTo.Base64Decode(UserId));
                if (decryptedId > 0)
                {
                    PageParam pageParam = new PageParam();
                    pageParam.Offset = 0;
                    pageParam.Limit = 1000000;
                    abstractServices.ServicesRequests = this.abstractServicesRequest.ServicesRequest_ById(pageParam, "", decryptedId, ProjectSession.ProviderId).Values;
                    abstractServices.ServicesRequestCommunication = this.abstractServicesRequest.Communication_AllByProviderId(pageParam, "", 0, ProjectSession.ProviderId, abstractServices.ServicesRequests[0].ServiceRequestChildId).Values;
                    abstractServices.SelectUser = this.abstractUserServices.User_ById(decryptedId1).Item;
                    abstractServices.SelectProvider = this.abstractProvider.Provider_ById(ProjectSession.ProviderId, decryptedId1).Item;
                    abstractServices.SelectPlanManager = this.abstractUserServices.PlanManager_ById(abstractServices.SelectUser.PlanManagerId, 0).Item;
                    this.abstractServicesRequest.CommunicationIsRead(ProjectSession.ProviderId, decryptedId1, abstractServices.ServicesRequests[0].ServiceRequestChildId);
                    //ViewBag.IsRead = abstractServices.ServicesRequestCommunication.Find(x => x.IsRead == 0 && x.CustomerId == 0) != null ? true : false;

                    ViewBag.ServiceRequestId = decryptedId;
                    ViewBag.UserId = decryptedId1;
                    ViewBag.ServiceChildId = abstractServices.ServicesRequests[0].ServiceRequestChildId;
                    ViewBag.ACount = abstractServices.ServicesRequests[0].PActiveCount.ToString("00");
                    ViewBag.APCount = abstractServices.ServicesRequests[0].PApporveCount.ToString("00");
                    ViewBag.CCount = abstractServices.ServicesRequests[0].PRejectedCount.ToString("00");
                    
                    ViewBag.ShowBtn = false;
                    ViewBag.ShowEBtn = false;
                    ViewBag.ShowRBtn = false;
                    if (abstractServices.ServicesRequests[0].Status == 0)
                    {
                        ViewBag.ShowBtn = true;
                    }
                    else if (abstractServices.ServicesRequests[0].Status == 1)
                    {
                        ViewBag.ShowEBtn = true;
                    }
                    else if (abstractServices.ServicesRequests[0].Status == 2)
                    {
                        ViewBag.ShowRBtn = true;
                    }
                    else
                    {
                        ViewBag.ShowBtn = false;
                    }
                }
            }
            return View(abstractServices);
        }

        //public ActionResult ServiceDetails(string ServiceId = "", string UserId = "")
        //{
        //    ServicesRequest abstractServices = new ServicesRequest();
        //    if (ServiceId != null)
        //    {
        //        int decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(ServiceId));
        //        int decryptedId1 = Convert.ToInt32(ConvertTo.Base64Decode(UserId));
        //        if (decryptedId > 0)
        //        {
        //            PageParam pageParam = new PageParam();
        //            pageParam.Offset = 0;
        //            pageParam.Limit = 1000000;
        //            abstractServices.ServicesRequests = this.abstractServicesRequest.ServicesRequest_ById(pageParam, "", decryptedId, ProjectSession.ProviderId).Values;
        //            abstractServices.ServicesRequestCommunication = this.abstractServicesRequest.Communication_AllByProviderId(pageParam, "", 0, ProjectSession.ProviderId, abstractServices.ServicesRequests[0].ServiceRequestChildId).Values;
        //            abstractServices.SelectUser = this.abstractUserServices.User_ById(decryptedId1).Item;
        //            this.abstractServicesRequest.CommunicationIsRead(decryptedId1, 0, abstractServices.ServicesRequests[0].ServiceRequestChildId);

        //            ViewBag.ServiceRequestId = decryptedId;
        //            ViewBag.UserId = decryptedId1;
        //            ViewBag.ServiceChildId = abstractServices.ServicesRequests[0].ServiceRequestChildId;
        //            ViewBag.ACount = abstractServices.ServicesRequests[0].PActiveCount.ToString("00");
        //            ViewBag.APCount = abstractServices.ServicesRequests[0].PApporveCount.ToString("00");
        //            ViewBag.CCount = abstractServices.ServicesRequests[0].PRejectedCount.ToString("00");

        //                if (abstractServices.ServicesRequests[0].Status == 0)
        //                {
        //                    ViewBag.ShowBtn = true;
        //                }
        //                else
        //                {
        //                    ViewBag.ShowBtn = false;
        //                }


        //        }
        //    }
        //    return View(abstractServices);
        //}

        [HttpGet]
        [ActionName(Actions.UpdateProviderIsCapacity)]
        public ActionResult UpdateProviderIsCapacity(int SId, int IsCapacity)
        {
            var result = abstractServices.UpdateProviderIsCapacity(ProjectSession.ProviderId, SId, IsCapacity);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName(Actions.DeleteServiceRequestProvider)]
        public ActionResult DeleteServiceRequestProvider(int SId)
        {
            var result = abstractServices.DeleteServiceRequestProvider(SId, ProjectSession.ProviderId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
           
            List<SelectListItem> items = new List<SelectListItem>();
            ViewBag.RegistrationGroup = BindRegistrationGroupDropdown();
            ViewBag.SupportCategoryMaster = items;
            ViewBag.SupportItem = items;
            ViewBag.Distance = BindDistanceDropdown();
            ServicesRequest servicesRequest = new ServicesRequest();
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000000;
            servicesRequest.ServicesRequests = abstractServicesRequest.ServicesRequest_All(pageParam, "", ProjectSession.ProviderId).Values;
            return View(servicesRequest);
        }

        //[HttpPost]
        //[ActionName(Actions.ServiceDetails)]
        //public JsonResult ServiceDetails(string ServiceId = "", string UserId = "")
        //{
        //    AbstractServicesRequest abstractServices = new ServicesRequest();
        //    if (ServiceId != null)
        //    {
        //        int decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(ServiceId));
        //        int decryptedId1 = Convert.ToInt32(ConvertTo.Base64Decode(UserId));
        //        if (decryptedId > 0)
        //        {
        //            PageParam pageParam = new PageParam();
        //            pageParam.Offset = 0;
        //            pageParam.Limit = 1000000;
        //            abstractServices.ServicesRequests = this.abstractServicesRequest.ServicesRequest_ById(pageParam, "", decryptedId, ProjectSession.ProviderId).Values;
        //            abstractServices.ServicesRequestCommunication = this.abstractServicesRequest.Communication_AllByProviderId(pageParam, "", 0, ProjectSession.ProviderId, abstractServices.ServicesRequests[0].ServiceRequestChildId).Values;
        //            abstractServices.SelectUser = this.abstractUserServices.User_ById(decryptedId1).Item;
        //            ViewBag.ServiceRequestId = decryptedId;
        //            //ViewBag.ProviderId = decryptedId1;
        //           // ViewBag.Status = model[0].Status;
        //           // ViewBag.ServiceRequestChildId = model[0].ServiceRequestChildId;
        //            //ViewBag.ServiceData = model;
        //            //ViewBag.CommunicationData = model2;

        //        }
        //    }
        //    return Json(abstractServices, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        [ActionName(Actions.AddCommunication)]
        public JsonResult AddCommunication(string servicesRequest)
        {

            var s = JsonConvert.DeserializeObject<ServicesRequest>(servicesRequest);
            s.ProviderId = ProjectSession.ProviderId;
            var result = abstractServicesRequest.Communication_Upsert(s, null);
            if (result != null && result.Code == 200)
            {
                //var sendGridClient = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"]);

                //var sendGridMessage = new SendGridMessage();
                //sendGridMessage.SetFrom(ConfigurationManager.AppSettings["SendGridFromEmail"], "My Request");
                //sendGridMessage.AddTo(result.Item.Email, "user test"); // user email
                ////sendGridMessage.AddTo("parthdobariya09@gmail.com", "user test"); // user email
                //sendGridMessage.SetTemplateId("d-db493921a7854a2ea9c91cbbfe4cc555");

                //sendGridMessage.SetTemplateData(new RequestObj
                //{
                //    UName = result.Item.UserName,
                //    PName = result.Item.ProviderName,
                //    RName = result.Item.RegistrationGroupName,
                //    SNumber = result.Item.ServiceNumber

                //});

                //var response = sendGridClient.SendEmailAsync(sendGridMessage).Result;
                //if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
                //{

                //}
            }
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.SendRequest)]
        public ActionResult SendRequest(ServicesRequest model)
        {

            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 10;
            model.serviceObj.UserId = ProjectSession.UserID;
            model.serviceObj.RegistrationGroupId = model.RegistrationGroupId;
            model.serviceObj.CategoryId = model.CategoryId;
            model.serviceObj.Comment = model.Comment;
            model.serviceObj.BookingRequestedDate = model.BookingRequestedDate;
            //PagedList<AbstractProvider> result = abstractProviderServices.Provider_All(pageParam, "", 0, "", "", "");
            SuccessResult<AbstractServices> result2 = abstractServices.ServiceRequest_Upsert(model.serviceObj);
            if (result2 != null && result2.Code == 200 && result2.Item != null)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), "Your request has been sent to 32 Providers");
                var sendGridClient = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"]);

                var sendGridMessage = new SendGridMessage();
                sendGridMessage.SetFrom(ConfigurationManager.AppSettings["SendGridFromEmail"], "My Request");
                sendGridMessage.AddTo("anthony@plannedservices.com.au", "user test"); // user email
                sendGridMessage.SetTemplateId("d-54877013eb81443094b3901d66f8e67a");

                var response = sendGridClient.SendEmailAsync(sendGridMessage).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
                {

                }
            }
            else
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result2.Message);
            }
            //Services s = new Services();
            ///s.Provider = result.Values;
            // s.ServiceRequestId = result2.Item.Id;
            // TempData["ServiceData"] = s;
            return RedirectToAction(Actions.Index, Pages.Controllers.ServiceRequests);
        }

       

        //[HttpPost]
        //[ActionName(Actions.FilesUpload)]
        //public JsonResult FilesUpload(List<HttpPostedFileBase> files)
        //{
        //    var result = abstractServicesRequest.FilesUpload(files);
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}
        [HttpPost]
        public JsonResult Upload()
        {
            HttpFileCollectionBase files = Request.Files;
            var result = abstractServicesRequest.FilesUpload(files);
            //for (int i = 0; i < files.Count; i++)
            //{
            //    HttpPostedFileBase file = files[i];

            //}

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public IList<SelectListItem> BindRegistrationGroupDropdown()
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 100000;
            var model = abstractServices.RegistrationGroupMaster_All(pageParam, "","").Values;

            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in model)
            {
                items.Add(new SelectListItem() { Text = item.Name.ToString(), Value = item.Id.ToString() });
            }

            return items;
        }

        public IList<SelectListItem> BindReasonData()
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 100000;
            var model = abstractServices.Reason_All(pageParam, "").Values;

            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in model)
            {
                items.Add(new SelectListItem() { Text = item.Name.ToString(), Value = item.Id.ToString() });
            }

            return items;
        }
        public IList<SelectListItem> BindDistanceDropdown()
        {

            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "5km", Value = "5km" });
            items.Add(new SelectListItem() { Text = "10km", Value = "10km" });
            items.Add(new SelectListItem() { Text = "15km", Value = "15km" });
            items.Add(new SelectListItem() { Text = "20km", Value = "20km" });

            return items;
        }

        private class EmailObj
        {
            [JsonProperty("name")]
            public string name { get; set; }

            [JsonProperty("status")]
            public string status { get; set; }

            [JsonProperty("message")]
            public string message { get; set; }
        }
        private class RequestObj
        {
            [JsonProperty("UName")]
            public string UName { get; set; }

            [JsonProperty("PName")]
            public string PName { get; set; }

            [JsonProperty("RName")]
            public string RName { get; set; }

            [JsonProperty("Address")]
            public string Address { get; set; }

            [JsonProperty("email")]
            public string email { get; set; }

            [JsonProperty("SNumber")]
            public string SNumber { get; set; }

            [JsonProperty("PCount")]
            public string PCount { get; set; }
        }

        #endregion
    }
}