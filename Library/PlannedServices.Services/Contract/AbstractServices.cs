﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlannedServices.Common;
using PlannedServices.Common.Paging;
using PlannedServices.Data.Contract;
using PlannedServices.Entities.Contract;
using PlannedServices.Entities.V1;


namespace PlannedServices.Services.Contract
{
    public abstract class AbstractServicesServices
    {
        public abstract SuccessResult<AbstractServices> Services_Upsert(AbstractServices abstractServices);
        public abstract SuccessResult<AbstractServices> ServiceRequest_Upsert(AbstractServices abstractServices);
        public abstract SuccessResult<AbstractServices> ServiceRequestChild_Upsert(AbstractServices abstractServices);
        public abstract SuccessResult<AbstractServices> Services_ById(long Id);
        public abstract SuccessResult<AbstractServices> GetUserInfo_ByServiceRequestId(long Id);
        public abstract PagedList<AbstractServices> Services_All(PageParam pageParam, string search);
        public abstract PagedList<AbstractServices> GetServiceRequestProviderCount(int RId, int UId);
        public abstract PagedList<AbstractServices> GetTrialSendRequestProviderCount(int RId, int UId);
        public abstract PagedList<AbstractServices> SupportCategoryMasterByRegistrationGroupId(PageParam pageParam, string search, string GName, string BudgetName);
        public abstract PagedList<AbstractServices> RegistrationGroupMaster_All(PageParam pageParam, string search, string GName,string Type="");
        public abstract PagedList<AbstractServices> RegistrationGroupMaster_ByProviderId(int ProviderId);
        public abstract PagedList<AbstractServices> SupportGoal_AllByBudget(PageParam pageParam, string search, string BudgetName);
        public abstract PagedList<AbstractServices> BudgetMaster_All(PageParam pageParam, string search);
        public abstract PagedList<AbstractServices> State_AllByPostCode(int PostCode);
        public abstract PagedList<AbstractServices> ServiceRequestChild_ByServiceRequestId(int ServiceRequestId);
        public abstract PagedList<AbstractServices> City_AllByPostCode(int PostCode, int StateId);
        public abstract PagedList<AbstractServices> Reason_All(PageParam pageParam, string search);
        public abstract PagedList<AbstractServices> SupportItemMasterByCategoryName(PageParam pageParam, string search, int CategoryId, int RegistrationGroupId, string GName, string BudgetName);
        public abstract PagedList<AbstractServices> RegistrationGroupMasterByCategoryId(PageParam pageParam, string search, string GName, string BudgetName, int CategoryId);

        public abstract SuccessResult<AbstractServices> ChangeServiceStatus(int ServiceRequestId,int ProviderId, int Type,int IsApproved, string ReasonId);
        public abstract SuccessResult<AbstractServices> UpdateIsHideContactDetails(int ServiceRequestId,int IsHideContactDetails);
        public abstract SuccessResult<AbstractServices> UpdateProviderIsCapacity(int ProviderId, int ServiceRequestId,int IsCapacity);

        public abstract bool DeleteServiceRequest(int id);
        public abstract bool DeleteServiceRequestProvider(int id, int pid);

    }
}
