﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PlannedServices.Common;
using PlannedServices.Common.Paging;
using PlannedServices.Services.Contract;
using PlannedServicesProviderPortal.Infrastructure;

namespace PlannedServicesProviderPortal.Controllers
{
    public class ResourcesController : BaseController
    {
        private readonly AbstractResourcesServices abstractResources;
        public ResourcesController(AbstractResourcesServices abstractResources)
        {
            this.abstractResources = abstractResources;
        }

        // GET: Dashboard
        public ActionResult Index()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];

            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000000;
            var model = abstractResources.Resources_All(pageParam, "");
            return View(model);
        }
    }
}