﻿using OfficeOpenXml;
using PlannedServices.Common;
using PlannedServices.Services.Contract;
using PlannedServicesAdmin.Pages;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PlannedServicesAdmin.Controllers
{
    public class SalesInvoiceController : Controller
    {
        #region Fields
        private readonly AbstractUserServices abstractUserServices;
        private readonly AbstractProviderServices abstractProviderServices;
        #endregion

        #region Ctor
        public SalesInvoiceController(AbstractUserServices abstractUserServices, AbstractProviderServices abstractProviderServices)
        {
            this.abstractUserServices = abstractUserServices;
            this.abstractProviderServices = abstractProviderServices;
        }
        #endregion
        // GET: SalesInvoice
        public ActionResult Index()
        {
            if (ProjectSession.ExpoUserID > 0)
            {
                if (Session["DownloadExcel_FileManager"] != null)
                {
                    byte[] data = Session["DownloadExcel_FileManager"] as byte[];
                    Session["DownloadExcel_FileManager"] = null;
                    return File(data, "application/octet-stream", "EXPORTDATA.xlsx");
                }
                else
                {
                    return View();
                }
            }
            else
            {
                return RedirectToAction(Actions.ExportDataSignIn, Pages.Controllers.Authentication);
            }
            //return View();
        }
        public ActionResult ServiceRequestData()
        {
            if (ProjectSession.ExpoUserID > 0)
            {
                if (Session["DownloadExcel_FileManager1"] != null)
                {
                    byte[] data = Session["DownloadExcel_FileManager1"] as byte[];
                    Session["DownloadExcel_FileManager1"] = null;
                    return File(data, "application/octet-stream", "EXPORTREQUESTDATA.xlsx");
                }
                else
                {
                    return View();
                }
            }
            else
            {
                return RedirectToAction(Actions.ExportDataSignIn, Pages.Controllers.Authentication);
            }
            //return View();
        }


        [HttpGet]
        [ActionName(Actions.GetUserByCreatedDate)]
        public ActionResult GetUserByCreatedDate(string SDate, string EDate, int Type)
        {
            if (Type == 1)
            {
                var result = abstractUserServices.GetUserByCreatedDate(SDate, EDate).Values;
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = abstractProviderServices.GetProviderByCreatedDate(SDate, EDate).Values;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        [ActionName(Actions.GetUserRequestByCreatedDate)]
        public ActionResult GetUserRequestByCreatedDate(string SDate, string EDate)
        {
            var result = abstractUserServices.GetUserServiceRequestData(SDate, EDate).Values;
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        [ActionName(Actions.DownloadExcel)]
        public ActionResult DownloadExcel(string SDate, string EDate, int Type)
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            if (Type == 1)
            {
                var result = abstractUserServices.GetUserByCreatedDate(SDate, EDate).Values;

                ExcelPackage Ep = new ExcelPackage();
                ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("Report");
                Sheet.Cells["A1"].Value = "Contact Name";
                Sheet.Cells["B1"].Value = "Email Address";
                Sheet.Cells["C1"].Value = "Address";
                Sheet.Cells["D1"].Value = "PlanManager Name";
                Sheet.Cells["E1"].Value = "PlanManager Email";
                Sheet.Cells["F1"].Value = "PlanManager Address";
                Sheet.Cells["G1"].Value = "Nominee Name";
                Sheet.Cells["H1"].Value = "Reference";
                Sheet.Cells["I1"].Value = "Service Request Count";
                Sheet.Cells["J1"].Value = "Billing Type";
                //Sheet.Cells["F1"].Value = "InvoiceDate";
                //Sheet.Cells["G1"].Value = "DueDate";
                //Sheet.Cells["H1"].Value = "InventoryItem Code";
                //Sheet.Cells["I1"].Value = "Description";
                //Sheet.Cells["J1"].Value = "Quantity";
                //Sheet.Cells["K1"].Value = "UnitAmount";
                //Sheet.Cells["L1"].Value = "Discount";
                //Sheet.Cells["M1"].Value = "AccountCode";
                //Sheet.Cells["N1"].Value = "TaxType";
                //Sheet.Cells["O1"].Value = "TrackingName1";
                //Sheet.Cells["P1"].Value = "TrackingOption1";
                //Sheet.Cells["Q1"].Value = "TrackingName2";
                //Sheet.Cells["R1"].Value = "TrackingOption2";
                //Sheet.Cells["S1"].Value = "Currency";
                //Sheet.Cells["T1"].Value = "BrandingTheme";
                int row = 2;
                foreach (var item in result)
                {

                    Sheet.Cells[string.Format("A{0}", row)].Value = item.NDISReferenceNumber != null ? item.FirstName + " " + item.LastName + " NDIS: " + item.NDISReferenceNumber : item.FirstName + " " + item.LastName;
                    Sheet.Cells[string.Format("B{0}", row)].Value = item.Email;
                    Sheet.Cells[string.Format("C{0}", row)].Value = item.Surburb + ", " + item.StateName + ", " + item.PinCode;
                    Sheet.Cells[string.Format("D{0}", row)].Value = item.PlanManagerName != null ? "C/O " + item.PlanManagerName : "";
                    Sheet.Cells[string.Format("E{0}", row)].Value = item.PlanManagerEmail;
                    Sheet.Cells[string.Format("F{0}", row)].Value = item.PlanManagerAddress;
                    Sheet.Cells[string.Format("G{0}", row)].Value = item.NomineeFirstName != null ? "Nominee: " + item.NomineeFirstName : "";
                    Sheet.Cells[string.Format("H{0}", row)].Value = item.FirstName.Substring(0, 1) + item.LastName.Substring(0, 1) + String.Format("{0:D4}", item.Id);
                    Sheet.Cells[string.Format("I{0}", row)].Value = item.ServiceRequestCount;
                    Sheet.Cells[string.Format("J{0}", row)].Value = item.PlanText;
                    row++;
                }


                Sheet.Cells["A:AZ"].AutoFitColumns();
                Session["DownloadExcel_FileManager"] = Ep.GetAsByteArray();
                return Json("", JsonRequestBehavior.AllowGet);

            }
            else
            {
                var result = abstractProviderServices.GetProviderByCreatedDate(SDate, EDate).Values;

                ExcelPackage Ep = new ExcelPackage();
                ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("Report");
                Sheet.Cells["A1"].Value = "Company Name";
                Sheet.Cells["B1"].Value = "Email Address";
                Sheet.Cells["C1"].Value = "Address";
                Sheet.Cells["D1"].Value = "ABN";
                Sheet.Cells["E1"].Value = "NDIS Reference Number";
                Sheet.Cells["F1"].Value = "Reference";
                int row = 2;
                foreach (var item in result)
                {

                    Sheet.Cells[string.Format("A{0}", row)].Value = item.CompanyName != null ? item.CompanyName : item.Name + " " + item.LastName;
                    Sheet.Cells[string.Format("B{0}", row)].Value = item.Email;
                    Sheet.Cells[string.Format("C{0}", row)].Value = item.Surburb + ", " + item.StateName + ", " + item.PinCode;
                    Sheet.Cells[string.Format("D{0}", row)].Value = item.ABN;
                    Sheet.Cells[string.Format("E{0}", row)].Value = item.NDISReferenceNumber;
                    Sheet.Cells[string.Format("F{0}", row)].Value = item.Id;
                    row++;
                }


                Sheet.Cells["A:AZ"].AutoFitColumns();
                Session["DownloadExcel_FileManager"] = Ep.GetAsByteArray();
                return Json("", JsonRequestBehavior.AllowGet);

            }



        }
        //}


        [HttpPost]
        [ActionName(Actions.DownloadRequestExcel)]
        public ActionResult DownloadRequestExcel(string SDate, string EDate)
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;


            var result = abstractUserServices.GetUserServiceRequestData(SDate, EDate).Values;

            ExcelPackage Ep = new ExcelPackage();
            ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("Report");
            Sheet.Cells["A1"].Value = "Contact Name";
            Sheet.Cells["B1"].Value = "Email Address";
            Sheet.Cells["C1"].Value = "Address";
            Sheet.Cells["D1"].Value = "PlanManager Name";
            Sheet.Cells["E1"].Value = "PlanManager Email";
            Sheet.Cells["F1"].Value = "PlanManager Address";
            Sheet.Cells["G1"].Value = "Reference";
            Sheet.Cells["H1"].Value = "Created Date";

            int row = 2;
            foreach (var item in result)
            {

                Sheet.Cells[string.Format("A{0}", row)].Value = item.NDISReferenceNumber != null ? item.FirstName + " " + item.LastName + " NDIS: " + item.NDISReferenceNumber : item.FirstName + " " + item.LastName;
                Sheet.Cells[string.Format("B{0}", row)].Value = item.Email;
                Sheet.Cells[string.Format("C{0}", row)].Value = item.Surburb + ", " + item.StateName + ", " + item.PinCode;
                Sheet.Cells[string.Format("D{0}", row)].Value = item.PlanManagerName != null ? "C/O " + item.PlanManagerName : "";
                Sheet.Cells[string.Format("E{0}", row)].Value = item.PlanManagerEmail;
                Sheet.Cells[string.Format("F{0}", row)].Value = item.PlanManagerAddress;
                Sheet.Cells[string.Format("G{0}", row)].Value = item.FirstName != null ? item.FirstName.Substring(0, 1) + item.LastName.Substring(0, 1) + String.Format("{0:D4}", item.Id) : "";
                Sheet.Cells[string.Format("H{0}", row)].Value = item.CreatedDateStr;
                row++;
            }


            Sheet.Cells["A:AZ"].AutoFitColumns();
            Session["DownloadExcel_FileManager1"] = Ep.GetAsByteArray();
            return Json("", JsonRequestBehavior.AllowGet);



        }

    }
}