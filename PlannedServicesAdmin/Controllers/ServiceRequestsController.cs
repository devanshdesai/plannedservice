﻿using Microsoft.Reporting.WebForms;
using PlannedServices.Common;
using PlannedServices.Common.Paging;
using PlannedServices.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PlannedServicesAdmin.Infrastructure;
using PlannedServicesAdmin.Pages;
using PlannedServices.Entities.Contract;
using PlannedServices.Entities.V1;
using SendGrid.Helpers.Mail;
using static PlannedServicesAdmin.Infrastructure.Enums;
using SendGrid;
using System.Configuration;
using System.Dynamic;
using Newtonsoft.Json;
using System.Data;
using System.IO;
using Stripe;

namespace PlannedServicesAdmin.Controllers
{
    public class ServiceRequestsController : BaseController
    {
        #region Fields
        private readonly AbstractServicesServices abstractServices;
        private readonly AbstractServicesRequestServices abstractServicesRequest;
        private readonly AbstractProviderServices abstractProvider;
        private readonly AbstractUserServices abstractUserServices;

        #endregion

        #region Ctor
        public ServiceRequestsController(AbstractServicesServices abstractServices,
            AbstractServicesRequestServices abstractServicesRequest, AbstractProviderServices abstractProvider,
            AbstractUserServices abstractUserServices
            )
        {
            this.abstractServices = abstractServices;
            this.abstractServicesRequest = abstractServicesRequest;
            this.abstractProvider = abstractProvider;
            this.abstractUserServices = abstractUserServices;
        }
        #endregion

        #region Methods
        // GET: ServiceRequests
        public ActionResult Service()
        {
            //ServicesRequest servicesRequest = new ServicesRequest();
            //var model = abstractServicesRequest.GetParticipantsChartData(ProjectSession.UserID);
            //ViewBag.CData = model.Values;

            //if (ProjectSession.LoginUserID == ProjectSession.UserID)
            //{
            //    servicesRequest.ServicesRequests = abstractServicesRequest.ServiceRequests_AllByCustomerId(ProjectSession.UserID).Values;
            //}
            //else
            //{
            //    servicesRequest.ServicesRequests = abstractServicesRequest.ServiceRequests_AllByCustomerId(ProjectSession.LoginUserID).Values;
            //}
            //if (servicesRequest.ServicesRequests.Count > 0)
            //{
            //    ViewBag.ACount = servicesRequest.ServicesRequests[0].ActiveCount.ToString("00");
            //    ViewBag.CCount = servicesRequest.ServicesRequests[0].CompletedCount.ToString("00");
            //}
            //else
            //{
            //    ViewBag.ACount = "00";
            //    ViewBag.CCount = "00";
            //}


            ServicesRequest servicesRequest = new ServicesRequest();

            servicesRequest.ServicesRequests = abstractServicesRequest.ServiceRequests_AllByCustomerId(ProjectSession.UserID).Values;
            if (servicesRequest.ServicesRequests.Count > 0)
            {
                ViewBag.ACount = servicesRequest.ServicesRequests[0].ActiveCount.ToString("00");
                ViewBag.CCount = servicesRequest.ServicesRequests[0].CompletedCount.ToString("00");
            }
            else
            {
                ViewBag.ACount = "00";
                ViewBag.CCount = "00";
            }
            return View(servicesRequest);
        }
        public ActionResult AService()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            ServicesRequest servicesRequest = new ServicesRequest();

            servicesRequest.ServicesRequests = abstractServicesRequest.ServiceRequests_AllByCustomerId(ProjectSession.UserID).Values;
            //if (ProjectSession.LoginUserID == ProjectSession.UserID)
            //{
            //    servicesRequest.ServicesRequests = abstractServicesRequest.ServiceRequests_AllByCustomerId(ProjectSession.UserID).Values;
            //}
            //else
            //{
            //    servicesRequest.ServicesRequests = abstractServicesRequest.ServiceRequests_AllByCustomerId(ProjectSession.LoginUserID).Values;
            //}
            if (servicesRequest.ServicesRequests.Count > 0)
            {
                ViewBag.ACount = servicesRequest.ServicesRequests[0].ActiveCount.ToString("00");
                ViewBag.CCount = servicesRequest.ServicesRequests[0].CompletedCount.ToString("00");
            }
            else
            {
                ViewBag.ACount = "00";
                ViewBag.CCount = "00";
            }
            return View(servicesRequest);
        }
        public ActionResult CService()
        {
            ServicesRequest servicesRequest = new ServicesRequest();
            servicesRequest.ServicesRequests = abstractServicesRequest.ServiceRequests_AllByCustomerId(ProjectSession.UserID).Values;
            //if (ProjectSession.LoginUserID == ProjectSession.UserID)
            //{
            //    servicesRequest.ServicesRequests = abstractServicesRequest.ServiceRequests_AllByCustomerId(ProjectSession.UserID).Values;
            //}
            //else
            //{
            //    servicesRequest.ServicesRequests = abstractServicesRequest.ServiceRequests_AllByCustomerId(ProjectSession.LoginUserID).Values;
            //}
            if (servicesRequest.ServicesRequests.Count > 0)
            {
                for (var i = 0; i < servicesRequest.ServicesRequests.Count; i++)
                {
                    List<AbstractProvider> Plist = new List<AbstractProvider>();

                    var c = servicesRequest.ServicesRequests[i].Providers.Find(x => x.Status == 1);
                    if (c != null)
                    {
                        Plist.Add(servicesRequest.ServicesRequests[i].Providers.Find(x => x.Status == 1));
                        servicesRequest.ServicesRequests[i].Providers = Plist;
                    }
                    else
                    {
                        servicesRequest.ServicesRequests[i].Providers = Plist;
                    }
                }
                ViewBag.ACount = servicesRequest.ServicesRequests[0].ActiveCount.ToString("00");
                ViewBag.CCount = servicesRequest.ServicesRequests[0].CompletedCount.ToString("00");
            }
            else
            {
                ViewBag.ACount = "00";
                ViewBag.CCount = "00";
            }
            return View(servicesRequest);
        }
        public ActionResult RService()
        {
            ServicesRequest servicesRequest = new ServicesRequest();
            servicesRequest.ServicesRequests = abstractServicesRequest.ClosedServiceRequestByCustomerId(ProjectSession.UserID).Values;
            //if (servicesRequest.ServicesRequests.Count > 0)
            //{
            //    for (var i = 0; i < servicesRequest.ServicesRequests.Count; i++)
            //    {
            //        List<AbstractProvider> Plist = new List<AbstractProvider>();

            //        var c = servicesRequest.ServicesRequests[i].Providers.Find(x => x.Status == 2);
            //        if (c != null)
            //        {
            //            Plist.Add(servicesRequest.ServicesRequests[i].Providers.Find(x => x.Status == 2));
            //            servicesRequest.ServicesRequests[i].Providers = Plist;
            //        }
            //        else
            //        {
            //            servicesRequest.ServicesRequests[i].Providers = Plist;
            //        }
            //    }
            //    ViewBag.ACount = servicesRequest.ServicesRequests[0].ActiveCount.ToString("00");
            //    ViewBag.CCount = servicesRequest.ServicesRequests[0].CompletedCount.ToString("00");
            //}
            //else
            //{
            //    ViewBag.ACount = "00";
            //    ViewBag.CCount = "00";
            //}
            return View(servicesRequest);
        }
        public ActionResult PService(string ServiceId = "",int IsClosed =0)
        {
            PagedList<AbstractProvider> Provider = new PagedList<AbstractProvider>();
            if (ServiceId != null)
            {
                int decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(ServiceId));
                if (decryptedId > 0)
                {
                    ViewBag.ServiceRequestId = decryptedId;
                    PageParam pageParam = new PageParam();
                    pageParam.Offset = 0;
                    pageParam.Limit = 10000;
                    ViewBag.IsClosed = IsClosed;
                    if (IsClosed == 1)
                    {
                        Provider = abstractProvider.ClosedServiceRequestProviderAll(pageParam, "", decryptedId);
                    }
                    else
                    {
                        Provider = abstractProvider.Provider_AllByServiceRequestId(pageParam, "", decryptedId);
                    }
                    if (Provider.Values.Count > 0)
                    {

                        ViewBag.ServiceNumber = Provider.Values[0].ServiceNumber;
                        ViewBag.RegistrationGroupName = Provider.Values[0].RegistrationGroupName;
                    }
                    else
                    {
                        ViewBag.ServiceNumber = "0";
                        ViewBag.RegistrationGroupName = "";
                    }
                    //ViewBag.ACount = Provider.Values[0].ActiveCount.ToString("00");
                    //ViewBag.CCount = Provider.Values[0].CompletedCount.ToString("00");
                    //ViewBag.ProviderCount = Provider.Values[0].ProviderCount.ToString("00"); 
                }

            }
            return View(Provider);
        }
        public ActionResult ServiceDetails(string ServiceId = "", string ProviderId = "")
        {
            ServicesRequest abstractServices = new ServicesRequest();
            if (ServiceId != null)
            {
                ViewBag.ReasonData = BindReasonData();
                int decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(ServiceId));
                int decryptedId1 = Convert.ToInt32(ConvertTo.Base64Decode(ProviderId));
                if (decryptedId > 0)
                {
                    PageParam pageParam = new PageParam();
                    pageParam.Offset = 0;
                    pageParam.Limit = 1000000;
                    abstractServices.ServicesRequests = this.abstractServicesRequest.ServicesRequest_ById(pageParam, "", decryptedId, decryptedId1).Values;
                    abstractServices.ServicesRequestCommunication = this.abstractServicesRequest.Communication_AllByProviderId(pageParam, "", 0, decryptedId1, abstractServices.ServicesRequests[0].ServiceRequestChildId).Values;
                    abstractServices.SelectProvider = this.abstractProvider.Provider_ById(decryptedId1, ProjectSession.UserID).Item;
                    abstractServices.SelectUser = this.abstractUserServices.User_ById(ProjectSession.UserID).Item;
                    abstractServices.SelectPlanManager = this.abstractUserServices.PlanManager_ById(decryptedId1, 1).Item;
                    this.abstractServicesRequest.CommunicationIsRead(decryptedId1, 0, abstractServices.ServicesRequests[0].ServiceRequestChildId);

                    ViewBag.IsRead = abstractServices.ServicesRequestCommunication.Find(x => x.IsRead == 0 && x.CustomerId == 0) != null ? true : false;

                    ViewBag.ServiceRequestId = decryptedId;
                    ViewBag.ProviderId = decryptedId1;
                    ViewBag.ServiceChildId = abstractServices.ServicesRequests[0].ServiceRequestChildId;
                    ViewBag.ACount = abstractServices.ServicesRequests[0].ActiveCount.ToString("00");
                    ViewBag.CCount = abstractServices.ServicesRequests[0].CompletedCount.ToString("00");
                    
                    ViewBag.CompanyDoc = abstractServices.SelectProvider.FileURL !=null ? abstractServices.SelectProvider.FileURL.Split(',') : null;
                    ViewBag.CompanyDocFileName = abstractServices.SelectProvider.FileName !=null ? abstractServices.SelectProvider.FileName.Split(',') : null;
                    if (abstractServices.ServicesRequests[0].MainStatus == 0)
                    {
                        if (abstractServices.ServicesRequests[0].Status == 1)
                        {
                            ViewBag.ShowBtn = true;
                            ViewBag.ShowRBtn = false;
                        }
                        else if (abstractServices.ServicesRequests[0].Status == 2)
                        {
                            ViewBag.ShowBtn = false;
                            ViewBag.ShowRBtn = true;
                        }
                        else
                        {
                            ViewBag.ShowBtn = false;
                            ViewBag.ShowRBtn = false;
                        }
                    }
                    else
                    {
                        ViewBag.ShowBtn = false;
                        ViewBag.ShowRBtn = true;
                        if (abstractServices.ServicesRequests[0].MainStatus == 2)
                        {
                            ViewBag.ShowRBtn = false;
                        }
                    }

                }
            }
            return View(abstractServices);
        }
        public ActionResult NService()
        {
            ViewBag.Section = 1;
            List<SelectListItem> items = new List<SelectListItem>();
            ViewBag.BudgetMaster = BindBudgetMasterDropdown();
            ViewBag.RegistrationGroup = items;
            ViewBag.SupportCategoryMaster = items;
            ViewBag.SupportItem = items;
            ViewBag.userdata = abstractUserServices.UsersPaymentDetails_ById(ProjectSession.UserID).Item;
            return View();
        }

        public ActionResult Index()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];

            List<SelectListItem> items = new List<SelectListItem>();
            ViewBag.BudgetMaster = BindBudgetMasterDropdown();
            ViewBag.RegistrationGroup = items;
            ViewBag.SupportCategoryMaster = items;
            ViewBag.SupportItem = items;
            ViewBag.Distance = BindDistanceDropdown();
            ServicesRequest servicesRequest = new ServicesRequest();
            if (ProjectSession.LoginUserID == ProjectSession.UserID)
            {
                servicesRequest.ServicesRequests = abstractServicesRequest.ServiceRequests_AllByCustomerId(ProjectSession.UserID).Values;
            }
            else
            {
                servicesRequest.ServicesRequests = abstractServicesRequest.ServiceRequests_AllByCustomerId(ProjectSession.LoginUserID).Values;
            }
            return View(servicesRequest);
        }

        //[HttpPost]
        //[ActionName(Actions.ServiceDetails)]
        //public JsonResult ServiceDetails(string ServiceId = "", string ProviderId = "")
        //{
        //    AbstractServicesRequest abstractServices = new ServicesRequest();
        //    if (ServiceId != null)
        //    {
        //        int decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(ServiceId));
        //        int decryptedId1 = Convert.ToInt32(ConvertTo.Base64Decode(ProviderId));
        //        if (decryptedId > 0)
        //        {
        //            PageParam pageParam = new PageParam();
        //            pageParam.Offset = 0;
        //            pageParam.Limit = 1000000;
        //            abstractServices.ServicesRequests = this.abstractServicesRequest.ServicesRequest_ById(pageParam, "", decryptedId, decryptedId1).Values;
        //            abstractServices.ServicesRequestCommunication = this.abstractServicesRequest.Communication_AllByProviderId(pageParam, "", 0, decryptedId1, abstractServices.ServicesRequests[0].ServiceRequestChildId).Values;
        //            abstractServices.SelectProvider = this.abstractProvider.Provider_ById(decryptedId1).Item;
        //            ViewBag.ServiceRequestId = decryptedId;
        //            ViewBag.ProviderId = decryptedId1;
        //            // ViewBag.Status = model[0].Status;
        //            // ViewBag.ServiceRequestChildId = model[0].ServiceRequestChildId;
        //            //ViewBag.ServiceData = model;
        //            //ViewBag.CommunicationData = model2;

        //        }
        //    }
        //    return Json(abstractServices, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        [ActionName(Actions.SaveServiceRequest)]
        public JsonResult SaveServiceRequest(string servicesRequest)
        {
            var s = JsonConvert.DeserializeObject<Services>(servicesRequest);
            s.UserId = ProjectSession.UserID;
            var result = abstractServices.ServiceRequest_Upsert(s);
            if (result != null && result.Code == 200 && result.Item != null)
            {
                //var userdata1 = abstractUserServices.UsersPaymentDetails_ById(ProjectSession.UserID).Item;
                //if (userdata1.ManagerType != 2)
                //{
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 1;
                PagedList<AbstractProvider> Providerdata = new PagedList<AbstractProvider>();
                Providerdata = abstractProvider.Provider_AllByServiceRequestId(pageParam, "", result.Item.Id);
                if (Providerdata.Values.Count > 0)
                {

                    for (int i = 0; i < Providerdata.Values.Count; i++)
                    {
                        var sendGridClient = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"]);
                        var sendGridMessage = new SendGridMessage();
                        sendGridMessage.SetFrom(ConfigurationManager.AppSettings["SendGridFromEmail"], "My Request");

                        sendGridMessage.SetTemplateData(new RequestObj
                        {
                            PName = Providerdata.Values[i].CompanyName != null ? Providerdata.Values[i].CompanyName : Providerdata.Values[i].Name + ' ' + Providerdata.Values[i].LastName,
                            RName = Providerdata.Values[i].RegistrationGroupName,
                            Address = result.Item.City + ' ' + result.Item.StateName + ' ' + result.Item.PinCode,
                            email = ConvertTo.Base64Encode(Convert.ToString(Providerdata.Values[i].Email))
                        });
                        //sendGridMessage.SetSubject("New Support Request");
                        //sendGridMessage.AddTo(Providerdata.Values[i].Email);
                        sendGridMessage.AddTo("anthony@plannedservices.com.au");
                        //sendGridMessage.AddTo("parth"+i + "@rushkar.com");
                        if (Providerdata.Values[i].IsActive == 0)
                        {
                            sendGridMessage.SetTemplateId("d-e14d7c1886e747b5893d86c469df54ba");
                        }
                        else
                        {
                            sendGridMessage.SetTemplateId("d-bf32fd06db7b4da085c08b72db499c0f");
                        }
                        var response = sendGridClient.SendEmailAsync(sendGridMessage).Result;
                        if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
                        {

                        }
                    }

                    var sendGridClient1 = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"]);
                    var sendGridMessage1 = new SendGridMessage();
                    sendGridMessage1.SetFrom(ConfigurationManager.AppSettings["SendGridFromEmail"], "My Request");

                    sendGridMessage1.SetTemplateData(new RequestObj
                    {
                        PName = result.Item.Name + ' ' + result.Item.LastName,
                        PCount = ConvertTo.String(result.Item.PCount),
                        RName = result.Item.RegistrationGroupName,
                        SNumber = result.Item.ServiceNumber
                    });
                    var userdata = abstractUserServices.User_ById(ProjectSession.UserID).Item;
                    sendGridMessage1.AddTo(userdata.Email);
                    //sendGridMessage1.AddTo("parth@rushkar.com");
                    sendGridMessage1.SetTemplateId("d-59b5d5dbc6da498398f082a7084375dc");
                    var response1 = sendGridClient1.SendEmailAsync(sendGridMessage1).Result;
                    if (response1.StatusCode == System.Net.HttpStatusCode.Accepted)
                    {

                    }

                    if (userdata.ManagerType == 2 && userdata.IsPlan == 2)
                    {
                        var userPaymentData = abstractUserServices.UsersPaymentDetails_ById(ProjectSession.UserID).Item;
                        var userPaymentData1 = abstractUserServices.UsersPaymentDetails_All();

                        Stripe.StripeConfiguration.SetApiKey(Configurations.StripePublishableKey);
                        Stripe.StripeConfiguration.ApiKey = Configurations.StripeSecretKey;

                        var myCharge = new Stripe.ChargeCreateOptions();
                        // always set these properties
                        myCharge.Amount = ConvertTo.Long(9000);
                        myCharge.Currency = "AUD";
                        myCharge.ReceiptEmail = s.stripeEmail;
                        myCharge.Description = "NDIS";
                        myCharge.Source = s.stripeToken;
                        myCharge.Capture = true;
                        var chargeService = new Stripe.ChargeService();
                        Charge stripeCharge = chargeService.Create(myCharge);

                        userPaymentData.TransactionDate = stripeCharge.Created;
                        userPaymentData.TransactionEndDate = stripeCharge.Created.AddYears(1).ToString();
                        userPaymentData.TransactionId = stripeCharge.BalanceTransactionId;
                        userPaymentData.CardNumber = stripeCharge.PaymentMethodDetails.Card.Last4;
                        userPaymentData.ExpiryDate = stripeCharge.PaymentMethodDetails.Card.ExpMonth.ToString() + '/' + stripeCharge.PaymentMethodDetails.Card.ExpYear;
                        SuccessResult<AbstractUser> result2 = abstractUserServices.UsersPaymentDetails_Upsert(userPaymentData);

                        byte[] bytes = null;
                        string strDeviceInfo = null;
                        string strMimeType = null;
                        string strEncoding;
                        string strExtension;
                        string[] strStreams = null;
                        Warning[] warnings;

                        try
                        {
                            DataTable dttest = new DataTable();
                            dttest.Columns.Add("id");
                            dttest.Columns.Add("fname");
                            dttest.Columns.Add("lname");
                            dttest.Columns.Add("userid");
                            dttest.Columns.Add("city");
                            dttest.Columns.Add("state");
                            dttest.Columns.Add("postcode");
                            dttest.Columns.Add("invoicedate");
                            dttest.Columns.Add("invoicenumber");
                            dttest.Columns.Add("add");

                            dttest.Rows.Add("1", userdata.NomineeFirstName + " " + userdata.NomineeLastName, userdata.FirstName + " " + userdata.LastName, userdata.Id.ToString(), userPaymentData.City + " " + userPaymentData.StateName + " " + userPaymentData.PinCode,
                                userPaymentData.StateName, userPaymentData.PinCode.ToString(), userPaymentData.TransactionDateStr,
                                userPaymentData1.TotalRecords.ToString(), userPaymentData.AddressLine1);
                            ReportViewer rptViewer1 = new ReportViewer();

                            //ReportParameter NomineeFirstName = new ReportParameter("NomineeFirstName", Convert.ToString(userdata.NomineeFirstName));
                            //ReportParameter NomineeLastName = new ReportParameter("NomineeLastName", Convert.ToString(userdata.NomineeLastName));
                            //ReportParameter AddressFormPayment = new ReportParameter("AddressFormPayment", Convert.ToString(userPaymentData.AddressLine1));
                            //ReportParameter City = new ReportParameter("City", Convert.ToString(userPaymentData.City));
                            //ReportParameter State = new ReportParameter("State", Convert.ToString(userPaymentData.StateName));
                            //ReportParameter PostCode = new ReportParameter("PostCode", Convert.ToString(userPaymentData.PinCode));
                            //ReportParameter InvoiceDate = new ReportParameter("InvoiceDate", Convert.ToString(userPaymentData.TransactionDateStr));
                            //ReportParameter InvoiceNumber = new ReportParameter("InvoiceNumber", Convert.ToString(userPaymentData1.TotalRecords));
                            //ReportParameter Reference = new ReportParameter("Reference", Convert.ToString(userdata.Id));

                            rptViewer1.ProcessingMode = ProcessingMode.Local;
                            rptViewer1.LocalReport.ReportPath = Server.MapPath("~/report/CausalReport.rdlc");

                            ReportDataSource rdc = new ReportDataSource("DataSet1", dttest);
                            rptViewer1.LocalReport.DataSources.Add(rdc);

                            //rptViewer1.LocalReport.SetParameters(new ReportParameter[] {
                            //        NomineeFirstName,NomineeLastName,AddressFormPayment,City,State,PostCode,InvoiceDate,InvoiceNumber,Reference
                            // });
                            rptViewer1.LocalReport.Refresh();

                            bytes = rptViewer1.LocalReport.Render("PDF", strDeviceInfo, out strMimeType, out strEncoding, out strExtension, out strStreams, out warnings);



                            using (FileStream fs = new FileStream(Server.MapPath("~/Content/doc/Invoice" + userdata.Id + ".pdf"), FileMode.Create))
                            {
                                fs.Write(bytes, 0, bytes.Length);
                            }
                            invoicesend("Invoice" + userdata.Id + ".pdf", bytes, userdata.Email);
                           
                            //Response.Buffer = true;
                            //Response.Clear();
                            //Response.ContentType = strMimeType;
                            //Response.AddHeader("content-disposition", "attachment; filename=Invoice.pdf");
                            //Response.BinaryWrite(bytes); // create the file
                            //Response.Flush();
                        }// send it to the client to download
                        catch (Exception ex)
                        {
                            //fs.Write(bytes, 0, bytes.Length);
                        }
                    
                    }

                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public void invoicesend(string FileName, byte[] bytes, string Email)
        {
            var sendGridClient = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"]);

            var sendGridMessage = new SendGridMessage();
            sendGridMessage.SetFrom(ConfigurationManager.AppSettings["SendGridFromEmail"], "My Request");
            //sendGridMessage.AddTo(Email, "user"); // user email
            sendGridMessage.AddTo("anthony@plannedservices.com.au", "user test"); // user email
            sendGridMessage.SetTemplateId("d-4970d7b1cb5a401782d894def1390958");

            // sendGridMessage.AddAttachment(Server.MapPath(FileName), null);

            var file = Convert.ToBase64String(bytes);
            sendGridMessage.AddAttachment(FileName, file);

            var response = sendGridClient.SendEmailAsync(sendGridMessage).Result;
            if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
            {

            }
        }
        [HttpPost]
        [ActionName(Actions.SaveSelfMServiceRequest)]
        public JsonResult SaveSelfMServiceRequest(string Id)
        {
            //var s = JsonConvert.DeserializeObject<Services>(servicesRequest);
            //s.UserId = ProjectSession.UserID;
            var result = abstractServices.GetUserInfo_ByServiceRequestId(Convert.ToInt64(Id));
            if (result != null && result.Code == 200 && result.Item != null)
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 10000;
                PagedList<AbstractProvider> Providerdata = new PagedList<AbstractProvider>();
                Providerdata = abstractProvider.Provider_AllByServiceRequestId(pageParam, "", result.Item.Id);
                if (Providerdata.Values.Count > 0)
                {
                    var sendGridClient = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"]);
                    var sendGridMessage = new SendGridMessage();
                    sendGridMessage.SetFrom(ConfigurationManager.AppSettings["SendGridFromEmail"], "My Request");

                    for (int i = 0; i < Providerdata.Values.Count; i++)
                    {
                        sendGridMessage.SetTemplateData(new RequestObj
                        {
                            PName = Providerdata.Values[i].CompanyName != null ? Providerdata.Values[i].CompanyName : Providerdata.Values[i].Name + ' ' + Providerdata.Values[i].LastName,
                            RName = Providerdata.Values[i].RegistrationGroupName,
                            Address = result.Item.City + ' ' + result.Item.StateName + ' ' + result.Item.PinCode,
                            email = ConvertTo.Base64Encode(Convert.ToString(Providerdata.Values[i].Email))
                        });
                        //sendGridMessage.SetSubject("New Support Request");
                         sendGridMessage.AddTo(Providerdata.Values[i].Email);
                        //sendGridMessage.AddTo("anthony@plannedservices.com.au");
                        if (Providerdata.Values[i].IsActive == 0)
                        {
                            sendGridMessage.SetTemplateId("d-e14d7c1886e747b5893d86c469df54ba");
                        }
                        else
                        {
                            sendGridMessage.SetTemplateId("d-bf32fd06db7b4da085c08b72db499c0f");
                        }
                        var response = sendGridClient.SendEmailAsync(sendGridMessage).Result;
                        if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
                        {

                        }
                    }

                    var sendGridClient1 = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"]);
                    var sendGridMessage1 = new SendGridMessage();
                    sendGridMessage1.SetFrom(ConfigurationManager.AppSettings["SendGridFromEmail"], "My Request");

                    sendGridMessage1.SetTemplateData(new RequestObj
                    {
                        PName = result.Item.Name + ' ' + result.Item.LastName,
                        PCount = ConvertTo.String(result.Item.PCount),
                        RName = result.Item.RegistrationGroupName,
                        SNumber = result.Item.ServiceNumber
                    });
                    var userdata = abstractUserServices.User_ById(ProjectSession.UserID).Item;
                    sendGridMessage.AddTo(userdata.Email);
                    //sendGridMessage1.AddTo("parthdobariya09@gmail.com");
                    sendGridMessage1.SetTemplateId("d-59b5d5dbc6da498398f082a7084375dc");
                    var response1 = sendGridClient1.SendEmailAsync(sendGridMessage1).Result;
                    if (response1.StatusCode == System.Net.HttpStatusCode.Accepted)
                    {

                    }
                }
                else
                {

                }
                //TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), "Your request has been sent to 32 Providers");

                //var sendGridClient = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"]);

                //var sendGridMessage = new SendGridMessage();
                //sendGridMessage.SetFrom(ConfigurationManager.AppSettings["SendGridFromEmail"], "My Request");
                //sendGridMessage.AddTo("anthony@plannedservices.com.au", "user test"); // user email
                //sendGridMessage.SetTemplateId("d-54877013eb81443094b3901d66f8e67a");

                //var response = sendGridClient.SendEmailAsync(sendGridMessage).Result;
                //if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
                //{

                //}
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ActionName(Actions.GetSendRequestProviderCount)]
        public JsonResult GetSendRequestProviderCount(int RId)
        {
            var result = abstractServices.GetServiceRequestProviderCount(RId, ProjectSession.UserID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ActionName(Actions.AddCommunication)]
        public JsonResult AddCommunication(string servicesRequest)
        {

            var s = JsonConvert.DeserializeObject<ServicesRequest>(servicesRequest);
            s.CustomerId = ProjectSession.UserID;
            var result = abstractServicesRequest.Communication_Upsert(s, null);
            if (result != null && result.Code == 200)
            {
                //var sendGridClient = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"]);

                //var sendGridMessage = new SendGridMessage();
                //sendGridMessage.SetFrom(ConfigurationManager.AppSettings["SendGridFromEmail"], "My Request");
                //sendGridMessage.AddTo(result.Item.ProviderEmail, "user test"); // user email
                //                                                               // sendGridMessage.AddTo("parthdobariya09@gmail.com", "user test"); // user email
                //sendGridMessage.SetTemplateId("d-73c8f8ce1e1348acb658923a2a3b0445");

                //sendGridMessage.SetTemplateData(new RequestObj
                //{
                //    PName = result.Item.ProviderName,
                //    RName = result.Item.RegistrationGroupName,
                //    SNumber = result.Item.ServiceNumber

                //});

                //var response = sendGridClient.SendEmailAsync(sendGridMessage).Result;
                //if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
                //{

                //}
            }
            return Json(result, JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.SendRequest)]
        public ActionResult SendRequest(ServicesRequest model)
        {

            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 10;
            model.serviceObj.UserId = ProjectSession.UserID;
            model.serviceObj.BudgetId = model.BudgetId;
            model.serviceObj.RegistrationGroupId = model.RegistrationGroupId;
            model.serviceObj.CategoryId = model.CategoryId;
            model.serviceObj.Comment = model.Comment;
            model.serviceObj.BookingRequestedDate = model.BookingRequestedDate;
            //PagedList<AbstractProvider> result = abstractProviderServices.Provider_All(pageParam, "", 0, "", "", "");
            SuccessResult<AbstractServices> result2 = abstractServices.ServiceRequest_Upsert(model.serviceObj);
            if (result2 != null && result2.Code == 200 && result2.Item != null)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), "Your request has been sent to 32 Providers");
                var sendGridClient = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"]);

                var sendGridMessage = new SendGridMessage();
                sendGridMessage.SetFrom(ConfigurationManager.AppSettings["SendGridFromEmail"], "My Request");
                sendGridMessage.AddTo("anthony@plannedservices.com.au", "user test"); // user email
                sendGridMessage.SetTemplateId("d-54877013eb81443094b3901d66f8e67a");

                var response = sendGridClient.SendEmailAsync(sendGridMessage).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
                {

                }
            }
            else
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result2.Message);
            }
            //Services s = new Services();
            ///s.Provider = result.Values;
            // s.ServiceRequestId = result2.Item.Id;
            // TempData["ServiceData"] = s;
            return RedirectToAction(Actions.Index, Pages.Controllers.ServiceRequests);
        }

        [HttpPost]
        [ActionName(Actions.ChangeServiceStatus)]
        public JsonResult ChangeServiceStatus(int Type, int IsApproved, string ReasonId, string ServiceRequestId, string ProviderId)
        {
            var result = abstractServices.ChangeServiceStatus(ConvertTo.Integer(ServiceRequestId), ConvertTo.Integer(ProviderId), Type, IsApproved, ReasonId);
            if (result != null && result.Code == 200 && result.Item != null)
            {
                var sendGridClient = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"]);
                var sendGridMessage = new SendGridMessage();
                sendGridMessage.SetFrom(ConfigurationManager.AppSettings["SendGridFromEmail"], "My Request");

                sendGridMessage.SetTemplateData(new RequestObj
                {
                    PName = result.Item.CompanyName != null ? result.Item.CompanyName : result.Item.Name + ' ' + result.Item.LastName,
                    RName = result.Item.RegistrationGroupName,
                    SNumber = result.Item.ServiceNumber
                });
                sendGridMessage.AddTo(result.Item.Email);
                //sendGridMessage.AddTo("parthdobariya09@gmail.com");

                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 10000;
               
                var modal = abstractProvider.ClosedServiceRequestProviderAll(pageParam, "", ConvertTo.Integer(ServiceRequestId)).Values;
                //abstractServices.ServiceRequestChild_ByServiceRequestId(ConvertTo.Integer(ServiceRequestId)).Values;
                if (modal.Count > 0)
                {
                    for (int i = 0; i < modal.Count; i++)
                    {
                        //sendrequestmail(modal[i]);
                        if (i == 0)
                        {
                            sendrequestmail(modal[i]);
                        }
                    }
                }

                //if (Type == 1)
                //{
                //    sendGridMessage.SetTemplateId("d-efac31b56f1f45789cbbaa472467cad7");
                //    var modal = abstractServices.ServiceRequestChild_ByServiceRequestId(ConvertTo.Integer(ServiceRequestId)).Values;
                //    if (modal.Count > 0)
                //    {
                //        for (int i = 0; i < modal.Count; i++)
                //        {
                //            //sendrequestmail(modal[i]);
                //            if (i == 0)
                //            {
                //                sendrequestmail(modal[i]);
                //            }
                //        }
                //    }
                //}
                //else
                //{
                //    sendGridMessage.SetTemplateId("d-82b0ca1841d3427798e7a5c70458a54c");
                //}

                sendGridMessage.SetTemplateId("d-82b0ca1841d3427798e7a5c70458a54c");
                var response = sendGridClient.SendEmailAsync(sendGridMessage).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
                {

                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ActionName(Actions.UpdateIsHideContactDetails)]
        public JsonResult UpdateIsHideContactDetails(int IsHideContactDetails,string ServiceRequestId)
        {
            var result = abstractServices.UpdateIsHideContactDetails(ConvertTo.Integer(ServiceRequestId), IsHideContactDetails);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        [ActionName(Actions.RegistrationGroupByCategoryId)]
        public ActionResult RegistrationGroupByCategoryId(int Type, int RId, int CId, string GName = "", int BId = 0)
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 100000;
            List<ServicesRequest> items = new List<ServicesRequest>();
            string bName = "";
            if (BId == 1)
            {
                bName = "Capital";
            }
            else if (BId == 2)
            {
                bName = "Core";
            }
            else if (BId == 3)
            {
                bName = "Capacity Building";
            }

            if (Type == 1)
            {
                ViewBag.Section = 2;
                //var model = abstractServices.RegistrationGroupMaster_All(pageParam, "", GName);
                var model = abstractServices.RegistrationGroupMasterByCategoryId(pageParam, "", GName, bName, CId);
                foreach (var item in model.Values)
                {
                    items.Add(new ServicesRequest() { Name = item.Name.ToString(), Description = ConvertTo.String(item.Description), DrpId = item.Id.ToString() });
                }
            }
            else if (Type == 0)
            {
                ViewBag.Section = 3;
                var model = abstractServices.SupportCategoryMasterByRegistrationGroupId(pageParam, "", GName, bName);
                foreach (var item in model.Values)
                {
                    items.Add(new ServicesRequest() { Name = item.Name.ToString(), DrpId = item.Id.ToString(), Description = ConvertTo.String(item.Description) });
                }
            }
            else if (Type == 3)
            {
                ViewBag.Section = 3;
                var model = abstractServices.SupportCategoryMasterByRegistrationGroupId(pageParam, "", GName, bName);
                foreach (var item in model.Values)
                {
                    items.Add(new ServicesRequest() { Name = item.Name.ToString(), DrpId = item.Id.ToString(), Description = ConvertTo.String(item.Description) });
                }

                //ViewBag.Section = 5;
                //var model = abstractServices.SupportGoal_AllByBudget(pageParam, "", bName);
                //foreach (var item in model.Values)
                //{
                //    items.Add(new ServicesRequest() { Name = item.OutcomeSort.ToString(), DrpId = item.OutcomeSort.ToString() });
                //}
            }
            else
            {
                ViewBag.Section = 4;
                var model = abstractServices.SupportItemMasterByCategoryName(pageParam, "", CId, RId, GName, bName);
                foreach (var item in model.Values)
                {
                    items.Add(new ServicesRequest() { Name = item.Name.ToString(), DrpId = item.Id.ToString(), Description = ConvertTo.String(item.Description), Unit = ConvertTo.String(item.Unit) });
                }
            }

            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName(Actions.DeleteServiceRequest)]
        public ActionResult DeleteServiceRequest(int SId)
        {
            var result = abstractServices.DeleteServiceRequest(SId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName(Actions.DeleteServiceRequestProvider)]
        public ActionResult DeleteServiceRequestProvider(int SId, int PId)
        {
            var result = abstractServices.DeleteServiceRequestProvider(SId, PId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public IList<ServicesRequest> BindBudgetMasterDropdown()
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 100000;
            var model = abstractServices.BudgetMaster_All(pageParam, "").Values;

            List<ServicesRequest> items = new List<ServicesRequest>();

            foreach (var item in model)
            {
                items.Add(new ServicesRequest() { Name = item.Name.ToString(), Description = item.Description.ToString(), Id = item.Id });
            }

            return items;
        }

        public IList<SelectListItem> BindReasonData()
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 100000;
            var model = abstractServices.Reason_All(pageParam, "").Values;

            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in model)
            {
                items.Add(new SelectListItem() { Text = item.Name.ToString(), Value = item.Id.ToString() });
            }

            return items;
        }

        public IList<SelectListItem> BindDistanceDropdown()
        {

            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "5km", Value = "5km" });
            items.Add(new SelectListItem() { Text = "10km", Value = "10km" });
            items.Add(new SelectListItem() { Text = "15km", Value = "15km" });
            items.Add(new SelectListItem() { Text = "20km", Value = "20km" });

            return items;
        }

        public ActionResult sendrequestmail(AbstractProvider services)
        {
            var sendGridClient = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"]);
            var sendGridMessage = new SendGridMessage();
            sendGridMessage.SetFrom(ConfigurationManager.AppSettings["SendGridFromEmail"], "My Request");
            var CreatedDate = DateTime.Now.ToString("dd MMM yyyy");
            sendGridMessage.SetTemplateData(new RequestObj
            {
                PName = services.CompanyName != null ? services.CompanyName : services.Name + ' ' + services.LastName,
                RName = services.RegistrationGroupName,
                Address = services.City + ' ' + services.StateName + ' ' + services.PinCode,
                SNumber = services.ServiceNumber,
                SDate = CreatedDate
            });
            sendGridMessage.AddTo("anthony@plannedservices.com.au");
            //sendGridMessage.AddTo(services.Email);
            if (services.IsProviderRead > 0)
            {
                sendGridMessage.SetTemplateId("d-82b0ca1841d3427798e7a5c70458a54c");
            }
            else
            {
                sendGridMessage.SetTemplateId("d-3a7b3ffcae5d46a0a439b496054ea3e0");
            }
            var response = sendGridClient.SendEmailAsync(sendGridMessage).Result;
            if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
            {

            }
            return null;
        }
        private class EmailObj
        {
            [JsonProperty("name")]
            public string name { get; set; }

            [JsonProperty("status")]
            public string status { get; set; }

            [JsonProperty("message")]
            public string message { get; set; }
        }
        private class RequestObj
        {
            [JsonProperty("UName")]
            public string UName { get; set; }

            [JsonProperty("PName")]
            public string PName { get; set; }

            [JsonProperty("RName")]
            public string RName { get; set; }

            [JsonProperty("Address")]
            public string Address { get; set; }

            [JsonProperty("email")]
            public string email { get; set; }

            [JsonProperty("SNumber")]
            public string SNumber { get; set; }

            [JsonProperty("PCount")]
            public string PCount { get; set; }

            [JsonProperty("SDate")]
            public string SDate { get; set; }
        }
        #endregion
    }
}