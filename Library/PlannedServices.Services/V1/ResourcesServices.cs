﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlannedServices.Common;
using PlannedServices.Common.Paging;
using PlannedServices.Data.Contract;
using PlannedServices.Entities.Contract;
using PlannedServices.Services.Contract;

namespace PlannedServices.Services.V1
{
    public class ResourcesServices : AbstractResourcesServices
    {
        private AbstractResourcesDao abstractResourcesDao;

        public ResourcesServices(AbstractResourcesDao abstractResourcesDao)
        {
            this.abstractResourcesDao = abstractResourcesDao;
        }

        public override SuccessResult<AbstractResources> Resources_Upsert(AbstractResources abstractResources)
        {
            return this.abstractResourcesDao.Resources_Upsert(abstractResources);
        }

        public override SuccessResult<AbstractResources> Resources_ById(long Id)
        {
            return this.abstractResourcesDao.Resources_ById(Id);
        }

        public override PagedList<AbstractResources> Resources_All(PageParam pageParam, string search)
        {
            return this.abstractResourcesDao.Resources_All(pageParam, search);
        }




    }
}
