﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlannedServices.Common;
using PlannedServices.Common.Paging;
using PlannedServices.Data.Contract;
using PlannedServices.Entities.Contract;
using PlannedServices.Entities.V1;
using Dapper;

namespace PlannedServices.Data.V1
{
    public class ServicesRequestDao : AbstractServicesRequestDao
    {

      
        public override PagedList<AbstractServicesRequest> ServicesRequest_All(PageParam pageParam, string search, int ProviderId)
        {
            PagedList<AbstractServicesRequest> ServicesRequest = new PagedList<AbstractServicesRequest>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ProviderId", ProviderId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ServiceRequest_All, param, commandType: CommandType.StoredProcedure);
                ServicesRequest.Values.AddRange(task.Read<ServicesRequest>());
                ServicesRequest.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ServicesRequest;
        }
        public override PagedList<AbstractServicesRequest> ClosedServiceRequestParticipateAll(PageParam pageParam, string search, int ProviderId)
        {
            PagedList<AbstractServicesRequest> ServicesRequest = new PagedList<AbstractServicesRequest>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ProviderId", ProviderId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ClosedServiceRequestParticipateAll, param, commandType: CommandType.StoredProcedure);
                ServicesRequest.Values.AddRange(task.Read<ServicesRequest>());
                ServicesRequest.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ServicesRequest;
        }
        public override PagedList<AbstractServicesRequest> ServiceRequest_AllByCustomerId(PageParam pageParam, string search, int CustomerId)
        {
            PagedList<AbstractServicesRequest> ServicesRequest = new PagedList<AbstractServicesRequest>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CustomerId", CustomerId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ServiceRequest_AllByCustomerId, param, commandType: CommandType.StoredProcedure);
                ServicesRequest.Values.AddRange(task.Read<ServicesRequest>());
                ServicesRequest.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ServicesRequest;
        }
        public override PagedList<AbstractServicesRequest> ServicesRequest_ById(PageParam pageParam, string search, int Id, int ProviderId)
        {
            PagedList<AbstractServicesRequest> ServicesRequest = new PagedList<AbstractServicesRequest>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ProviderId", ProviderId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ServicesRequest_ById, param, commandType: CommandType.StoredProcedure);
                ServicesRequest.Values.AddRange(task.Read<ServicesRequest>());
                ServicesRequest.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ServicesRequest;
        }
        public override SuccessResult<AbstractServicesRequest> ServiceRequestSupportItemProvider_Upsert(AbstractServicesRequest abstractServices)
        {
            SuccessResult<AbstractServicesRequest> ServicesRequest =null;

            var param = new DynamicParameters();
            param.Add("@Id", abstractServices.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ServiceReqId", abstractServices.ServiceRequestId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@SupportItemMasterId", abstractServices.SupportItemId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Rate", abstractServices.Rate, dbType: DbType.Double, direction: ParameterDirection.Input);
            param.Add("@Unit", abstractServices.Unit, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Quote", abstractServices.Quote, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ProviderId", abstractServices.ProviderId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@FileURL", abstractServices.FileURL, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@FileName", abstractServices.FileName, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ServiceRequestSupportItemProvider_Upsert, param, commandType: CommandType.StoredProcedure);
                ServicesRequest = task.Read<SuccessResult<AbstractServicesRequest>>().SingleOrDefault();
                ServicesRequest.Item = task.Read<ServicesRequest>().SingleOrDefault();
            }
            return ServicesRequest;
        }

        public override SuccessResult<AbstractServicesRequest> UpdateServiceRequestChildByProviderId(int ServiceRequestId, int ProviderId, int Status, string Comment, string ReasonId)
        {
            SuccessResult<AbstractServicesRequest> ServicesRequest = null;

            var param = new DynamicParameters();
            param.Add("@ServiceRequestId", ServiceRequestId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ProviderId", ProviderId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Status", Status, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Comment", Comment, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ReasonId", ReasonId, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UpdateServiceRequestChildByProviderId, param, commandType: CommandType.StoredProcedure);
                ServicesRequest = task.Read<SuccessResult<AbstractServicesRequest>>().SingleOrDefault();
                ServicesRequest.Item = task.Read<ServicesRequest>().SingleOrDefault();
            }
            return ServicesRequest;
        }
        public override SuccessResult<AbstractServicesRequest> Communication_Upsert(AbstractServicesRequest servicesRequest)
        {
            SuccessResult<AbstractServicesRequest> ServicesRequest = null;

            var param = new DynamicParameters();
            param.Add("@Id", servicesRequest.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ProviderId", servicesRequest.ProviderId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CustomerId", servicesRequest.CustomerId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ServiceRequestChildId", servicesRequest.ServiceChildId, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Messages", servicesRequest.Message, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ImageUrl", servicesRequest.ImageUrl, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ServiceRequestId", servicesRequest.SRequestId, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Communication_Upsert, param, commandType: CommandType.StoredProcedure);
                ServicesRequest = task.Read<SuccessResult<AbstractServicesRequest>>().SingleOrDefault();
                ServicesRequest.Item = task.Read<ServicesRequest>().SingleOrDefault();
            }
            return ServicesRequest;
        }

        public override PagedList<AbstractServicesRequest> Communication_AllByProviderId(PageParam pageParam, string search, int CustomerId, int ProviderId, int ServiceRequestChildId)
        {
            PagedList<AbstractServicesRequest> ServicesRequest = new PagedList<AbstractServicesRequest>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CustomerId", CustomerId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ProviderId", ProviderId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ServiceRequestChildId", ServiceRequestChildId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Communication_AllByProviderId, param, commandType: CommandType.StoredProcedure);
                ServicesRequest.Values.AddRange(task.Read<ServicesRequest>());
                ServicesRequest.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ServicesRequest;
        }

        public override SuccessResult<AbstractServicesRequest> ServiceRequests_AllByCustomerId(int CustomerId)
        {
            SuccessResult<AbstractServicesRequest> servicesRequest = null;

            var param = new DynamicParameters();
            param.Add("@CustomerId", CustomerId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ServiceRequests_AllByCustomerId, param, commandType: CommandType.StoredProcedure);
                servicesRequest = task.Read<SuccessResult<AbstractServicesRequest>>().SingleOrDefault();
                servicesRequest.Item = task.Read<ServicesRequest>().SingleOrDefault();
                if (servicesRequest.Item != null)
                {
                    servicesRequest.Item.ServicesRequests = new List<AbstractServicesRequest>();
                    servicesRequest.Item.ServicesRequests.AddRange(task.Read<ServicesRequest>());
                    servicesRequest.Item.Providers = new List<AbstractProvider>();
                    servicesRequest.Item.Providers.AddRange(task.Read<Provider>());
                }
            }
            return servicesRequest;
        }
        public override SuccessResult<AbstractServicesRequest> ClosedServiceRequestByCustomerId(int CustomerId)
        {
            SuccessResult<AbstractServicesRequest> servicesRequest = null;

            var param = new DynamicParameters();
            param.Add("@CustomerId", CustomerId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ClosedServiceRequestByCustomerId, param, commandType: CommandType.StoredProcedure);
                servicesRequest = task.Read<SuccessResult<AbstractServicesRequest>>().SingleOrDefault();
                servicesRequest.Item = task.Read<ServicesRequest>().SingleOrDefault();
                if (servicesRequest.Item != null)
                {
                    servicesRequest.Item.ServicesRequests = new List<AbstractServicesRequest>();
                    servicesRequest.Item.ServicesRequests.AddRange(task.Read<ServicesRequest>());
                    servicesRequest.Item.Providers = new List<AbstractProvider>();
                    servicesRequest.Item.Providers.AddRange(task.Read<Provider>());
                }
            }
            return servicesRequest;
        }

        public override SuccessResult<AbstractServicesRequest> CommunicationIsRead(int ProviderId, int CustomerId, int ServiceRequestChildId)
        {
            SuccessResult<AbstractServicesRequest> servicesRequest = null;

            var param = new DynamicParameters();
            param.Add("@ProviderId", ProviderId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CustomerId", CustomerId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@ServiceRequestChildId", ServiceRequestChildId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CommunicationIsRead, param, commandType: CommandType.StoredProcedure);
                servicesRequest = task.Read<SuccessResult<AbstractServicesRequest>>().SingleOrDefault();
                servicesRequest.Item = task.Read<ServicesRequest>().SingleOrDefault();
            }
            return servicesRequest;
        }

        public override bool DeleteServiceRequestSupportItemProvider(int id)
        {
            bool result = false;
            var param = new DynamicParameters();

            param.Add("@Id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.DeleteServiceRequestSupportItemProvider, param, commandType: CommandType.StoredProcedure);
                result = task.SingleOrDefault<bool>();
            }

            return result;
        }

        public override PagedList<AbstractServicesRequest> GetParticipantsChartData(int Id, int Type = 0)
        {
            PagedList<AbstractServicesRequest> ServicesRequest = new PagedList<AbstractServicesRequest>();

            var param = new DynamicParameters();
            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            //param.Add("@Type", Type, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.GetParticipantsChartData, param, commandType: CommandType.StoredProcedure);
                ServicesRequest.Values.AddRange(task.Read<ServicesRequest>());
                ServicesRequest.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ServicesRequest;
        }
        public override PagedList<AbstractServicesRequest> GetProviderChartData(int Id)
        {
            PagedList<AbstractServicesRequest> ServicesRequest = new PagedList<AbstractServicesRequest>();

            var param = new DynamicParameters();
            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.GetProviderChartData, param, commandType: CommandType.StoredProcedure);
                ServicesRequest.Values.AddRange(task.Read<ServicesRequest>());
                ServicesRequest.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ServicesRequest;
        }
    }
}
