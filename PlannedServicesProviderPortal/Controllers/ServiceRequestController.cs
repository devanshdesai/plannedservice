﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PlannedServices.Common;
using PlannedServices.Common.Paging;
using PlannedServices.Entities.Contract;
using PlannedServices.Entities.V1;
using PlannedServices.Services.Contract;
using PlannedServicesProviderPortal.Infrastructure;
using PlannedServicesProviderPortal.Pages;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace PlannedServicesProviderPortal.Controllers
{
    public class ServiceRequestController : BaseController
    {
        #region Fields
        private readonly AbstractServicesRequestServices abstractServicesRequest;

        #endregion

        #region Ctor
        public ServiceRequestController(AbstractServicesRequestServices abstractServicesRequest)
        {
            this.abstractServicesRequest = abstractServicesRequest;
        }
        #endregion

        // GET: ServiceRequest
        public ActionResult Index()
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000000;
            var model = abstractServicesRequest.ServicesRequest_All(pageParam, "", ProjectSession.ProviderId);
            return View(model);
        }
        public ActionResult ViewServiceRequest(string ServiceId = "")
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            ViewBag.userId = 0;
            if (ServiceId != null)
            {
                int decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(ServiceId));
                if (decryptedId > 0)
                {
                    PageParam pageParam = new PageParam();
                    pageParam.Offset = 0;
                    pageParam.Limit = 1000000;
                    var model = this.abstractServicesRequest.ServicesRequest_ById(pageParam, "", decryptedId, ProjectSession.ProviderId).Values;
                    var model2 = this.abstractServicesRequest.Communication_AllByProviderId(pageParam, "", 0, ProjectSession.ProviderId, model[0].ServiceRequestChildId).Values;
                    ViewBag.ServiceRequestId = decryptedId;
                    ViewBag.Status = model[0].Status;
                    ViewBag.ServiceRequestChildId = model[0].ServiceRequestChildId;
                    ViewBag.ServiceData = model;
                    ViewBag.CommunicationData = model2;

                }
            }
            return View();
        }


        [HttpGet]
        [ActionName(Actions.AddServiceRequest)]
        public ActionResult AddServiceRequest(int Type, string ReasonId, string servicesRequests)
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 100000;
            List<SelectListItem> items = new List<SelectListItem>();
            var s = JsonConvert.DeserializeObject<List<ServicesRequest>>(servicesRequests);
            if (Type != 2)
            {
                var deleteRes = abstractServicesRequest.DeleteServiceRequestSupportItemProvider(s[0].ServiceRequestId);
                for (int i = 0; i < s.Count; i++)
                {
                    s[i].ProviderId = ProjectSession.ProviderId;
                    var model = abstractServicesRequest.ServiceRequestSupportItemProvider_Upsert(s[i]);
                }
            }
            var result = abstractServicesRequest.UpdateServiceRequestChildByProviderId(s[0].ServiceRequestId, ProjectSession.ProviderId, Type, s[0].Comment, ReasonId);

            if (result != null && result.Code == 200)
            {
                var sendGridClient = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"]);

                var sendGridMessage = new SendGridMessage();
                sendGridMessage.SetFrom(ConfigurationManager.AppSettings["SendGridFromEmail"], "rushkar");
                sendGridMessage.AddTo(result.Item.Email, "user test"); // user email
                sendGridMessage.SetTemplateId("d-0919d6b118bb48839f0f000029df291f");

                sendGridMessage.SetTemplateData(new EmailObj
                {
                    name = result.Item.Name,
                    status = Type == 1 ? "Accepted" : "Rejected"

                });

                var response = sendGridClient.SendEmailAsync(sendGridMessage).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
                {

                }
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName(Actions.AddServiceRequestSupport)]
        public ActionResult AddServiceRequestSupport(string servicesRequests)
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 100000;
            List<SelectListItem> items = new List<SelectListItem>();
            var s = JsonConvert.DeserializeObject<List<ServicesRequest>>(servicesRequests);

            var deleteRes = abstractServicesRequest.DeleteServiceRequestSupportItemProvider(s[0].ServiceRequestId);
            for (int i = 0; i < s.Count; i++)
            {
                s[i].ProviderId = ProjectSession.ProviderId;
                var model = abstractServicesRequest.ServiceRequestSupportItemProvider_Upsert(s[i]);
            }
               
            return Json(items, JsonRequestBehavior.AllowGet);
        }
        
        [HttpGet]
        [ActionName(Actions.DeleteServiceRequestSupport)]
        public ActionResult DeleteServiceRequestSupport(string ServiceRequestId)
        {
            var deleteRes = abstractServicesRequest.DeleteServiceRequestSupportItemProvider(ConvertTo.Integer(ServiceRequestId));
            
            return Json(deleteRes, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.AddCommunication)]
        public ActionResult AddCommunication(ServicesRequest servicesRequest, IEnumerable<HttpPostedFileBase> images)
        {
            servicesRequest.ProviderId = ProjectSession.ProviderId;
            //var result = abstractServicesRequest.Communication_Upsert(servicesRequest.ServiceRequestId, ProjectSession.ProviderId, 0, servicesRequest.ServiceRequestChildId, servicesRequest.Message);
            var result = abstractServicesRequest.Communication_Upsert(servicesRequest, images);

            if (result != null && result.Code == 200)
            {
                var sendGridClient = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"]);

                var sendGridMessage = new SendGridMessage();
                sendGridMessage.SetFrom(ConfigurationManager.AppSettings["SendGridFromEmail"], "rushkar");
                sendGridMessage.AddTo("parth@rushkar.com", "user test"); // user email
                sendGridMessage.SetTemplateId("d-a0ea4e6f63684fafb5f980d3af212aa5");

                sendGridMessage.SetTemplateData(new EmailObj
                {
                    name = result.Item.Name,
                    message = result.Item.Message

                });

                var response = sendGridClient.SendEmailAsync(sendGridMessage).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
                {

                }
            }
            return RedirectToAction("ViewServiceRequest", new { ServiceId = ConvertTo.Base64Encode(Convert.ToString(servicesRequest.SRequestId)) });
            // Json(result, JsonRequestBehavior.AllowGet);
        }

        public static List<string> InvalidJsonElements;
        public static IList<T> DeserializeToList<T>(string jsonString)
        {
            InvalidJsonElements = null;
            var array = JArray.Parse(jsonString);
            IList<T> objectsList = new List<T>();

            foreach (var item in array)
            {
                try
                {
                    // CorrectElements  
                    objectsList.Add(item.ToObject<T>());
                }
                catch (Exception ex)
                {
                    InvalidJsonElements = InvalidJsonElements ?? new List<string>();
                    InvalidJsonElements.Add(item.ToString());
                }
            }

            return objectsList;
        }

        private class EmailObj
        {
            [JsonProperty("name")]
            public string name { get; set; }

            [JsonProperty("status")]
            public string status { get; set; }

            [JsonProperty("message")]
            public string message { get; set; }
        }
    }
}