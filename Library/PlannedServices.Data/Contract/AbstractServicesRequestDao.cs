﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlannedServices.Common;
using PlannedServices.Common.Paging;
using PlannedServices.Entities.Contract;


namespace PlannedServices.Data.Contract
{
    public abstract class AbstractServicesRequestDao
    {
       public abstract PagedList<AbstractServicesRequest> ServicesRequest_ById(PageParam pageParam, string search, int Id, int ProviderId);
        public abstract PagedList<AbstractServicesRequest> ServiceRequest_AllByCustomerId(PageParam pageParam, string search, int CustomerId);
        public abstract PagedList<AbstractServicesRequest> ServicesRequest_All(PageParam pageParam, string search, int ProviderId);
        public abstract PagedList<AbstractServicesRequest> ClosedServiceRequestParticipateAll(PageParam pageParam, string search, int ProviderId);
        public abstract SuccessResult<AbstractServicesRequest> ServiceRequestSupportItemProvider_Upsert(AbstractServicesRequest abstractServices);
        public abstract SuccessResult<AbstractServicesRequest> UpdateServiceRequestChildByProviderId(int ServiceRequestId, int ProviderId, int Status, string Comment, string ReasonId);
        public abstract  SuccessResult<AbstractServicesRequest> Communication_Upsert(AbstractServicesRequest servicesRequest);
        public abstract PagedList<AbstractServicesRequest> Communication_AllByProviderId(PageParam pageParam, string search, int CustomerId, int ProviderId, int ServiceRequestChildId);
        public abstract SuccessResult<AbstractServicesRequest> ServiceRequests_AllByCustomerId(int CustomerId);
        public abstract SuccessResult<AbstractServicesRequest> ClosedServiceRequestByCustomerId(int CustomerId);

        public abstract SuccessResult<AbstractServicesRequest> CommunicationIsRead(int ProviderId, int CustomerId, int ServiceRequestChildId);
    
        public abstract bool DeleteServiceRequestSupportItemProvider(int id);
        public abstract PagedList<AbstractServicesRequest> GetParticipantsChartData(int Id, int Type = 0);
        public abstract PagedList<AbstractServicesRequest> GetProviderChartData(int Id);
    }
}
