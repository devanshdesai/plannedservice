﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlannedServices.Entities.Contract
{
    public abstract class AbstractPlanManager
    {
        public int Id { get; set; }
        public int PlanManagerId { get; set; }
        public int UserId { get; set; }
        public int ParentId { get; set; }
        public int UserType { get; set; }
        public int Type { get; set; }
        public string Title { get; set; }
        public string CompanyName { get; set; }
        public string SubLastName { get; set; }
        public string SubEmail { get; set; }
        public string SubPassword { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ABN { get; set; }
        public string Position { get; set; }
        public string StateName { get; set; }
        public string Email { get; set; }
        public string WebSite { get; set; }
        public string Password { get; set; }
        public string MobileNo { get; set; }
        public string PhoneNumber { get; set; }
        public string DOB { get; set; }
        public string Gender { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Address { get; set; }
        public string Surburb { get; set; }
        public int LookupCountryId { get; set; }
        public int LookupStateId { get; set; }
        public int LookupCityId { get; set; }
        public string PinCode { get; set; }
        public string UserProfileImage { get; set; }
        public string CardNumber { get; set; }
        public string NameOnCard { get; set; }
        public string ExpiryDate { get; set; }
        public string CVVCode { get; set; }
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
        public string CompanyLogoURL { get; set; }
        public double lng { get; set; }
        public double lat { get; set; }
        public bool IsEmailVerified { get; set; }
        public bool IsResetPassword { get; set; }
        public bool IsInvitationAccepted { get; set; }
        public bool IsActive { get; set; }
        public string NDISReferenceNumber { get; set; }
        public DateTime LastLogin { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public int DeletedBy { get; set; }
        public List<AbstractUser> Users{ get; set; }

        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("MM/dd/yyyy") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("MM/dd/yyyy") : "-";
    }
}
