﻿using DataTables.Mvc;
using PlannedServices.Common.Paging;
using PlannedServices.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PlannedServicesProviderPortal.Infrastructure;
namespace PlannedServicesProviderPortal.Controllers
{
    public class ProviderController : BaseController
    {
        private readonly AbstractProviderServices providerService;
        public ProviderController(AbstractProviderServices providerService)
        {
            this.providerService = providerService;
        }
        // GET: Provider
        public ActionResult FindProvider(int Page = 1)
        {
            try
            {
                int RecordsPerPage = 20;                
                int skipRecords = (Page - 1) * RecordsPerPage;

                int totalRecord = 0;
                int filteredRecord = 0;
                long LookupStateId = 0;

                PageParam pageParam = new PageParam();               
                pageParam.Offset = skipRecords;
                pageParam.Limit = RecordsPerPage;
                // string search = Convert.ToString(requestModel.Search.Value);
                string search = "";

                var model = providerService.Provider_All(pageParam, search, LookupStateId,"","","");
                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;
               
                // return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
                return View(model);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                // return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
                return View();
            }
            //return View();
        }
    }
}