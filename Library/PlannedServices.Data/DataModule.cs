﻿//-----------------------------------------------------------------------
// <copyright file="DataModule.cs" company="Rushkar">
//     Copyright Rushkar Solutions. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace PlannedServices.Data
{
    using Autofac;
    using PlannedServices.Data.Contract;

    //using PlannedServices.Data.Contract;

    //using PlannedServices.Data.Contract;


    /// <summary>
    /// Contract Class for DataModule.
    /// </summary>
    public class DataModule : Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be
        /// registered.</param>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        protected override void Load(ContainerBuilder builder)
        {
            //builder.RegisterType<V1.LookUpServicesDao>().As<AbstractLookUpServicesDao>().InstancePerDependency();
            builder.RegisterType<V1.UserDao>().As<AbstractUserDao>().InstancePerDependency();
            builder.RegisterType<V1.ProviderDao>().As<AbstractProviderDao>().InstancePerDependency();
            builder.RegisterType<V1.LookupStateDao>().As<AbstractLookupStateDao>().InstancePerDependency();
            builder.RegisterType<V1.ServicesDao>().As<AbstractServicesDao>().InstancePerDependency();
            builder.RegisterType<V1.ServicesRequestDao>().As<AbstractServicesRequestDao>().InstancePerDependency();
            builder.RegisterType<V1.ResourcesDao>().As<AbstractResourcesDao>().InstancePerDependency();

            base.Load(builder);
        }
    }
}
