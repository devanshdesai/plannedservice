﻿using PlannedServices.Common;
using PlannedServices.Entities.Contract;
using PlannedServices.Entities.V1;
using PlannedServices.Services.Contract;
using PlannedServicesProviderPortal.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static PlannedServicesProviderPortal.Infrastructure.Enums;
using PlannedServicesProviderPortal.Infrastructure;
using PlannedServices.Common.Paging;
using SendGrid;
using System.Configuration;
using SendGrid.Helpers.Mail;
using Newtonsoft.Json;
using System.Net.Http;

namespace PlannedServicesProviderPortal.Controllers
{
    public class UserController : BaseController
    {
        #region Fields
        private readonly AbstractProviderServices abstractProviderServices;
        private readonly AbstractLookupStateServices abstractLookupStateServices;
        private readonly AbstractUserServices abstractUserServices;
        private readonly AbstractServicesServices abstractServices;
        #endregion

        #region Ctor
        public UserController(AbstractProviderServices abstractProviderServices,
            AbstractLookupStateServices abstractLookupStateServices, AbstractServicesServices abstractServices,
        AbstractUserServices abstractUserServices)
        {
            this.abstractProviderServices = abstractProviderServices;
            this.abstractLookupStateServices = abstractLookupStateServices;
            this.abstractUserServices = abstractUserServices;
            this.abstractServices = abstractServices;
        }
        #endregion


        public ActionResult PSettings()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            return View();
        }
        public ActionResult PDSettings()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            AbstractProvider abstractProvider = null;
            ViewBag.userId = 0;
            ViewBag.States = BindStateDropdown();

            int decryptedId = ProjectSession.ProviderId;
            if (decryptedId > 0)
            {
                PageParam pageParam = new PageParam();

                abstractProvider = abstractProviderServices.Provider_ById(decryptedId).Item;
                ViewBag.userId = decryptedId;
            }
            return View(abstractProvider);
        }

        public ActionResult USettings()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            PageParam pageParam = new PageParam();
            AbstractProvider abstractProvider = new Provider();
            var model = abstractProviderServices.ChildProvider_AllByParentId(pageParam, "", ProjectSession.ProviderId).Values;
            if (model.Count > 0)
            {
                abstractProvider.Provider = model;
            }
            return View(abstractProvider);
        }
        public ActionResult UserInvite()
        {

            return View();
        }


        public ActionResult PMDSettings()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            AbstractProvider abstractPlanManager = null;
            ViewBag.States = BindStateDropdown();
            if (ProjectSession.ProviderId > 0)
            {
                int decryptedId = ProjectSession.ProviderId;
                if (decryptedId > 0)
                {
                    abstractPlanManager = abstractProviderServices.Provider_ById(decryptedId).Item;
                }
            }
            return View(abstractPlanManager);
        }
        public ActionResult PaymentDetails()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            return View();
        }
        public ActionResult CPSettings()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            return View();
        }



        public ActionResult UserDetails(string userId = "")
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];

            AbstractProvider abstractProvider = null;
            ViewBag.userId = 0;
            ViewBag.States = BindStateDropdown();
            if (userId != null)
            {
                int decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(userId));
                if (decryptedId > 0)
                {
                    abstractProvider = abstractProviderServices.Provider_ById(decryptedId).Item;
                    ViewBag.userId = decryptedId;
                }
            }
            return View(abstractProvider);
        }

        //public ActionResult ProviderServices()
        //{
        //    if (TempData["openPopup"] != null)
        //        ViewBag.openPopup = TempData["openPopup"];
        //    PagedList<AbstractServices> Services = new PagedList<AbstractServices>();
        //    int decryptedId = ProjectSession.ProviderId;
        //    if (decryptedId > 0)
        //    {
        //        PageParam pageParam = new PageParam();
        //        pageParam.Offset = 0;
        //        pageParam.Limit = 100000;
        //        Services = this.abstractServices.RegistrationGroupMaster_All(pageParam, "", "");
        //        ViewBag.ProviderData = abstractProviderServices.Provider_ById(decryptedId).Item;
        //        if (ViewBag.ProviderData.RegistrationGroupId != null)
        //        {
        //            ViewBag.RegaArray = ViewBag.ProviderData.RegistrationGroupId.Split(',');
        //        }
        //        else
        //        {
        //            ViewBag.RegaArray = "";
        //        }
        //    }
        //    return View(Services);
        //}
        public ActionResult ProviderServices()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            PagedList<AbstractServices> Services = new PagedList<AbstractServices>();
            int decryptedId = ProjectSession.ProviderId;
            if (decryptedId > 0)
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 100000;
                if (ProjectSession.ProviderId == ProjectSession.ParentId)
                {
                    ViewBag.ProviderSData = this.abstractServices.RegistrationGroupMaster_All(pageParam, "", "", "General Registration Groups");
                    ViewBag.ProviderPData = this.abstractServices.RegistrationGroupMaster_All(pageParam, "", "", "Professional Registration Groups");
                    ViewBag.ProviderHData = this.abstractServices.RegistrationGroupMaster_All(pageParam, "", "", "Home and Vehicle Modification registration Groups");
                    ViewBag.ProviderAData = this.abstractServices.RegistrationGroupMaster_All(pageParam, "", "", "Assistive Technology and Equipment Registration Groups");
                }
                else
                {
                    ViewBag.ProviderSData = this.abstractServices.RegistrationGroupMaster_ByProviderId(ProjectSession.ParentId);
                }
                ViewBag.ProviderData = abstractProviderServices.Provider_ById(decryptedId).Item;
                if (ViewBag.ProviderData.RegistrationGroupId != null)
                {
                    ViewBag.RegaArray = ViewBag.ProviderData.RegistrationGroupId.Split(',');
                }
                else
                {
                    ViewBag.RegaArray = "";
                }
            }
            return View(Services);
        }


        public ActionResult ProviderServicesArea()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];

            AbstractProvider abstractProvider = null;
            int decryptedId = ProjectSession.ProviderId;
          
            List<SelectListItem> items = new List<SelectListItem>();
            //ViewBag.States = BindStateDropdown();
            ViewBag.States = items;
            ViewBag.Citys = items;
            ViewBag.IStates = items;
            ViewBag.ICitys = items;
            if (decryptedId > 0)
            {
                abstractProvider = abstractProviderServices.Provider_ById(decryptedId).Item;
                ViewBag.LocationData = abstractProviderServices.ProviderLocation_All(decryptedId).Values;
                ViewBag.ProviderData = abstractProvider;
                if (ProjectSession.ProviderId == ProjectSession.ParentId)
                {
                    ViewBag.ProviderData = abstractProvider;
                }
                else
                {
                    abstractProvider.IPinCode = ViewBag.LocationData[0].PinCode;
                    abstractProvider.ILookupStateId = ViewBag.LocationData[0].LookupStateId;
                    abstractProvider.ICity = ViewBag.LocationData[0].City;
                    abstractProvider.ProviderLocationId = ViewBag.LocationData[0].Id;
                    ViewBag.IStates = BindStateCityDropdown(0, ConvertTo.Integer(abstractProvider.IPinCode), 0);
                    ViewBag.ICitys = BindStateCityDropdown(1, ConvertTo.Integer(abstractProvider.IPinCode), abstractProvider.ILookupStateId);
                }
               

            }
            return View(abstractProvider);
        }

        // GET: User
        public ActionResult Index()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            return View();
        }
        public ActionResult UserProfile(string userId = "")
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            AbstractProvider abstractProvider = null;
            ViewBag.userId = 0;
            if (userId != null)
            {
                int decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(userId));
                if (decryptedId > 0)
                {
                    abstractProvider = abstractProviderServices.Provider_ById(decryptedId).Item;
                    //abstractProvider.Phone = abstractProvider.Phone.Remove(0, 4);
                    //if (abstractProvider.ImageUrl != null)
                    //{
                    //    abstractProvider.ImageUrl = Configurations.S3BaseUrl + users.ImageUrl;
                    //}
                    ViewBag.userId = decryptedId;
                }
            }
            return View(abstractProvider);
        }
        public ActionResult EditUserProfile(string userId = "")
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            AbstractProvider abstractProvider = null;
            ViewBag.userId = 0;
            ViewBag.States = BindStateDropdown();
            if (userId != null)
            {
                int decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(userId));
                if (decryptedId > 0)
                {
                    abstractProvider = abstractProviderServices.Provider_ById(decryptedId).Item;
                    PageParam pageParam = new PageParam();
                    abstractProvider.Provider = abstractProviderServices.Provider_AllByParentId(pageParam, "", 0, ProjectSession.ProviderId, "", "", "").Values;
                    //abstractProvider.Phone = abstractProvider.Phone.Remove(0, 4);
                    //if (abstractProvider.ImageUrl != null)
                    //{
                    //    abstractProvider.ImageUrl = Configurations.S3BaseUrl + users.ImageUrl;
                    //}
                    ViewBag.userId = decryptedId;
                }
            }
            return View(abstractProvider);
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.AddUsers)]
        public ActionResult AddUsers(Provider provider)
        {
            int Id = 0;
            try
            {
                provider.CreatedBy = ProjectSession.ProviderId;
                provider.UpdatedBy = ProjectSession.ProviderId;
                SuccessResult<AbstractProvider> result = abstractProviderServices.Provider_Upsert(provider);
                if (result != null && result.Code == 200 && result.Item != null)
                {
                    Id = result.Item.Id;
                    //TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), result.Message);
                    TempData["displayModal"] = "myModal";
                    if (provider.OfficeType == "0")
                    {
                        return RedirectToAction("PMDSettings", Pages.Controllers.User, new { Area = "" });

                    }
                    else
                    {
                        return RedirectToAction("PDSettings", Pages.Controllers.User, new { Area = "" });

                    }
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "");
            }
            return RedirectToAction("PDSettings", Pages.Controllers.User, new { Area = "" });
        }
      
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.AddSubProvider)]
        public ActionResult AddSubProvider(Provider provider)
        {
            int Id = 0;
            try
            {
                provider.ParentId = ProjectSession.ProviderId;
                SuccessResult<AbstractProvider> result = abstractProviderServices.SubProvider_Upsert(provider);
                if (result != null && result.Code == 200 && result.Item != null)
                {
                    Id = result.Item.Id;
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), result.Message);
                    return RedirectToAction("USettings", Pages.Controllers.User, new { Area = "" });
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "");
            }
            return RedirectToAction("USettings", Pages.Controllers.User, new { Area = "" });
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.PlanManagerDetails)]
        public ActionResult PlanManagerDetails(Provider planManager)
        {
            int Id = 0;
            try
            {
                planManager.Id = ProjectSession.ProviderId;
                HttpClient client = new HttpClient();

                var stringTask = client.GetStringAsync("https://maps.googleapis.com/maps/api/geocode/json?address=" + planManager.AddressLine1 + ",Aus&key=AIzaSyB97EhOhMCYhK1tZIofUkpKcGW61fdgKjg");
                var msg = stringTask;

                var s = JsonConvert.DeserializeObject<Root>(msg.Result);
                if (s.results.Count > 0)
                {
                    planManager.Lat = ConvertTo.String(s.results[0].geometry.location.lat);
                    planManager.Long = ConvertTo.String(s.results[0].geometry.location.lng);
                }
                SuccessResult<AbstractProvider> result = abstractProviderServices.Provider_Upsert(planManager);
                if (result != null && result.Code == 200 && result.Item != null)
                {
                    Id = result.Item.Id;
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), result.Message);
                    return RedirectToAction(Actions.PSettings, Pages.Controllers.User, new { Area = "" });
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "");
            }
            //return null;
            return RedirectToAction(Actions.PSettings, Pages.Controllers.User, new { Area = "" });
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.ChangePassword)]
        public ActionResult ChangePassword(Provider provider)
        {
            int Id = 0;
            try
            {
                provider.Id = ProjectSession.ProviderId;
                SuccessResult<AbstractProvider> result = abstractProviderServices.Provider_ChangePassword(provider);
                if (result != null && result.Code == 200 && result.Item != null)
                {
                    Id = result.Item.Id;
                    //TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), result.Message);
                    TempData["displayModal"] = "myModal";
                    TempData["displayModalMSG"] = "";
                    return RedirectToAction(Actions.CPSettings, Pages.Controllers.User, new { Area = "" });
                }
                else
                {
                    TempData["displayModal"] = "myModal";
                    TempData["displayModalMSG"] = result.Message;
                    //TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "");
            }
            return RedirectToAction(Actions.CPSettings, Pages.Controllers.User, new { Area = "" });
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.ChangeLocation)]
        public ActionResult ChangeLocation(Provider provider)
        {
            int Id = 0;
            try
            {
                provider.CreatedBy = ProjectSession.ProviderId;
                provider.UpdatedBy = ProjectSession.ProviderId;
                SuccessResult<AbstractProvider> result = abstractProviderServices.ChangeProviderLocation(provider);
                if (result != null && result.Code == 200 && result.Item != null)
                {
                    Id = result.Item.Id;
                    //TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), result.Message);
                    TempData["displayModal"] = "myModal2";
                    return RedirectToAction("ProviderServicesArea", Pages.Controllers.User, new { Area = "" });
                }
                else
                {
                    //TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                }
            }
            catch (Exception ex)
            {
                //TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "");
            }
            return RedirectToAction("ProviderServicesArea", Pages.Controllers.User, new { Area = "" });
        }


        [HttpPost]
        [ActionName(Actions.AddProviderService)]
        public JsonResult AddProviderService(string Ids, string PIds, string HIds, string AIds)
        {
            var result = abstractProviderServices.ProviderService_Upsert(Ids, ProjectSession.ProviderId, 1);
            var result2 = abstractProviderServices.ProviderService_Upsert(PIds, ProjectSession.ProviderId, 2);
            var result3 = abstractProviderServices.ProviderService_Upsert(HIds, ProjectSession.ProviderId, 3);
            var result4 = abstractProviderServices.ProviderService_Upsert(AIds, ProjectSession.ProviderId, 4);
            return Json(result4, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ActionName(Actions.AddProviderServiceArea)]
        public JsonResult AddProviderServiceArea(int Type, string value = "")
        {
            var result = abstractProviderServices.ProviderServiceArea_Upsert(Type, value, ProjectSession.ProviderId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName(Actions.DeleteProvider)]
        public ActionResult DeleteProvider(int UId)
        {
            var result = abstractProviderServices.DeleteProvider(UId, ProjectSession.ProviderId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName(Actions.DeleteProviderLocation)]
        public ActionResult DeleteProviderLocation(int LId)
        {
            var result = abstractProviderServices.DeleteProviderLocation(LId, ProjectSession.ProviderId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName(Actions.AddLocation)]
        public ActionResult AddLocation(int LId, int SId, string PinCode, string City)
        {
            Provider location = new Provider();
            HttpClient client = new HttpClient();

            var stringTask = client.GetStringAsync("https://maps.googleapis.com/maps/api/geocode/json?address=" + PinCode + ",Australia&key=AIzaSyB97EhOhMCYhK1tZIofUkpKcGW61fdgKjg");
            var msg = stringTask;

            var s = JsonConvert.DeserializeObject<Root>(msg.Result);
            if (s.results.Count > 0)
            {
                location.Lat = ConvertTo.String(s.results[0].geometry.location.lat);
                location.Long = ConvertTo.String(s.results[0].geometry.location.lng);
            }

            location.ProviderId = ProjectSession.ProviderId;
            location.Id = LId;
            location.LookupStateId = SId;
            location.PinCode = PinCode;
            location.City = City;

            var result = abstractProviderServices.AddLocation(location);
            abstractProviderServices.ProviderLocationPostCodeMapping_Upsert(result.Item.Id);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult VerifyEmail(string Email)
        {
            var modal = abstractProviderServices.GetProvider_ByEmail(Email);
            SuccessResult<AbstractUser> result = new SuccessResult<AbstractUser>();
            var userdata = abstractProviderServices.Provider_ById(ProjectSession.ProviderId);
            var isVerify = false;
            if (userdata.Item != null)
            {
                isVerify = userdata.Item.Email == Email ? true : false;
            }
            if (!modal && !isVerify)
            {
                var sendGridClient = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"]);

                var sendGridMessage = new SendGridMessage();
                sendGridMessage.SetFrom(ConfigurationManager.AppSettings["SendGridFromEmail"], "My Request");
                sendGridMessage.AddTo(Email, "user test"); // user email
                //sendGridMessage.AddTo("parthdobariya09@gmail.com", "user test"); // user email
                sendGridMessage.SetTemplateId("d-0bddf5d12367494f8dd47c39f83541c6");

                Random generator = new Random();
                String r = generator.Next(0, 1000000).ToString("D6");

                sendGridMessage.SetTemplateData(new EmailObj
                {
                    UName = ProjectSession.SetupUserName,
                    email = ConvertTo.Base64Encode(Convert.ToString(Email)),
                    Code = r
                });

                var response = sendGridClient.SendEmailAsync(sendGridMessage).Result;

                User user = new User();
                result.Item = user;
                result.Item.ECode = r;
                if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
                {
                    result.Code = 200;
                }
                else
                {
                    result.Code = 400;
                }
                return Json(result, JsonRequestBehavior.AllowGet);

            }
            else
            {
                result.Code = isVerify ? 401 : 403;
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [ActionName(Actions.GetState)]
        public ActionResult GetState(int Type, int PostCode, int StateId = 0)
        {
            List<ServicesRequest> items = new List<ServicesRequest>();


            if (Type == 0)
            {
                var model = abstractServices.State_AllByPostCode(PostCode);
                foreach (var item in model.Values)
                {
                    items.Add(new ServicesRequest() { Name = item.Name.ToString(), DrpId = item.Id.ToString() });
                }
            }
            else
            {
                var model = abstractServices.City_AllByPostCode(PostCode, StateId);
                foreach (var item in model.Values)
                {
                    items.Add(new ServicesRequest() { Name = item.Name.ToString(), DrpId = item.Name.ToString() });
                }
            }

            return Json(items, JsonRequestBehavior.AllowGet);
        }
        public IList<SelectListItem> BindStateDropdown()
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 100000;
            var model = abstractLookupStateServices.LookupState_All(pageParam, "").Values;

            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in model)
            {
                items.Add(new SelectListItem() { Text = item.Name.ToString(), Value = item.Id.ToString() });
            }

            return items;
        }

        [HttpPost]
        [ActionName(Actions.SendUserInvite)]
        public JsonResult SendUserInvite(string Email)
        {
            SuccessResult<AbstractUser> result = new SuccessResult<AbstractUser>();
            var modal = abstractProviderServices.GetProvider_ByEmail(Email);
            var pdata = abstractProviderServices.Provider_ById(ProjectSession.ProviderId).Item;
            if (!modal)
            {
                //TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), "Your request has been sent to 32 Providers");
                var sendGridClient = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"]);

                var sendGridMessage = new SendGridMessage();
                sendGridMessage.SetFrom(ConfigurationManager.AppSettings["SendGridFromEmail"], "My Request");
                sendGridMessage.AddTo(Email, ""); // user email
                //sendGridMessage.AddTo("anthony@plannedservices.com.au", "user test"); // user email
                sendGridMessage.SetTemplateId("d-40682f5d548b4df191190a0257856daf");

                sendGridMessage.SetTemplateData(new EmailObj
                {
                    //  UName = result.Item.FirstName + " " + result.Item.LastName,
                    email = ConvertTo.Base64Encode(Convert.ToString(Email)),
                    sid = ConvertTo.Base64Encode(Convert.ToString(ProjectSession.ProviderId)),
                    PName = pdata.CompanyName != null ? pdata.CompanyName : pdata.Name + " " + pdata.LastName

                });
                var response = sendGridClient.SendEmailAsync(sendGridMessage).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
                {
                    result.Code = 200;
                }
                else
                {
                    result.Code = 400;
                    result.Message = "something went wrong please try again.!";
                }
            }
            else
            {
                result.Code = 400;
                result.Message = "Email already exists";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ActionName(Actions.SendInvite)]
        public JsonResult SendInvite(int Id, string Email)
        {
            var result = abstractProviderServices.Provider_ById(Id);
            if (result != null && result.Code == 200 && result.Item != null)
            {
                //TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), "Your request has been sent to 32 Providers");
                var sendGridClient = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"]);

                var sendGridMessage = new SendGridMessage();
                sendGridMessage.SetFrom(ConfigurationManager.AppSettings["SendGridFromEmail"], "My Request");
                sendGridMessage.AddTo(Email, "user test"); // user email
                sendGridMessage.SetTemplateId("d-40682f5d548b4df191190a0257856daf");

                sendGridMessage.SetTemplateData(new EmailObj
                {
                    UName = result.Item.Name + " " + result.Item.LastName,
                    email = ConvertTo.Base64Encode(Convert.ToString(result.Item.Email)),
                    PName = result.Item.ParentName

                });

                var response = sendGridClient.SendEmailAsync(sendGridMessage).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
                {

                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ActionName(Actions.UploadCompanyLogo)]
        public ActionResult UploadCompanyLogo()
        {
            HttpFileCollectionBase files = Request.Files;
            var result = abstractProviderServices.CompanyLogoUpload(files);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public IList<SelectListItem> BindStateCityDropdown(int Type, int PostCode, int StateId = 0)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            if (Type == 0)
            {
                var model = abstractServices.State_AllByPostCode(PostCode);
                foreach (var item in model.Values)
                {
                    items.Add(new SelectListItem() { Text = item.Name.ToString(), Value = item.Id.ToString() });
                }
            }
            else
            {
                var model = abstractServices.City_AllByPostCode(PostCode, StateId);
                foreach (var item in model.Values)
                {
                    items.Add(new SelectListItem() { Text = item.Name.ToString(), Value = item.Name.ToString() });
                }
            }

            return items;
        }

        private class EmailObj
        {
            [JsonProperty("username")]
            public string username { get; set; }

            [JsonProperty("UName")]
            public string UName { get; set; }

            [JsonProperty("PName")]
            public string PName { get; set; }

            [JsonProperty("email")]
            public string email { get; set; }

            [JsonProperty("password")]
            public string password { get; set; }
            [JsonProperty("Code")]
            public string Code { get; set; }

            [JsonProperty("sid")]
            public string sid { get; set; }
        }
    }
}