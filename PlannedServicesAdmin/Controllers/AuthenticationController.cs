﻿using PlannedServices.Common;
using PlannedServices.Common.Paging;
using PlannedServices.Entities.Contract;
using PlannedServices.Entities.V1;
using PlannedServices.Services.Contract;
using PlannedServicesAdmin.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static PlannedServicesAdmin.Infrastructure.Enums;
using System.Net.Http;
using Newtonsoft.Json;
using SendGrid;
using System.Configuration;
using SendGrid.Helpers.Mail;

namespace PlannedServicesAdmin.Controllers
{
    public class AuthenticationController : Controller
    {
        private readonly AbstractUserServices userService;
        private readonly AbstractLookupStateServices abstractLookup;
        public AuthenticationController(AbstractUserServices _userService, AbstractLookupStateServices _abstractLookup)
        {
            userService = _userService;
            abstractLookup = _abstractLookup;
        }
        [HttpGet]
        public ActionResult SignUp()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            ViewBag.States = BindStateDropdown();
            string id = Request.QueryString["id"];
            //string id = Base64Decode(EncodedID);
            if (id != null)
            {
                SuccessResult<AbstractUser> result = userService.User_ById(Convert.ToInt32(id));
                if (result != null && result.Code == 200 && result.Item != null)
                {
                    result.Item.FirstName = result.Item.FirstName + " " + result.Item.LastName;
                    return View(result.Item);
                }
                else
                {
                    ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                }
            }
            return View();
        }

        public ActionResult ResetPassword(string stremail = "")
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];

            User user = new User();
            if (stremail != null)
            {
                string decryptedEmail = Convert.ToString(ConvertTo.Base64Decode(stremail));
                if (decryptedEmail != null && decryptedEmail != "")
                {
                    user.Email = decryptedEmail;
                    ViewBag.userId = decryptedEmail;
                }
            }
            return View(user);
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.UserSignUp)]
        public ActionResult UserSignUp(User model)
        {
            if (model.FirstName.IndexOf(" ") > 0)
            {
                string fullName = model.FirstName;
                var names = fullName.Split(' ');
                model.FirstName = names[0];
                model.LastName = names[1];
            }
            HttpClient client = new HttpClient();

            var stringTask = client.GetStringAsync("https://maps.googleapis.com/maps/api/geocode/json?address=" + model.AddressLine1 + ",Aus&key=AIzaSyB97EhOhMCYhK1tZIofUkpKcGW61fdgKjg");
            var msg = stringTask;

            var s = JsonConvert.DeserializeObject<Root>(msg.Result);
            if (s.results.Count > 0)
            {
                model.lat = s.results[0].geometry.location.lat;
                model.lng = s.results[0].geometry.location.lng;
            }
            SuccessResult<AbstractUser> result = userService.User_Upsert(model);
            if (result != null && result.Code == 200 && result.Item != null)
            {

                Session.Clear();
                ProjectSession.UserID = result.Item.Id;
                ProjectSession.UserName = result.Item.FirstName + " " + result.Item.LastName;
                ProjectSession.Email = result.Item.Email;


                HttpCookie cookie = new HttpCookie("UserLogin");
                cookie.Values.Add("Id", result.Item.Id.ToString());
                cookie.Values.Add("UserName", ProjectSession.UserName);
                cookie.Values.Add("Email", ProjectSession.Email);
                cookie.Expires = DateTime.Now.AddHours(1);
                Response.Cookies.Add(cookie);

                if (model.Id > 0)
                {
                    TempData["Plan"] = "Plan Manage";
                }
                return RedirectToAction(Actions.SignUpPlan, Pages.Controllers.Authentication);
            }
            else
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                return RedirectToAction(Actions.SignUp, Pages.Controllers.Authentication);
            }

            return RedirectToAction(Actions.Service, Pages.Controllers.ServiceRequests);
        }

        [HttpGet]
        public ActionResult SignUpPlan()
        {
            return View();
        }


        [HttpPost]
        public ActionResult SignUpPlan(string email, string password)
        {
            return View();
        }
        [HttpGet]
        public ActionResult SignUpSuccess()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Home()
        {
            return View();
        }

        public ActionResult AccountSetUp()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            return View();
        }

        [HttpGet]
        public ActionResult ParentUser()
        {
            PageParam pageParam = new PageParam();
            User user = new User();
            var model = userService.ChildUsers_AllByParentId(pageParam, "", ProjectSession.UserID).Values;
            user.Users = model;
            return View(user);
        }
        [HttpPost]
        public ActionResult SignUpSuccess(string email, string password)
        {
            return View();
        }


        [HttpGet]
        [ActionName(Actions.ForgotPassword)]
        public ActionResult ForgotPassword()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];

            return View();
        }



        [HttpGet]
        [ActionName(Actions.SignIn)]
        public ActionResult SignIn(string stremail = "")
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];

            User user = new User();
            if (stremail != null)
            {
                string decryptedEmail = Convert.ToString(ConvertTo.Base64Decode(stremail));
                if (decryptedEmail != null && decryptedEmail != "")
                {
                    user.Email = decryptedEmail;
                    var modal = userService.Users_Active(decryptedEmail);
                }
            }

            Session.Clear();
            Session.Abandon();

            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();

            if (Request.Cookies["UserLogin"] != null)
            {
                var c = new HttpCookie("UserLogin");
                c.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(c);
            }
            return View(user);
        }

        [HttpGet]
        [ActionName(Actions.ExportDataSignIn)]
        public ActionResult ExportDataSignIn()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];

            User user = new User();
            return View(user);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.LogIn)]
        public ActionResult LogIn(User model)
        {
            SuccessResult<AbstractUser> result = userService.User_Login(model);
            if (result != null && result.Code == 200 && result.Item != null)
            {
                Session.Clear();

                if (result.Item.UserType == 1)
                {
                    model.Id = result.Item.Id;
                    model.CreatedBy = result.Item.CreatedBy;
                    var parent = userService.CheckSubUser(model).Values;
                    if (parent.Count == 1)
                    {
                        ProjectSession.LoginUserID = result.Item.Id;
                        ProjectSession.LoginUserName = result.Item.FirstName + " " + result.Item.LastName;
                        ProjectSession.UserID = result.Item.CreatedBy;
                        ProjectSession.UserName = parent[0].FirstName + " " + parent[0].LastName;
                        ProjectSession.Email = parent[0].Email;
                        ProjectSession.SetupUserType = Convert.ToString(parent[0].ManagerType);
                    }
                    else
                    {
                        ProjectSession.UserID = result.Item.Id;
                        ProjectSession.LoginUserID = result.Item.Id;
                        ProjectSession.UserName = result.Item.FirstName + " " + result.Item.LastName;
                        ProjectSession.SetupUserType = Convert.ToString(result.Item.ManagerType);
                        return RedirectToAction(Actions.ParentUser, Pages.Controllers.Authentication);
                    }
                }
                else
                {
                    ProjectSession.LoginUserID = result.Item.Id;
                    ProjectSession.LoginUserName = result.Item.FirstName + " " + result.Item.LastName;
                    ProjectSession.UserID = result.Item.Id;
                    ProjectSession.UserName = result.Item.FirstName + " " + result.Item.LastName;
                    ProjectSession.Email = result.Item.Email;
                    ProjectSession.SetupUserType = Convert.ToString(result.Item.ManagerType);
                }


                HttpCookie cookie = new HttpCookie("UserLogin");
                cookie.Values.Add("Id", result.Item.Id.ToString());
                cookie.Values.Add("UserName", ProjectSession.UserName);
                cookie.Values.Add("Email", ProjectSession.Email);
                cookie.Expires = DateTime.Now.AddHours(1);
                Response.Cookies.Add(cookie);
                if (result.Item.IsUserMultiple == 2)
                {
                    return RedirectToAction("ManagerDashboard", Pages.Controllers.Dashboard);

                }
                else
                {
                    return RedirectToAction(Actions.Service, Pages.Controllers.ServiceRequests);

                }
            }
            else
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
            }
            return RedirectToAction(Actions.SignIn, Pages.Controllers.Authentication);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.ExportDataLogIn)]
        public ActionResult ExportDataLogIn(User model)
        {

            if(model.Email == "admin@admin.com" && model.Password == "*aA123123")
            {
                ProjectSession.ExpoUserID = 1;
                return RedirectToAction(Actions.Index, Pages.Controllers.SalesInvoice);
            }
            else
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "Password is incorrect !");
            }
            return RedirectToAction(Actions.ExportDataSignIn, Pages.Controllers.Authentication);
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.SetPassword)]
        public ActionResult SetPassword(User user)
        {
            int Id = 0;
            try
            {
                SuccessResult<AbstractUser> result = userService.User_RestPassword(user);
                if (result != null && result.Code == 200 && result.Item != null)
                {
                    //Id = result.Item.Id;
                    SuccessResult<AbstractUser> result1 = userService.User_Login(user);
                    if (result1 != null && result1.Code == 200 && result1.Item != null)
                    {
                        Session.Clear();

                        if (result1.Item.UserType == 1)
                        {
                            user.Id = result1.Item.Id;
                            user.CreatedBy = result1.Item.CreatedBy;
                            var parent = userService.CheckSubUser(user).Values;
                            if (parent.Count == 1)
                            {
                                ProjectSession.LoginUserID = result1.Item.Id;
                                ProjectSession.LoginUserName = result1.Item.FirstName + " " + result1.Item.LastName;
                                ProjectSession.UserID = result1.Item.CreatedBy;
                                ProjectSession.UserName = parent[0].FirstName + " " + parent[0].LastName;
                                ProjectSession.Email = parent[0].Email;
                                ProjectSession.SetupUserType = Convert.ToString(parent[0].ManagerType);
                            }
                            else
                            {
                                ProjectSession.UserID = result1.Item.Id;
                                ProjectSession.LoginUserID = result1.Item.Id;
                                ProjectSession.UserName = result1.Item.FirstName + " " + result1.Item.LastName;
                                ProjectSession.SetupUserType = Convert.ToString(result1.Item.ManagerType);
                                return RedirectToAction(Actions.ParentUser, Pages.Controllers.Authentication);
                            }
                        }
                        else
                        {
                            ProjectSession.LoginUserID = result1.Item.Id;
                            ProjectSession.LoginUserName = result1.Item.FirstName + " " + result1.Item.LastName;
                            ProjectSession.UserID = result1.Item.Id;
                            ProjectSession.UserName = result1.Item.FirstName + " " + result1.Item.LastName;
                            ProjectSession.Email = result1.Item.Email;
                            ProjectSession.SetupUserType = Convert.ToString(result1.Item.ManagerType);
                        }


                        HttpCookie cookie = new HttpCookie("UserLogin");
                        cookie.Values.Add("Id", result1.Item.Id.ToString());
                        cookie.Values.Add("UserName", ProjectSession.UserName);
                        cookie.Values.Add("Email", ProjectSession.Email);
                        cookie.Expires = DateTime.Now.AddHours(1);
                        Response.Cookies.Add(cookie);
                        return RedirectToAction(Actions.Service, Pages.Controllers.ServiceRequests);
                    }
                    else
                    {
                        TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result1.Message);
                    }
                    return RedirectToAction(Actions.SignIn, Pages.Controllers.Authentication);
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "");
            }
            return RedirectToAction("ResetPassword", Pages.Controllers.Authentication, new { Area = "", strEmail = ConvertTo.Base64Encode(Convert.ToString(user.Email)) });
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.SendEmail)]
        public ActionResult SendEmail(User user)
        {
            SuccessResult<AbstractUser> result1 = userService.User_ByEmail(user.Email, "", 1);
            if (result1 != null && result1.Code == 200 && result1.Item != null)
            {
                var sendGridClient = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"]);

                var sendGridMessage = new SendGridMessage();
                sendGridMessage.SetFrom(ConfigurationManager.AppSettings["SendGridFromEmail"], "My Request");
                sendGridMessage.AddTo(user.Email, "user test"); // user email
                //sendGridMessage.AddTo("parthdobariya09@gmail.com", "user test"); // user email
                sendGridMessage.SetTemplateId("d-9a3a0452833543feb12c4ce1f519dd2e");

                sendGridMessage.SetTemplateData(new EmailObj
                {
                    UName = result1.Item.FirstName + ' ' + result1.Item.LastName,
                    email = ConvertTo.Base64Encode(Convert.ToString(user.Email)),

                });

                var response = sendGridClient.SendEmailAsync(sendGridMessage).Result;
                var result = response;
                if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
                {
                    TempData["displayModal"] = "myModal";
                    TempData["displayModalMSG"] = "Please check your email we send you reset password link.!";
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), "Please check your email we send you reset password link.!");
                }
            }

            return RedirectToAction(Actions.SignIn, Pages.Controllers.Authentication);
        }


        [AllowAnonymous]
        [ActionName(Actions.Logout)]
        public ActionResult Logout()
        {
            Session.Clear();
            Session.Abandon();

            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();

            if (Request.Cookies["UserLogin"] != null)
            {
                var c = new HttpCookie("UserLogin");
                c.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(c);
            }

            return RedirectToAction("SignIn", Pages.Controllers.Authentication);
        }

        [HttpPost]
        [ActionName(Actions.SelectedUser)]
        public JsonResult SelectedUser(int Id)
        {
            AbstractUser abstractUser = null;
            User user = new User();
            abstractUser = userService.User_ById(Id).Item;
            user.Email = abstractUser.Email;
            user.Password = abstractUser.Password;
            SuccessResult<AbstractUser> result = userService.User_Login(user);
            if (result != null && result.Code == 200 && result.Item != null)
            {
                ProjectSession.LoginUserID = result.Item.Id;
                ProjectSession.LoginUserName = result.Item.FirstName + " " + result.Item.LastName;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public IList<SelectListItem> BindStateDropdown()
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 100000;
            var model = abstractLookup.LookupState_All(pageParam, "").Values;

            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in model)
            {
                items.Add(new SelectListItem() { Text = item.Name.ToString(), Value = item.Id.ToString() });
            }

            return items;
        }

        private class EmailObj
        {
            [JsonProperty("username")]
            public string username { get; set; }

            [JsonProperty("email")]
            public string email { get; set; }

            [JsonProperty("UName")]
            public string UName { get; set; }

            [JsonProperty("PName")]
            public string PName { get; set; }

            [JsonProperty("password")]
            public string password { get; set; }
        }
    }

    public class AddressComponent
    {
        public string long_name { get; set; }
        public string short_name { get; set; }
        public List<string> types { get; set; }
    }

    public class Northeast
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Southwest
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Bounds
    {
        public Northeast northeast { get; set; }
        public Southwest southwest { get; set; }
    }

    public class Location
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Viewport
    {
        public Northeast northeast { get; set; }
        public Southwest southwest { get; set; }
    }

    public class Geometry
    {
        public Bounds bounds { get; set; }
        public Location location { get; set; }
        public string location_type { get; set; }
        public Viewport viewport { get; set; }
    }

    public class Result
    {
        public List<AddressComponent> address_components { get; set; }
        public string formatted_address { get; set; }
        public Geometry geometry { get; set; }
        public string place_id { get; set; }
        public List<string> types { get; set; }
    }

    public class Root
    {
        public List<Result> results { get; set; }
        public string status { get; set; }
    }
}