﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlannedServices.Common;
using PlannedServices.Common.Paging;
using PlannedServices.Data.Contract;
using PlannedServices.Entities.Contract;
using PlannedServices.Entities.V1;
using Dapper;

namespace PlannedServices.Data.V1
{
    public class LookupStateDao : AbstractLookupStateDao
    {

        public override SuccessResult<AbstractLookupState> LookupState_Upsert(AbstractLookupState abstractLookupState)
        {
            SuccessResult<AbstractLookupState> LookupState = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractLookupState.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", abstractLookupState.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractLookupState.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractLookupState.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.LookupState_Upsert, param, commandType: CommandType.StoredProcedure);
                LookupState = task.Read<SuccessResult<AbstractLookupState>>().SingleOrDefault();
                LookupState.Item = task.Read<LookupState>().SingleOrDefault();
            }

            return LookupState;
        }

        public override SuccessResult<AbstractLookupState> LookupState_ById(long Id)
        {
            SuccessResult<AbstractLookupState> LookupState = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.LookupState_ById, param, commandType: CommandType.StoredProcedure);
                LookupState = task.Read<SuccessResult<AbstractLookupState>>().SingleOrDefault();
                LookupState.Item = task.Read<LookupState>().SingleOrDefault();
            }

            return LookupState;
        }

        public override PagedList<AbstractLookupState> LookupState_All(PageParam pageParam, string search)
        {
            PagedList<AbstractLookupState> LookupState = new PagedList<AbstractLookupState>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.LookupState_All, param, commandType: CommandType.StoredProcedure);
                LookupState.Values.AddRange(task.Read<LookupState>());
                LookupState.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return LookupState;
        }




    }
}
