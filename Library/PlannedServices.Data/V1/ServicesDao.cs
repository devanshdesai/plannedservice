﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlannedServices.Common;
using PlannedServices.Common.Paging;
using PlannedServices.Data.Contract;
using PlannedServices.Entities.Contract;
using PlannedServices.Entities.V1;
using Dapper;

namespace PlannedServices.Data.V1
{
    public class ServicesDao : AbstractServicesDao
    {

        public override SuccessResult<AbstractServices> Services_Upsert(AbstractServices abstractServices)
        {
            SuccessResult<AbstractServices> Services = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractServices.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", abstractServices.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractServices.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy ", abstractServices.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Services_Upsert, param, commandType: CommandType.StoredProcedure);
                Services = task.Read<SuccessResult<AbstractServices>>().SingleOrDefault();
                Services.Item = task.Read<Services>().SingleOrDefault();
            }

            return Services;
        }

        public override SuccessResult<AbstractServices> ServiceRequest_Upsert(AbstractServices abstractServices)
        {
            SuccessResult<AbstractServices> Services = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractServices.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", abstractServices.UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CategoryId", abstractServices.CategoryId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@RegistrationGroupId", abstractServices.RegistrationGroupId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@BudgetId", abstractServices.BudgetId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@SupportGoal", abstractServices.SupportGoal, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@SupportId", abstractServices.SupportItemIds, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Comment", abstractServices.Comment, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Distance", abstractServices.Distance, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@BookingRequestedDate", abstractServices.BookingRequestedDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Status ", 0, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@IsProviderContact ", abstractServices.IsProviderContact, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@PCName ", abstractServices.PCName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PCNumber ", abstractServices.PCNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PCEmail ", abstractServices.PCEmail, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ServiceRequest_Upsert, param, commandType: CommandType.StoredProcedure);
                Services = task.Read<SuccessResult<AbstractServices>>().SingleOrDefault();
                Services.Item = task.Read<Services>().SingleOrDefault();
            }

            return Services;
        }

        public override SuccessResult<AbstractServices> ServiceRequestChild_Upsert(AbstractServices abstractServices)
        {
            SuccessResult<AbstractServices> Services = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractServices.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ServiceRequestId", abstractServices.ServiceRequestId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ProviderId", abstractServices.ProviderIds, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AcceptDate", abstractServices.AcceptDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CancelDate", abstractServices.CancelDate, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ServiceRequestChild_Upsert, param, commandType: CommandType.StoredProcedure);
                Services = task.Read<SuccessResult<AbstractServices>>().SingleOrDefault();
                Services.Item = task.Read<Services>().SingleOrDefault();
            }

            return Services;
        }
        public override SuccessResult<AbstractServices> Services_ById(long Id)
        {
            SuccessResult<AbstractServices> Services = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Services_ById, param, commandType: CommandType.StoredProcedure);
                Services = task.Read<SuccessResult<AbstractServices>>().SingleOrDefault();
                Services.Item = task.Read<Services>().SingleOrDefault();
            }

            return Services;
        }
        public override SuccessResult<AbstractServices> GetUserInfo_ByServiceRequestId(long Id)
        {
            SuccessResult<AbstractServices> Services = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.GetUserInfo_ByServiceRequestId, param, commandType: CommandType.StoredProcedure);
                Services = task.Read<SuccessResult<AbstractServices>>().SingleOrDefault();
                Services.Item = task.Read<Services>().SingleOrDefault();
            }

            return Services;
        }

        public override PagedList<AbstractServices> Services_All(PageParam pageParam, string search)
        {
            PagedList<AbstractServices> Services = new PagedList<AbstractServices>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Services_All, param, commandType: CommandType.StoredProcedure);
                Services.Values.AddRange(task.Read<Services>());
                Services.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Services;
        }
        public override PagedList<AbstractServices> GetServiceRequestProviderCount(int RId, int UId)
        {
            PagedList<AbstractServices> Services = new PagedList<AbstractServices>();

            var param = new DynamicParameters();
            param.Add("@RId", RId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", UId, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.GetServiceRequestProviderCount, param, commandType: CommandType.StoredProcedure);
                Services.Values.AddRange(task.Read<Services>());
                Services.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Services;
        }
        public override PagedList<AbstractServices> GetTrialSendRequestProviderCount(int RId, int UId)
        {
            PagedList<AbstractServices> Services = new PagedList<AbstractServices>();

            var param = new DynamicParameters();
            param.Add("@RId", RId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@PinCode", UId, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.GetTrialSendRequestProviderCount, param, commandType: CommandType.StoredProcedure);
                Services.Values.AddRange(task.Read<Services>());
                Services.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Services;
        }

        public override PagedList<AbstractServices> SupportCategoryMasterByRegistrationGroupId(PageParam pageParam, string search, string GName, string BudgetName)
        {
            PagedList<AbstractServices> Services = new PagedList<AbstractServices>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@SupportGoal", GName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@BudgetName", BudgetName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.SupportCategoryMasterByRegistrationGroupId, param, commandType: CommandType.StoredProcedure);
                Services.Values.AddRange(task.Read<Services>());
                Services.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Services;
        }
        public override PagedList<AbstractServices> RegistrationGroupMasterByCategoryId(PageParam pageParam, string search, string GName, string BudgetName, int CategoryId)
        {
            PagedList<AbstractServices> Services = new PagedList<AbstractServices>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CategoryId", CategoryId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@SupportGoal", GName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@BudgetName", BudgetName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.RegistrationGroupMasterByCategoryId, param, commandType: CommandType.StoredProcedure);
                Services.Values.AddRange(task.Read<Services>());
                Services.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Services;
        }

        public override PagedList<AbstractServices> RegistrationGroupMaster_All(PageParam pageParam, string search, string GName, string Type = "")
        {
            PagedList<AbstractServices> Services = new PagedList<AbstractServices>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@SupportGoal", GName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Type", Type, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.RegistrationGroupMaster_All, param, commandType: CommandType.StoredProcedure);
                Services.Values.AddRange(task.Read<Services>());
                Services.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Services;
        }
         public override PagedList<AbstractServices> RegistrationGroupMaster_ByProviderId(int ProviderId)
        {
            PagedList<AbstractServices> Services = new PagedList<AbstractServices>();

            var param = new DynamicParameters();
            param.Add("@ProviderId", ProviderId, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.RegistrationGroupMaster_ByProviderId, param, commandType: CommandType.StoredProcedure);
                Services.Values.AddRange(task.Read<Services>());
                Services.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Services;
        }
        public override PagedList<AbstractServices> SupportGoal_AllByBudget(PageParam pageParam, string search, string BudgetName)
        {
            PagedList<AbstractServices> Services = new PagedList<AbstractServices>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@BudgetName", BudgetName, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.SupportGoal_All, param, commandType: CommandType.StoredProcedure);
                Services.Values.AddRange(task.Read<Services>());
                Services.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Services;
        }
        public override PagedList<AbstractServices> BudgetMaster_All(PageParam pageParam, string search)
        {
            PagedList<AbstractServices> Services = new PagedList<AbstractServices>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.BudgetMaster_All, param, commandType: CommandType.StoredProcedure);
                Services.Values.AddRange(task.Read<Services>());
                Services.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Services;
        }
        public override PagedList<AbstractServices> City_AllByPostCode(int PostCode, int StateId)
        {
            PagedList<AbstractServices> Services = new PagedList<AbstractServices>();

            var param = new DynamicParameters();
            param.Add("@PostCode", PostCode, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@StateId", StateId, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.City_AllByPostCode, param, commandType: CommandType.StoredProcedure);
                Services.Values.AddRange(task.Read<Services>());
                Services.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Services;
        }
        public override PagedList<AbstractServices> State_AllByPostCode(int PostCode)
        {
            PagedList<AbstractServices> Services = new PagedList<AbstractServices>();

            var param = new DynamicParameters();
            param.Add("@PostCode", PostCode, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.State_AllByPostCode, param, commandType: CommandType.StoredProcedure);
                Services.Values.AddRange(task.Read<Services>());
                Services.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Services;
        }
        public override PagedList<AbstractServices> Reason_All(PageParam pageParam, string search)
        {
            PagedList<AbstractServices> Services = new PagedList<AbstractServices>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Reason_All, param, commandType: CommandType.StoredProcedure);
                Services.Values.AddRange(task.Read<Services>());
                Services.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Services;
        }

        public override PagedList<AbstractServices> SupportItemMasterByCategoryName(PageParam pageParam, string search, int CategoryId, int RegistrationGroupId, string GName, string BudgetName)
        {
            PagedList<AbstractServices> Services = new PagedList<AbstractServices>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@RegistrationId", RegistrationGroupId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CategoryId", CategoryId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@SupportGoal", GName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@BudgetName", BudgetName, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.SupportItemMasterByCategoryName, param, commandType: CommandType.StoredProcedure);
                Services.Values.AddRange(task.Read<Services>());
                Services.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Services;
        }

        public override PagedList<AbstractServices> ServiceRequestChild_ByServiceRequestId(int ServiceRequestId)
        {
            PagedList<AbstractServices> Services = new PagedList<AbstractServices>();

            var param = new DynamicParameters();
            param.Add("@ServiceRequestId", ServiceRequestId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ServiceRequestChild_ByServiceRequestId, param, commandType: CommandType.StoredProcedure);
                Services.Values.AddRange(task.Read<Services>());
                Services.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Services;
        }

        public override SuccessResult<AbstractServices> ChangeServiceStatus(int ServiceRequestId, int ProviderId, int Type, int IsApproved, string ReasonId)
        {
            SuccessResult<AbstractServices> Services = null;
            var param = new DynamicParameters();

            param.Add("@ServiceRequestId", ServiceRequestId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ProviderId", ProviderId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Type", Type, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsApproved", IsApproved, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ReasonId", ReasonId, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ChangeServiceStatus, param, commandType: CommandType.StoredProcedure);
                Services = task.Read<SuccessResult<AbstractServices>>().SingleOrDefault();
                Services.Item = task.Read<Services>().SingleOrDefault();
            }

            return Services;
        }
        public override SuccessResult<AbstractServices> UpdateIsHideContactDetails(int ServiceRequestId, int IsHideContactDetails)
        {
            SuccessResult<AbstractServices> Services = null;
            var param = new DynamicParameters();

            param.Add("@ServiceRequestId", ServiceRequestId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsHideContactDetails", IsHideContactDetails, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UpdateIsHideContactDetails, param, commandType: CommandType.StoredProcedure);
                Services = task.Read<SuccessResult<AbstractServices>>().SingleOrDefault();
                Services.Item = task.Read<Services>().SingleOrDefault();
            }

            return Services;
        }
        public override SuccessResult<AbstractServices> UpdateProviderIsCapacity(int ProviderId, int ServiceRequestId, int IsCapacity)
        {
            SuccessResult<AbstractServices> Services = null;
            var param = new DynamicParameters();

            param.Add("@ProviderId", ProviderId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ServiceRequestId", ServiceRequestId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsCapacity", IsCapacity, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UpdateProviderIsCapacity, param, commandType: CommandType.StoredProcedure);
                Services = task.Read<SuccessResult<AbstractServices>>().SingleOrDefault();
                Services.Item = task.Read<Services>().SingleOrDefault();
            }

            return Services;
        }


        public override bool DeleteServiceRequest(int id)
        {
            bool result = false;
            var param = new DynamicParameters();

            param.Add("@Id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.DeleteServiceRequest, param, commandType: CommandType.StoredProcedure);
                result = task.SingleOrDefault<bool>();
            }

            return result;
        }
        public override bool DeleteServiceRequestProvider(int id, int pid)
        {
            bool result = false;
            var param = new DynamicParameters();

            param.Add("@Id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@ProviderId", pid, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.DeleteServiceRequestProvider, param, commandType: CommandType.StoredProcedure);
                result = task.SingleOrDefault<bool>();
            }

            return result;
        }


    }
}
