﻿using PlannedServices.Common;
using PlannedServices.Common.Paging;
using PlannedServices.Entities.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlannedServices.Services.Contract
{
    public abstract class AbstractUserServices        
    {
        public abstract SuccessResult<AbstractUser> User_Upsert(AbstractUser abstractUser);
        public abstract SuccessResult<AbstractUser> ManagerUsers_Upsert(AbstractUser abstractUser);
        public abstract SuccessResult<AbstractUser> UsersPaymentDetails_Upsert(AbstractUser abstractUser);
        public abstract SuccessResult<AbstractPlanManager> PlanManager_Upsert(AbstractPlanManager abstractPlanManager);
        public abstract SuccessResult<AbstractUser> SubUser_Upsert(AbstractUser abstractUser);
        public abstract PagedList<AbstractUser> CheckSubUser(AbstractUser abstractUser);
        public abstract PagedList<AbstractUser> ParentUsers_All(PageParam pageParam, string search, int ParentId);
        public abstract PagedList<AbstractUser> GetUserByCreatedDate(string SDate,string EDate);
        public abstract PagedList<AbstractUser> GetUserServiceRequestData(string SDate,string EDate);
        public abstract PagedList<AbstractUser> ManagerUser_byUserId(int UserId);
        public abstract PagedList<AbstractUser> UsersPaymentDetails_All();
        public abstract PagedList<AbstractUser> ChildUsers_AllByParentId(PageParam pageParam, string search, int ParentId);
        public abstract SuccessResult<AbstractUser> User_ById(int Id);
        public abstract SuccessResult<AbstractUser> Users_Active(string Email);
        public abstract SuccessResult<AbstractUser> Users_EmailVerified(string Email);
        public abstract bool Users_IsEmailExist(string Email);
        public abstract bool GetUser_ByEmail(string Email);
        public abstract SuccessResult<AbstractUser> User_ByEmail(string Email,string Password,int Type=0);
        public abstract SuccessResult<AbstractUser> UsersPaymentDetails_ById(int Id);
        public abstract SuccessResult<AbstractPlanManager> PlanManager_ById(int Id,int type);
        public abstract SuccessResult<AbstractPlanManager> UpdatePlanType(int Id,int type);
        public abstract SuccessResult<AbstractUser> UpdatePlanManager(int Id,int PlanManagerId,int Type=0);
        public abstract PagedList<AbstractPlanManager> PlanManager_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractUser> User_Login(AbstractUser abstractUser);

        public abstract SuccessResult<AbstractUser> User_ChangePassword(AbstractUser abstractUser);
        public abstract SuccessResult<AbstractUser> User_RestPassword(AbstractUser abstractUser);

        public abstract bool User_Logout(int id);

        public abstract bool DeleteUser(int id, int Deletedby);
    }
}
