﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlannedServices.Entities.Contract
{
    public abstract class AbstractServices
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int UserId { get; set; }
        public int CategoryId { get; set; }
        public int BudgetId { get; set; }
        public int RegistrationGroupId { get; set; }
        public string RegistrationGroupName { get; set; }
        public string LastName { get; set; }
        public string CompanyName { get; set; }
        public string Email { get; set; }
        public string ServiceNumber { get; set; }
        public int ServiceRequestId { get; set; }
        public int SupportItemId { get; set; }
        public int IsProviderRead { get; set; }
        public long PCount { get; set; }
        public string SupportItemIds { get; set; }
        public string ProviderIds { get; set; }
        public string Distance { get; set; }
        public string Comment { get; set; }
        public string Description { get; set; }
        public string OutcomeSort { get; set; }
        public string SupportGoal { get; set; }
        public string Unit { get; set; }
        public string BookingRequestedDate { get; set; }
        public string AcceptDate { get; set; }
        public string CancelDate { get; set; }
        public string StateName { get; set; }
        public string City { get; set; }
        public string PinCode { get; set; }
        public string stripeEmail { get; set; }
        public string stripeToken { get; set; }
        public string PCName { get; set; }
        public string PCNumber { get; set; }
        public string PCEmail { get; set; }
        public int IsProviderContact { get; set; }
        public int IsHideContactDetails { get; set; }
        public int IsCapacity { get; set; }
        public List<AbstractProvider> Provider { get; set; }
        public DateTime CreatedDate { get; set; }

        public int CreatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        public int UpdatedBy { get; set; }

        public String CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";

        public String UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";

    }
}
