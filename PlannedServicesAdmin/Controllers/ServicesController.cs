﻿using PlannedServices.Common;
using PlannedServices.Common.Paging;
using PlannedServices.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PlannedServicesAdmin.Controllers
{
    public class ServicesController : Controller
    {
        private readonly AbstractServicesServices abstractServices;
        private readonly AbstractServicesRequestServices abstractServicesRequest;
        private readonly AbstractProviderServices abstractProvider;
        public ServicesController(AbstractServicesServices abstractServices,
            AbstractServicesRequestServices abstractServicesRequest, AbstractProviderServices abstractProvider)
        {
            this.abstractServices = abstractServices;
            this.abstractServicesRequest = abstractServicesRequest;
            this.abstractProvider = abstractProvider;
        }

        // GET: Services
        public ActionResult Index()
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000000;
            var model = abstractServicesRequest.ServiceRequest_AllByCustomerId(pageParam, "", ProjectSession.UserID);
            return View(model);
        }  
        public ActionResult ServiceResponse(string ServiceId = "")
        {

            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            ViewBag.userId = 0;
            if (ServiceId != null)
            {
                int decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(ServiceId));
                if (decryptedId > 0)
                {
                    PageParam pageParam = new PageParam();
                    pageParam.Offset = 0;
                    pageParam.Limit = 1000000;
                    var model = this.abstractProvider.Provider_AllByServiceRequestId(pageParam, "", decryptedId);
                    ViewBag.ServiceRequestId = decryptedId;
                    return View(model);
                }
            }
            return View();
        }

        

    }
}