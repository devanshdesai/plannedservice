﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlannedServices.Common;
using PlannedServices.Common.Paging;
using PlannedServices.Data.Contract;
using PlannedServices.Entities.Contract;
using PlannedServices.Entities.V1;


namespace PlannedServices.Services.Contract
{
    public abstract class AbstractLookupStateServices
    {
        public abstract SuccessResult<AbstractLookupState> LookupState_Upsert(AbstractLookupState abstractLookupState);
        public abstract SuccessResult<AbstractLookupState> LookupState_ById(long Id);
        public abstract PagedList<AbstractLookupState> LookupState_All(PageParam pageParam, string search);

    }
}
