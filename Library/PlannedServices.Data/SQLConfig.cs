﻿//-----------------------------------------------------------------------
// <copyright file="SQLConfig.cs" company="Rushkar">
//     Copyright Rushkar. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace PlannedServices.Data
{
    /// <summary>
    /// SQL configuration class which holds stored procedure name.
    /// </summary>
    internal sealed class SQLConfig
    {
        #region LookupState
        public const string LookupState_All = "LookupState_All";
        public const string LookupState_ParentId = "LookupState_ParentId";
        public const string LookupState_ById = "LookupState_ById";
        public const string LookupState_Upsert = "LookupState_Upsert";
        public const string LookupState_ActInact = "LookupState_ActInact";
        public const string LookupState_Delete = "LookupState_Delete";
        #endregion

        #region Resources
        public const string Resources_All = "Resources_All";
        public const string Resources_ParentId = "Resources_ParentId";
        public const string Resources_ById = "Resources_ById";
        public const string Resources_Upsert = "Resources_Upsert";
        public const string Resources_ActInact = "Resources_ActInact";
        public const string Resources_Delete = "Resources_Delete";
        #endregion

        #region Services
        public const string Services_All = "Services_All";
        public const string GetServiceRequestProviderCount = "GetServiceRequestProviderCount";
        public const string GetTrialSendRequestProviderCount = "GetTrialSendRequestProviderCount";
        public const string Services_ParentId = "Services_ParentId";
        public const string Services_ById = "Services_ById";
        public const string GetUserInfo_ByServiceRequestId = "GetUserInfo_ByServiceRequestId";
        public const string Services_Upsert = "Services_Upsert";
        public const string Services_ActInact = "Services_ActInact";
        public const string Services_Delete = "Services_Delete";
        public const string SupportCategoryMasterByRegistrationGroupId = "SupportCategoryMasterByRegistrationGroupId";
        public const string RegistrationGroupMasterByCategoryId = "RegistrationGroupMasterByCategoryId";
        public const string BudgetMaster_All = "BudgetMaster_All";
        public const string City_AllByPostCode = "City_AllByPostCode";
        public const string State_AllByPostCode = "State_AllByPostCode";
        public const string Reason_All = "Reason_All";
        public const string RegistrationGroupMaster_All = "RegistrationGroupMaster_All";
        public const string RegistrationGroupMaster_ByProviderId = "RegistrationGroupMaster_ByProviderId";
        public const string SupportGoal_All = "SupportGoal_All";
        public const string SupportItemMasterByCategoryName = "SupportItemMasterByCategoryName";
        public const string ServiceRequest_Upsert = "ServiceRequest_Upsert";
        public const string ServiceRequestChild_Upsert = "ServiceRequestChild_Upsert";
        public const string ServiceRequests_AllByCustomerId = "ServiceRequests_AllByCustomerId";
        public const string ClosedServiceRequestByCustomerId = "ClosedServiceRequestByCustomerId";
        public const string ChangeServiceStatus = "ChangeServiceStatus";
        public const string UpdateIsHideContactDetails = "UpdateIsHideContactDetails";
        public const string UpdateProviderIsCapacity = "UpdateProviderIsCapacity";
        public const string CommunicationIsRead = "CommunicationIsRead";
        public const string ServiceRequestChild_ByServiceRequestId = "ServiceRequestChild_ByServiceRequestId";
        #endregion

        #region ServicesRequest
        public const string ServiceRequest_All = "ServiceRequest_All";
        public const string ClosedServiceRequestParticipateAll = "ClosedServiceRequestParticipateAll";
        public const string ServicesRequest_ParentId = "ServicesRequest_ParentId";
        public const string ServicesRequest_ById = "ServicesRequest_ById";
        public const string ServicesRequest_Upsert = "ServicesRequest_Upsert";
        public const string ServicesRequest_ActInact = "ServicesRequest_ActInact";
        public const string ServicesRequest_Delete = "ServicesRequest_Delete";
        public const string ServiceRequestSupportItemProvider_Upsert = "ServiceRequestSupportItemProvider_Upsert";
        public const string UpdateServiceRequestChildByProviderId = "UpdateServiceRequestChildByProviderId";
        public const string Communication_Upsert = "Communication_Upsert";
        public const string Communication_AllByProviderId = "Communication_AllByProviderId";
        public const string ServiceRequest_AllByCustomerId = "ServiceRequest_AllByCustomerId";
        public const string DeleteServiceRequest = "DeleteServiceRequest";
        public const string DeleteServiceRequestProvider = "DeleteServiceRequestProvider";
        public const string GetParticipantsChartData = "GetParticipantsChartData";
        public const string GetProviderChartData = "GetProviderChartData";
 
        #endregion

        //#region UserServices
        public const string Users_Upsert = "Users_Upsert";
        public const string ManagerUsers_Upsert = "ManagerUsers_Upsert";
        public const string PlanManager_Upsert = "PlanManager_Upsert";
        public const string SubUsers_Upsert = "SubUsers_Upsert";
        public const string UsersPaymentDetails_Upsert = "UsersPaymentDetails_Upsert";
        public const string CheckSubUser = "CheckSubUser";
        public const string Users_Login = "Users_Login";
        public const string Users_ChangePassword = "Users_ChangePassword";
        public const string User_RestPassword = "User_RestPassword";
        public const string Users_Logout = "Users_Logout";        
        public const string ParentUsers_All = "ParentUsers_All";        
        public const string GetUserByCreatedDate = "GetUserByCreatedDate";        
        public const string GetUserServiceRequestData = "GetUserServiceRequestData";        
        public const string UsersPaymentDetails_All = "UsersPaymentDetails_All";        
        public const string ManagerUser_byUserId = "ManagerUser_byUserId";        
        public const string ChildUsers_AllByParentId = "ChildUsers_AllByParentId";
        public const string DeleteUser = "DeleteUser";        
        public const string Users_ById = "Users_ById";
        public const string Users_Active = "Users_Active";
        public const string Users_EmailVerified = "Users_EmailVerified";
        public const string Users_IsEmailExist = "Users_IsEmailExist";
        public const string GetUser_ByEmail = "GetUser_ByEmail";
        public const string Users_ByEmail = "Users_ByEmail";
        public const string UsersPaymentDetails_ById = "UsersPaymentDetails_ById";
        public const string PlanManager_ById = "PlanManager_ById";
        public const string UpdatePlanType = "UpdatePlanType";
        public const string UpdatePlanManager = "UpdatePlanManager";
        public const string PlanManager_All = "PlanManager_All";
        public const string DeleteServiceRequestSupportItemProvider = "DeleteServiceRequestSupportItemProvider";
        //public const string User_Delete = "User_Delete";
        //#endregion

        #region Provider
        public const string Provider_All = "Provider_All";
        public const string Provider_AllByParentId = "Provider_AllByParentId";
        public const string Provider_AllByServiceRequestId = "Provider_AllByServiceRequestId";
        public const string ClosedServiceRequestProviderAll = "ClosedServiceRequestProviderAll";
        public const string CheckSubProvider = "CheckSubProvider";
        public const string ChildProvider_AllByParentId = "ChildProvider_AllByParentId";
        public const string Provider_ById = "Provider_ById";
        public const string GetProviderByCreatedDate = "GetProviderByCreatedDate";
        public const string Provider_Upsert = "Provider_Upsert";
        public const string ChangeProviderLocation = "ChangeProviderLocation";
        public const string SubProvider_Upsert = "SubProvider_Upsert";
        public const string ProviderService_Upsert = "ProviderService_Upsert";
        public const string ProviderServiceArea_Upsert = "ProviderServiceArea_Upsert";
        public const string Provider_Delete = "Provider_Delete";
        public const string Provider_ActInactive = "Provider_ActInactive";
        public const string Provider_Login = "Provider_Login";
        public const string Provider_Logout = "Provider_Logout";
        public const string DeleteProvider = "DeleteProvider";
        public const string DeleteProviderLocation = "DeleteProviderLocation";
        public const string Provider_ChangePassword = "Provider_ChangePassword";
        public const string Provider_RestPassword = "Provider_RestPassword";
        public const string ProviderLocation_Upsert = "ProviderLocation_Upsert";
        public const string ProviderLocationPostCodeMapping_Upsert = "ProviderLocationPostCodeMapping_Upsert";
        public const string ProviderLocation_All = "ProviderLocation_All";
        public const string ProviderLocation_ById = "ProviderLocation_ById";
        public const string Provider_Active = "Provider_Active";
        public const string Provider_EmailVerified = "Provider_EmailVerified";
        public const string Provider_IsEmailExist = "Provider_IsEmailExist";
        public const string GetProvider_ByEmail = "GetProvider_ByEmail";
        public const string Provider_ByEmail = "Provider_ByEmail";
        #endregion
    }
}
