﻿namespace PlannedServicesAdmin.Pages
{
    public class Actions
    {
        public const string Index = "Index";
        public const string Search = "Search";
        public const string ManagerDashboard = "ManagerDashboard";


        #region Authentication
        public const string SignIn = "SignIn";
        public const string ExportDataSignIn = "ExportDataSignIn";
        public const string SignUp = "SignUp";
        public const string UserSignUp = "UserSignUp";
        public const string SignUpPlan = "SignUpPlan";
        public const string SignUpSuccess = "SignUpSuccess";
        public const string RecoverPassword = "RecoverPassword";
        public const string Logout = "Logout";
        public const string LogIn = "LogIn";
        public const string ExportDataLogIn = "ExportDataLogIn";
        public const string ParentUser = "ParentUser";
        public const string SetPassword = "SetPassword";
        public const string ForgotPassword = "ForgotPassword";

        #endregion

        #region AccountSetup
        public const string AccountSetUp = "AccountSetUp";
        public const string PMDSettings = "PMDSettings";
        public const string PDSettings = "PDSettings";
        public const string CompanyParticipate = "CompanyParticipate";
        public const string CompanyParicipateStep2 = "CompanyParicipateStep2";
        public const string SignUpStep2 = "SignUpStep2";
        public const string PDManagerStep2 = "PDManagerStep2";
        public const string Location = "Location";
        public const string PlanManagerDetails = "PlanManagerDetails";
        public const string PaymentDetails = "PaymentDetails";
        public const string AddPaymentDetails = "AddPaymentDetails";
        public const string UserList = "UserList";
        public const string DeleteUser = "DeleteUser";
        public const string GetState = "GetState";
        public const string GetCity = "GetCity";
        #endregion

        #region Provider
        public const string FindProvider = "FindProvider";
        public const string FilterData = "FilterData";
      
        #endregion
        
        #region User
        public const string AddUsers = "AddUsers";
        public const string AddComapanyUsers = "AddComapanyUsers";
        public const string AddUpdateComapanyUsers = "AddUpdateComapanyUsers";
        public const string AddUpdateUsers = "AddUpdateUsers";
        public const string AddUpdateManagerUsers = "AddUpdateManagerUsers";
        public const string AddInviteUsers = "AddInviteUsers";
        public const string AddLocation = "AddLocation";
        public const string AddManageType = "AddManageType";
        public const string AddSubUsers = "AddSubUsers";
        public const string UserProfile = "UserProfile";
        public const string SelectedUser = "SelectedUser";
        public const string PersonalDetails = "PersonalDetails";
        public const string ChangePassword = "ChangePassword";
        public const string PSettings = "PSettings";
        public const string CPSettings = "CPSettings";
        public const string UpdatePlanType = "UpdatePlanType";
        public const string UpdatePlanManager = "UpdatePlanManager";
        public const string ManageType = "ManageType";
        public const string PDStep2 = "PDStep2";
        public const string VerifyEmail = "VerifyEmail";
        public const string InviteUsers = "InviteUsers";
        public const string Upload = "Upload";
    
        #endregion

        #region Service
        public const string SendRequest = "SendRequest";
        public const string GetTrialSendRequestProviderCount = "GetTrialSendRequestProviderCount";
        public const string GetSendRequestProviderCount = "GetSendRequestProviderCount";
        public const string ServiceResponse = "ServiceResponse";
        public const string RegistrationGroupByCategoryId = "RegistrationGroupByCategoryId";
        public const string ViewServiceRequest = "ViewServiceRequest";
        public const string AddServiceRequest = "AddServiceRequest";
        public const string AddCommunication = "AddCommunication";
        public const string ServiceDetails = "ServiceDetails";
        public const string ChangeServiceStatus = "ChangeServiceStatus";
        public const string UpdateIsHideContactDetails = "UpdateIsHideContactDetails";
        public const string GetCommunication = "GetCommunication";
        public const string SaveServiceRequest = "SaveServiceRequest";
        public const string SaveSelfMServiceRequest = "SaveSelfMServiceRequest";
        public const string DeleteServiceRequest = "DeleteServiceRequest";
        public const string DeleteServiceRequestProvider = "DeleteServiceRequestProvider";
        public const string SendInvite = "SendInvite";
        public const string SendUserInvite = "SendUserInvite";
        public const string SendEmail = "SendEmail";
        public const string SendManagerEmail = "SendManagerEmail";
        public const string Service = "Service";
        public const string GetUserByCreatedDate = "GetUserByCreatedDate";
        public const string GetUserRequestByCreatedDate = "GetUserRequestByCreatedDate";
        public const string DownloadExcel = "DownloadExcel";
        public const string DownloadRequestExcel = "DownloadRequestExcel";
        #endregion

        //public const string Signin = "Signin";
        //public const string Salons = "Salons";
        //public const string Signout = "Signout";
        //public const string Index = "Index";
        //public const string Delete = "Delete";
        //public const string ViewAllData = "ViewAllData";
        //public const string ViewAllDataSalons = "ViewAllDataSalons";
        //public const string ViewAllDataCustomer = "ViewAllDataCustomer";
        //public const string ActiveInActive = "ActiveInActive";
        //public const string UserGallery = "UserGallery";
        //public const string ChangeStatusSalon = "ChangeStatusSalon";
        //public const string ManageCustomers = "ManageCustomers";
        //public const string ManageEmployeeLeaves = "ManageEmployeeLeaves";
        //public const string ViewAllDataEmployees = "ViewAllDataEmployees";
        //public const string ViewAllDataEmployeeLeaves = "ViewAllDataEmployeeLeaves";
        //public const string ManageEmployeeWorksheet = "ManageEmployeeWorksheet";
        //public const string ViewAllDataEmployeeWorkSheet = "ViewAllDataEmployeeWorkSheet";
        //public const string ManageEmployees = "ManageEmployees";
        //public const string ManageSalonServices = "ManageSalonServices";
        //public const string ViewAllDataSalonServices = "ViewAllDataSalonServices";
        //public const string ChangePassword = "ChangePassword";
        //public const string ViewAllDataAppointments = "ViewAllDataAppointments";
        //public const string ManageAppointments = "ManageAppointments";
        //public const string BindServicesDropdown = "BindServicesDropdown";
        //public const string ViewDataCustomerDetails = "ViewDataCustomerDetails";
        //public const string ViewAllDataEmployeeLeavecount = "ViewAllDataEmployeeLeavecount";
        //public const string GetAppointmentDetaislById = "GetAppointmentDetaislById";
        //public const string ManageEmployeeAndCharges = "ManageEmployeeAndCharges";
        //public const string ViewAllDataEmployeeAndCharges = "ViewAllDataEmployeeAndCharges";
        //public const string ViewAllDataMasterProductType = "ViewAllDataMasterProductType";
        //public const string MasterProductType = "MasterProductType";
        //public const string ActiveInActiveProductType = "ActiveInActiveProductType";
        //public const string DeleteProductType = "DeleteProductType";
        //public const string ProductTypeUpsert = "ProductTypeUpsert";
        //public const string GetDataProductTypeById = "GetDataProductTypeById";

        //public const string GetDataProductBrandById = "GetDataProductBrandById";
        //public const string ProductBrandUpsert = "ProductBrandUpsert";
        //public const string DeleteProductBrand = "DeleteProductBrand";
        //public const string ViewAllDataMasterProductBrand = "ViewAllDataMasterProductBrand";
        //public const string ActiveInActiveProductBrand = "ActiveInActiveProductBrand";
        //public const string MasterProductBrand = "MasterProductBrand";

        //public const string ViewAllDataMasterProductWeight = "ViewAllDataMasterProductWeight";
        //public const string ActiveInActiveProductWeight = "ActiveInActiveProductWeight";
        //public const string DeleteProductWeight = "DeleteProductWeight";
        //public const string ProductWeightUpsert = "ProductWeightUpsert";
        //public const string GetDataProductweightById = "GetDataProductweightById";
        //public const string MasterProductWeight = "MasterProductWeight";

        //public const string ViewAllDataMasterOrderStatus = "ViewAllDataMasterOrderStatus";
        //public const string ActiveInActiveOrderStatus = "ActiveInActiveOrderStatus";
        //public const string DeleteOrderStatus = "DeleteOrderStatus";
        //public const string OrderStatusUpsert = "OrderStatusUpsert";
        //public const string GetDataOrderstatusById = "GetDataOrderstatusById";
        //public const string MasterOrderStatus = "MasterOrderStatus";
        //public const string MasterProductCategory = "MasterProductCategory";
        //public const string ViewAllDataMasterProductCategory = "ViewAllDataMasterProductCategory";
        //public const string ActiveInActiveProductCategory = "ActiveInActiveProductCategory";
        //public const string DeleteProductCategory = "DeleteProductCategory";
        //public const string ProductCategoryUpsert = "ProductCategoryUpsert";
        //public const string GetDataProductCategoryById = "GetDataProductCategoryById";
    }
}