﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlannedServices.Common;
using PlannedServices.Common.Paging;
using PlannedServices.Data.Contract;
using PlannedServices.Entities.Contract;
using PlannedServices.Entities.V1;
using Dapper;

namespace PlannedServices.Data.V1
{
    public class ProviderDao : AbstractProviderDao
    {

        public override SuccessResult<AbstractProvider> Provider_Upsert(AbstractProvider abstractProvider)
        {
            SuccessResult<AbstractProvider> Provider = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractProvider.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", abstractProvider.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LastName", abstractProvider.LastName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AddressLine1", abstractProvider.AddressLine1, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AddressLine2", abstractProvider.AddressLine2, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LookupStateId", abstractProvider.LookupStateId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Pincode", abstractProvider.PinCode, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@City", abstractProvider.Surburb, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Phone", abstractProvider.Phone, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Email", abstractProvider.Email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Password", abstractProvider.Password, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@WebSite", abstractProvider.Website, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@OfficeType", abstractProvider.OfficeType, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractProvider.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Lat", abstractProvider.Lat, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Long", abstractProvider.Long, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LastInviteEmailSentDate", abstractProvider.LastInviteEmailSentDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractProvider.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserType", abstractProvider.UserType, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ParentId", abstractProvider.ParentId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ServiceableArea", abstractProvider.ServiceableArea, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CompanyEmail", abstractProvider.CompanyEmail, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CompanyName", abstractProvider.CompanyName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ABN", abstractProvider.ABN, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@NDISReferenceNumber", abstractProvider.NDISReferenceNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ProviderProfileAndServices", abstractProvider.ProviderProfileAndServices, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@FileName", abstractProvider.FileName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@FileURL", abstractProvider.FileURL, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CompanyLogoURL", abstractProvider.CompanyLogoURL, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Position", abstractProvider.Position, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Provider_Upsert, param, commandType: CommandType.StoredProcedure);
                Provider = task.Read<SuccessResult<AbstractProvider>>().SingleOrDefault();
                Provider.Item = task.Read<Provider>().SingleOrDefault();
            }

            return Provider;
        }
        public override SuccessResult<AbstractProvider> ChangeProviderLocation(AbstractProvider abstractProvider)
        {
            SuccessResult<AbstractProvider> Provider = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractProvider.ProviderLocationId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ProviderId", abstractProvider.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@LookupStateId", abstractProvider.ILookupStateId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@PinCode", abstractProvider.IPinCode, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@City", abstractProvider.ICity, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ChangeProviderLocation, param, commandType: CommandType.StoredProcedure);
                Provider = task.Read<SuccessResult<AbstractProvider>>().SingleOrDefault();
                Provider.Item = task.Read<Provider>().SingleOrDefault();
            }

            return Provider;
        }

        public override SuccessResult<AbstractProvider> SubProvider_Upsert(AbstractProvider abstractProvider)
        {
            SuccessResult<AbstractProvider> Provider = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractProvider.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@ParentId", abstractProvider.ParentId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Name", abstractProvider.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LastName", abstractProvider.LastName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Email", abstractProvider.Email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Password", abstractProvider.Password, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Title", abstractProvider.Title, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LookupStateId", abstractProvider.LookupStateId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@PinCode", abstractProvider.PinCode, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@City", abstractProvider.City, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.SubProvider_Upsert, param, commandType: CommandType.StoredProcedure);
                Provider = task.Read<SuccessResult<AbstractProvider>>().SingleOrDefault();
                Provider.Item = task.Read<Provider>().SingleOrDefault();
            }

            return Provider;
        }
        public override SuccessResult<AbstractProvider> ProviderService_Upsert(string Ids, int PId, int Type)
        {
            SuccessResult<AbstractProvider> Provider = null;
            var param = new DynamicParameters();

            param.Add("@Id", PId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@RegistrationGroupId", Ids, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Type", Type, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProviderService_Upsert, param, commandType: CommandType.StoredProcedure);
                Provider = task.Read<SuccessResult<AbstractProvider>>().SingleOrDefault();
                Provider.Item = task.Read<Provider>().SingleOrDefault();
            }

            return Provider;
        } 
        public override SuccessResult<AbstractProvider> ProviderServiceArea_Upsert(int Type, string value, int PId)
        {
            SuccessResult<AbstractProvider> Provider = null;
            var param = new DynamicParameters();

            param.Add("@Id", PId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Type", Type, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ServiceableArea", value, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProviderServiceArea_Upsert, param, commandType: CommandType.StoredProcedure);
                Provider = task.Read<SuccessResult<AbstractProvider>>().SingleOrDefault();
                Provider.Item = task.Read<Provider>().SingleOrDefault();
            }

            return Provider;
        }
        public override SuccessResult<AbstractProvider> Provider_ById(long Id, long UserId = 0)
        {
            SuccessResult<AbstractProvider> Provider = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Provider_ById, param, commandType: CommandType.StoredProcedure);
                Provider = task.Read<SuccessResult<AbstractProvider>>().SingleOrDefault();
               Provider.Item = task.Read<Provider>().SingleOrDefault();
            }

            return Provider;
        }
        public override PagedList<AbstractProvider> GetProviderByCreatedDate(string SDate, string EDate)
        {
            PagedList<AbstractProvider> Provider = new PagedList<AbstractProvider>();
            var param = new DynamicParameters();

            param.Add("@SDate", SDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@EDate", EDate, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.GetProviderByCreatedDate, param, commandType: CommandType.StoredProcedure);
                Provider.Values.AddRange(task.Read<Provider>());
                Provider.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Provider;
        }

        public override PagedList<AbstractProvider> Provider_All(PageParam pageParam, string search, long LookupStateId, string Name, string PostCode, string State)
        {
            PagedList<AbstractProvider> Provider = new PagedList<AbstractProvider>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Name", Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PostCode", PostCode, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@State", State, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LookupStateId", LookupStateId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Provider_All, param, commandType: CommandType.StoredProcedure);
                Provider.Values.AddRange(task.Read<Provider>());
                Provider.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Provider;
        }
         public override PagedList<AbstractProvider> Provider_AllByParentId(PageParam pageParam, string search, long LookupStateId, long ParentId, string Name, string PostCode, string State)
        {
            PagedList<AbstractProvider> Provider = new PagedList<AbstractProvider>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Name", Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PostCode", PostCode, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@State", State, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LookupStateId", LookupStateId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ParentId", ParentId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Provider_AllByParentId, param, commandType: CommandType.StoredProcedure);
                Provider.Values.AddRange(task.Read<Provider>());
                Provider.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Provider;
        }

        public override PagedList<AbstractProvider> Provider_AllByServiceRequestId(PageParam pageParam, string search, int ServiceRequestId)
        {
            PagedList<AbstractProvider> Provider = new PagedList<AbstractProvider>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ServiceRequestId", ServiceRequestId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Provider_AllByServiceRequestId, param, commandType: CommandType.StoredProcedure);
                Provider.Values.AddRange(task.Read<Provider>());
                Provider.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Provider;
        }
        public override PagedList<AbstractProvider> ClosedServiceRequestProviderAll(PageParam pageParam, string search, int ServiceRequestId)
        {
            PagedList<AbstractProvider> Provider = new PagedList<AbstractProvider>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ServiceRequestId", ServiceRequestId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ClosedServiceRequestProviderAll, param, commandType: CommandType.StoredProcedure);
                Provider.Values.AddRange(task.Read<Provider>());
                Provider.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Provider;
        }
        public override PagedList<AbstractProvider> CheckSubProvider(PageParam pageParam, int Id)
        {
            PagedList<AbstractProvider> Provider = new PagedList<AbstractProvider>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CheckSubProvider, param, commandType: CommandType.StoredProcedure);
                Provider.Values.AddRange(task.Read<Provider>());
                Provider.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Provider;
        }
        public override SuccessResult<AbstractProvider> Provider_ActInactive(long Id)
        {
            SuccessResult<AbstractProvider> Provider = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Provider_ActInactive, param, commandType: CommandType.StoredProcedure);
                Provider = task.Read<SuccessResult<AbstractProvider>>().SingleOrDefault();
                Provider.Item = task.Read<Provider>().SingleOrDefault();
            }

            return Provider;
        }

     

        public override SuccessResult<AbstractProvider> Provider_Login(AbstractProvider abstractProvider)
        {
            SuccessResult<AbstractProvider> Provider = null;
            var param = new DynamicParameters();

            param.Add("@Email", abstractProvider.Email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Password", abstractProvider.Password, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Provider_Login, param, commandType: CommandType.StoredProcedure);
                Provider = task.Read<SuccessResult<AbstractProvider>>().SingleOrDefault();
                Provider.Item = task.Read<Provider>().SingleOrDefault();
            }

            return Provider;
        }

        public override bool Provider_Logout(long Id)
        {
            bool result = false;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.Provider_Logout, param, commandType: CommandType.StoredProcedure);
                result = task.SingleOrDefault<bool>();
            }

            return result;
        }
        public override  bool DeleteProvider(int id, int DeletedBy)
        {
            bool result = false;
            var param = new DynamicParameters();

            param.Add("@Id", id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.DeleteProvider, param, commandType: CommandType.StoredProcedure);
                result = task.SingleOrDefault<bool>();
            }

            return result;
        }
        public override  bool DeleteProviderLocation(int LId, int ProviderId)
        {
            bool result = false;
            var param = new DynamicParameters();

            param.Add("@Id", LId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ProviderId", ProviderId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.DeleteProviderLocation, param, commandType: CommandType.StoredProcedure);
                result = task.SingleOrDefault<bool>();
            }

            return result;
        }
        public override SuccessResult<AbstractProvider> Provider_ChangePassword(AbstractProvider abstractProvider)
        {
            SuccessResult<AbstractProvider> Provider = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractProvider.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OldPassword", abstractProvider.OldPassword, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@NewPassword", abstractProvider.NewPassword, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ConfirmPassword", abstractProvider.ConfirmPassword, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Provider_ChangePassword, param, commandType: CommandType.StoredProcedure);
                Provider = task.Read<SuccessResult<AbstractProvider>>().SingleOrDefault();
                Provider.Item = task.Read<Provider>().SingleOrDefault();
            }

            return Provider;
        }
        public override SuccessResult<AbstractProvider> Provider_RestPassword(AbstractProvider abstractProvider)
        {
            SuccessResult<AbstractProvider> Provider = null;
            var param = new DynamicParameters();

            param.Add("@Email", abstractProvider.Email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Password", abstractProvider.Password, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Provider_RestPassword, param, commandType: CommandType.StoredProcedure);
                Provider = task.Read<SuccessResult<AbstractProvider>>().SingleOrDefault();
                Provider.Item = task.Read<Provider>().SingleOrDefault();
            }

            return Provider;
        }

        public override PagedList<AbstractProvider> ChildProvider_AllByParentId(PageParam pageParam, string search, int ParentId)
        {
            PagedList<AbstractProvider> Provider = new PagedList<AbstractProvider>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ParentId", ParentId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ChildProvider_AllByParentId, param, commandType: CommandType.StoredProcedure);
                Provider.Values.AddRange(task.Read<Provider>());
                Provider.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Provider;
        }

        public override SuccessResult<AbstractProvider> AddLocation(AbstractProvider abstractProvider)
        {
            SuccessResult<AbstractProvider> Provider = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractProvider.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ProviderId", abstractProvider.ProviderId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@LookupStateId", abstractProvider.LookupStateId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@PinCode", abstractProvider.PinCode, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@City", abstractProvider.City, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Lat", abstractProvider.Lat, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Long", abstractProvider.Long, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProviderLocation_Upsert, param, commandType: CommandType.StoredProcedure);
                Provider = task.Read<SuccessResult<AbstractProvider>>().SingleOrDefault();
                Provider.Item = task.Read<Provider>().SingleOrDefault();
            }

            return Provider;
        }
        public override SuccessResult<AbstractProvider> ProviderLocationPostCodeMapping_Upsert(long Id)
        {
            SuccessResult<AbstractProvider> Provider = null;
            var param = new DynamicParameters();

            param.Add("@ProviderLocationId", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProviderLocationPostCodeMapping_Upsert, param, commandType: CommandType.StoredProcedure);
                Provider = task.Read<SuccessResult<AbstractProvider>>().SingleOrDefault();
                Provider.Item = task.Read<Provider>().SingleOrDefault();
            }

            return Provider;
        }
        public override SuccessResult<AbstractProvider> ProviderLocation_ById(long Id)
        {
            SuccessResult<AbstractProvider> Provider = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProviderLocation_ById, param, commandType: CommandType.StoredProcedure);
                Provider = task.Read<SuccessResult<AbstractProvider>>().SingleOrDefault();
                Provider.Item = task.Read<Provider>().SingleOrDefault();
            }

            return Provider;
        }
        public override SuccessResult<AbstractProvider> Provider_Active(string Email)
        {
            SuccessResult<AbstractProvider> Provider = null;
            var param = new DynamicParameters();

            param.Add("@Email", Email, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Provider_Active, param, commandType: CommandType.StoredProcedure);
                Provider = task.Read<SuccessResult<AbstractProvider>>().SingleOrDefault();
                Provider.Item = task.Read<Provider>().SingleOrDefault();
            }

            return Provider;
        }
        public override SuccessResult<AbstractProvider> Provider_EmailVerified(string Email)
        {
            SuccessResult<AbstractProvider> Provider = null;
            var param = new DynamicParameters();

            param.Add("@Email", Email, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Provider_EmailVerified, param, commandType: CommandType.StoredProcedure);
                Provider = task.Read<SuccessResult<AbstractProvider>>().SingleOrDefault();
                Provider.Item = task.Read<Provider>().SingleOrDefault();
            }

            return Provider;
        }

        public override bool Provider_IsEmailExist(string Email)
        {
            bool result = false;
            var param = new DynamicParameters();

            param.Add("@Email", Email, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.Provider_IsEmailExist, param, commandType: CommandType.StoredProcedure);
                result = task.SingleOrDefault<bool>();
            }

            return result;
        }
        public override bool GetProvider_ByEmail(string Email)
        {
            bool result = false;
            var param = new DynamicParameters();

            param.Add("@Email", Email, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.GetProvider_ByEmail, param, commandType: CommandType.StoredProcedure);
                result = task.SingleOrDefault<bool>();
            }

            return result;
        }
        
        public override SuccessResult<AbstractProvider> Provider_ByEmail(string Email, int Type = 0)
        {
            SuccessResult<AbstractProvider> Provider = null;
            var param = new DynamicParameters();

            param.Add("@Email", Email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Type", Type, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Provider_ByEmail, param, commandType: CommandType.StoredProcedure);
                Provider = task.Read<SuccessResult<AbstractProvider>>().SingleOrDefault();
                Provider.Item = task.Read<Provider>().SingleOrDefault();
            }

            return Provider;
        }

        public override PagedList<AbstractProvider> ProviderLocation_All(long PId)
        {
            PagedList<AbstractProvider> Provider = new PagedList<AbstractProvider>();

            var param = new DynamicParameters();
            param.Add("@ProviderId", PId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ProviderLocation_All, param, commandType: CommandType.StoredProcedure);
                Provider.Values.AddRange(task.Read<Provider>());
                Provider.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Provider;
        }
    }
}
