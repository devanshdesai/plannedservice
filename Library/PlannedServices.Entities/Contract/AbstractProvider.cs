﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlannedServices.Entities.Contract
{
    public abstract class AbstractProvider
    {
        public int Id { get; set; }
        public int ProviderId { get; set; }
        public int ProviderLocationId { get; set; }
        public int UserType { get; set; }
        public int ParentId { get; set; }
        public int IsActive { get; set; }
        public int ServiceableArea { get; set; }
        public int Type { get; set; }
        public int SubServiceableArea { get; set; }
        public int ServiceRequestId { get; set; }
        public int MessageCount { get; set; }
        public string RegistrationGroupName { get; set; }
        public string ServiceNumber { get; set; }
        public string RegistrationGroupId { get; set; }
        public string GRegistrationGroupId { get; set; }
        public string PRegistrationGroupId { get; set; }
        public string HRegistrationGroupId { get; set; }
        public string ARegistrationGroupId { get; set; }
        public string CompanyName { get; set; }
        public string ABN { get; set; }
        public string Surburb { get; set; }
        public string ISurburb { get; set; }
        public string ICity { get; set; }
        public string NDISReferenceNumber { get; set; }
        public string ECode { get; set; }
        public bool IsEmailVerified { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string SubName { get; set; }
        public string ParentName { get; set; }
        public string Title { get; set; }

        public string AddressLine1 { get; set; }
        public string City { get; set; }
        public string SubAddressLine1 { get; set; }
        
        public string AddressLine2 {get; set;}				
        
        public int LookupStateId { get; set; }
        public int ILookupStateId { get; set; }
        public long SubLookupStateId { get; set; }
        public int ActiveCount { get; set; }
        public int CompletedCount { get; set; }
        public int ProviderCount { get; set; }
        public string StateName { get; set; }

         public string PinCode { get; set; }
         public string IPinCode { get; set; }
         public string SubPinCode { get; set; }
        
         public string Phone { get; set; }		
         public string SubPhone { get; set; }		
         
         public string Email { get; set; }
         public string CompanyEmail { get; set; }
         public string SubEmail { get; set; }
        
         public string Password { get; set; }

        public string OldPassword { get; set; }

        public string NewPassword { get; set; }

        public string ConfirmPassword { get; set; }
        public string ProviderProfileAndServices { get; set; }
        
         public string Website { get; set; }
         public string SubWebsite { get; set; }
        public string OfficeType { get; set; }
         public double Rating { get; set; }
        public int Status { get; set; }
        public int MainStatus { get; set; }
        public int IsRead { get; set; }
        public int IsApprovedRead { get; set; }
        public int IsProviderRead { get; set; }
        public int IsResponse { get; set; }
        public int ServiceStatus { get; set; }
        public string Position { get; set; }
        public string PCName { get; set; }
        public string PCNumber { get; set; }
        public string PCEmail { get; set; }
        public int IsProviderContact { get; set; }
        public int IsHideContactDetails { get; set; }
        public int IsCapacity { get; set; }
        public  bool  paymentreceivedlast3months { get; set; }		
         
         public DateTime CreatedDate { get; set; }
         public DateTime ServiceRequestDate { get; set; }
        
         public long CreatedBy { get; set; }
        
         public bool IsLoggedIn { get; set; }
        
         public DateTime RegisterDatetime { get; set; }		

         public DateTime  LastlogIn { get; set; }
        
           
         public string Lat { get; set; }
        
         public string Long { get; set; }
        public string ContactName { get; set; }
        public string  LastInviteEmailSentDate { get; set; }
        public string FileName { get; set; }
        public string FileURL { get; set; }
        public string CompanyLogoURL { get; set; }
         public DateTime AcceptDate { get; set; }
         public DateTime CancelDate { get; set; }
        
         public  DateTime UpdatedDate { get; set; }	
         public  DateTime MessageDate { get; set; }	
         public  DateTime ReadDate { get; set; }	
        
         public  long  UpdatedBy { get; set; }

        public String CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd MMM yyyy") : "-";
        public String MessageDateStr => MessageDate != null ? MessageDate.ToString("dd MMM yyyy") : "-";
        public String ServiceRequestDateStr => ServiceRequestDate != null ? ServiceRequestDate.ToString("dd MMM yyyy") : "-";
        public String ReadDateStr => ReadDate != null ? ReadDate.ToString("dd MMM yyyy") : "-";

        public String UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd MMM yyyy") : "-";

        public String RegisterDateTimeStr => RegisterDatetime != null ? RegisterDatetime.ToString("dd-MM-yyyy hh:mm tt") : "-";

        public String LastLogingStr => LastlogIn != null ? LastlogIn.ToString("dd-MM-yyyy hh:mm tt") : "-";
        public String AcceptDateStr => AcceptDate != null ? AcceptDate.ToString("dd MMM yyyy") : "-";
        public String CancelDateStr => CancelDate != null ? CancelDate.ToString("dd MMM yyyy") : "-";
        public List<AbstractProvider> Provider { get; set; }

    }
}
