﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlannedServices.Entities.Contract
{
    public abstract class AbstractUser
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int ParentId { get; set; }
        public int UserType { get; set; }
        public int IsPlan { get; set; }
        public int ManagerType { get; set; }
        public int PlanManagerId { get; set; }
        public int IsUserMultiple { get; set; }
        public int Age { get; set; }
        public string stripeToken { get; set; }
        public string stripeEmail { get; set; }
        public string Title { get; set; }
        public string ParentName { get; set; }
        public DateTime TransactionDate { get; set; }
        public string TransactionEndDate { get; set; }
        public string TransactionId { get; set; }
        public string PlanText { get; set; }
        public string SubFirstName { get; set; }
        public string SubLastName { get; set; }
        public string SubEmail { get; set; }
        public string SubPassword { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string NomineeFirstName { get; set; }
        public string NomineeLastName { get; set; }
        public string NomineeTitle { get; set; }
        public string NomineeMobileNo { get; set; }
        public string NomineeEmail { get; set; }
        public string ABN { get; set; }
        public string Position { get; set; }
        public string StateName { get; set; }
        public string Email { get; set; }
        public string WebSite { get; set; }
        public string Password { get; set; }
        public string MobileNo { get; set; }
        public string PhoneNumber { get; set; }
        public string DOB { get; set; }
        public string Gender { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Surburb { get; set; }
        public string City { get; set; }
        public string BCity { get; set; }
        public int LookupCountryId { get; set; }
        public int LookupStateId { get; set; }
        public int BLookupStateId { get; set; }
        public int LookupCityId { get; set; }
        public string PinCode { get; set; }
        public string BPinCode { get; set; }
        public string UserProfileImage { get; set; }
        public string CardNumber { get; set; }
        public string EmailForInvoice { get; set; }
        public string NameOnCard { get; set; }
        public string ExpiryDate { get; set; }
        public string CVVCode { get; set; }
        public string ECode { get; set; }
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
        public string PlanManagerAddress { get; set; }
        public string PlanManagerName { get; set; }
        public string CompanyLogoURL { get; set; }
        public string PlanManagerEmail { get; set; }
        public double lng { get; set; }
        public double lat { get; set; }
        public bool IsEmailVerified { get; set; }
        public bool IsResetPassword { get; set; }
        public bool IsInvitationAccepted { get; set; }
        public bool IsActive { get; set; }
        public string NDISReferenceNumber { get; set; }
        public string CompanyName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string ReferenceNo { get; set; }



        public DateTime LastLogin { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public int DeletedBy { get; set; }
        public List<AbstractUser> Users{ get; set; }
        public int ManagerUserCount { get; set; }
        public int ServiceRequestCount { get; set; }
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("MM/dd/yyyy") : "-";
        public string TransactionDateStr => TransactionDate != null ? TransactionDate.ToString("dd MMM yyyy") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("MM/dd/yyyy") : "-";
    }
}
