﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlannedServices.Common;
using PlannedServices.Common.Paging;
using PlannedServices.Entities.Contract;

namespace PlannedServices.Data.Contract
{
  public abstract class AbstractProviderDao
    {
        public abstract SuccessResult<AbstractProvider> Provider_Upsert(AbstractProvider abstractProvider);
        public abstract SuccessResult<AbstractProvider> ChangeProviderLocation(AbstractProvider abstractProvider);
        public abstract SuccessResult<AbstractProvider> SubProvider_Upsert(AbstractProvider abstractProvider);
        public abstract SuccessResult<AbstractProvider> ProviderService_Upsert(string Ids, int PId, int Type);
        public abstract SuccessResult<AbstractProvider> ProviderServiceArea_Upsert(int Type, string value, int PId);
        public abstract SuccessResult<AbstractProvider> Provider_ById(long Id, long UserId = 0);
        public abstract PagedList<AbstractProvider> GetProviderByCreatedDate(string SDate, string EDate);
        public abstract PagedList<AbstractProvider> Provider_All(PageParam pageParam, string search, long LookupStateId, string Name, string PostCode, string State);
        public abstract PagedList<AbstractProvider> Provider_AllByParentId(PageParam pageParam, string search, long LookupStateId, long ParentId, string Name, string PostCode, string State);
        public abstract PagedList<AbstractProvider> Provider_AllByServiceRequestId(PageParam pageParam, string search, int ServiceRequestId);
        public abstract PagedList<AbstractProvider> ClosedServiceRequestProviderAll(PageParam pageParam, string search, int ServiceRequestId);
        public abstract PagedList<AbstractProvider> CheckSubProvider(PageParam pageParam, int Id);
        public abstract SuccessResult<AbstractProvider> Provider_ActInactive(long Id);
        public abstract SuccessResult<AbstractProvider> Provider_ChangePassword(AbstractProvider abstractProvider);
        public abstract SuccessResult<AbstractProvider> Provider_RestPassword(AbstractProvider abstractProvider);
        public abstract SuccessResult<AbstractProvider> Provider_Login(AbstractProvider abstractProvider);
        public abstract bool Provider_Logout(long id);
        public abstract bool DeleteProvider(int id, int DeletedBy);
        public abstract bool DeleteProviderLocation(int LId, int ProviderId);
        public abstract PagedList<AbstractProvider> ChildProvider_AllByParentId(PageParam pageParam, string search, int ParentId);
        public abstract SuccessResult<AbstractProvider> AddLocation(AbstractProvider abstractProvider);
        public abstract SuccessResult<AbstractProvider> ProviderLocationPostCodeMapping_Upsert(long Id);
        public abstract PagedList<AbstractProvider> ProviderLocation_All(long PId);
        public abstract SuccessResult<AbstractProvider> ProviderLocation_ById(long Id);
        public abstract SuccessResult<AbstractProvider> Provider_Active(string Email);
        public abstract SuccessResult<AbstractProvider> Provider_EmailVerified(string Email);
        public abstract bool Provider_IsEmailExist(string Email);
        public abstract bool GetProvider_ByEmail(string Email);
        public abstract SuccessResult<AbstractProvider> Provider_ByEmail(string Email, int Type = 0);
    }
}
