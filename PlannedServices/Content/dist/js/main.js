/*
* NDIS
* @version: 1.0.0
* @author: Uichamp
* @license: Uichamp (https://uichamp.com/licenses)
* Copyright 2021 Uichamp
*/
(function () {
  'use strict';

  var tooltip = function () {
    $('[data-toggle="tooltip"]').tooltip();
  }();

  var popover = function () {
    $('[data-toggle="popover"]').popover();
  }();

  var selectPicker = function () {
    $('.selectpicker').selectpicker();
  }(); // import dateRangePicker from './components/daterangepicker';
  // import timePicker from './components/timepicker';

})();