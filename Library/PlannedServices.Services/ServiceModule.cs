﻿//-----------------------------------------------------------------------
// <copyright file="ServiceModule.cs" company="Premiere Digital Services">
//     Copyright Premiere Digital Services. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace PlannedServices.Services
{
    using Autofac;
    using Data;
    using PlannedServices.Services.Contract;

    //using PlannedServices.Services.Contract;




    /// <summary>
    /// The Service module for dependency injection.
    /// </summary>
    public class ServiceModule : Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be
        /// registered.</param>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule<DataModule>();
            //builder.RegisterType<V1.LookUpServicesServices>().As<AbstractLookUpServicesServices>().InstancePerDependency();
            builder.RegisterType<V1.UserServices>().As<AbstractUserServices>().InstancePerDependency();
            builder.RegisterType<V1.ProviderServices>().As<AbstractProviderServices>().InstancePerDependency();
            builder.RegisterType<V1.LookupStateServices>().As<AbstractLookupStateServices>().InstancePerDependency();
            builder.RegisterType<V1.ServicesServices>().As<AbstractServicesServices>().InstancePerDependency();
            builder.RegisterType<V1.ServicesRequestServices>().As<AbstractServicesRequestServices>().InstancePerDependency();
            builder.RegisterType<V1.ResourcesServices>().As<AbstractResourcesServices>().InstancePerDependency();



            base.Load(builder);
        }
    }
}
