﻿using PlannedServices.Common;
using PlannedServices.Entities.Contract;
using PlannedServices.Entities.V1;
using PlannedServices.Services.Contract;
using PlannedServicesAdmin.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static PlannedServicesAdmin.Infrastructure.Enums;
using PlannedServicesAdmin.Infrastructure;
using PlannedServices.Common.Paging;
using SendGrid;
using System.Configuration;
using SendGrid.Helpers.Mail;
using Newtonsoft.Json;
using System.Net.Http;

namespace PlannedServicesAdmin.Controllers
{
    public class UserController : BaseController
    {
        #region Fields
        private readonly AbstractUserServices abstractUserServices;
        private readonly AbstractLookupStateServices abstractLookupStateServices;
        private readonly AbstractServicesServices abstractServices;
        #endregion

        #region Ctor
        public UserController(AbstractUserServices abstractUserServices,
            AbstractLookupStateServices abstractLookupStateServices, AbstractServicesServices abstractServices)
        {
            this.abstractUserServices = abstractUserServices;
            this.abstractLookupStateServices = abstractLookupStateServices;
            this.abstractServices = abstractServices;
        }
        #endregion


        // GET: User
        

        public ActionResult PSettings()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            return View();
        }
        public ActionResult ManagerPSettings()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            return View();
        }
        public ActionResult PDSettings()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];

            AbstractUser abstractUser = new User();
            ViewBag.userId = 0;
            List<SelectListItem> items = new List<SelectListItem>();
            //ViewBag.States = BindStateDropdown();
            ViewBag.States = items;
            ViewBag.Citys = items;
            int decryptedId = ProjectSession.UserID;
                if (decryptedId > 0)
                {
                    PageParam pageParam = new PageParam();

                    abstractUser = abstractUserServices.User_ById(decryptedId).Item;
                    if (ProjectSession.LoginUserID != ProjectSession.UserID && ProjectSession.ManagaerUserId ==0)
                    {
                        abstractUser.Users = abstractUserServices.ChildUsers_AllByParentId(pageParam, "", decryptedId).Values;
                    }
                    else
                    {
                        abstractUser.Users = abstractUserServices.ParentUsers_All(pageParam, "", decryptedId).Values;
                    ViewBag.States = BindStateCityDropdown(0,ConvertTo.Integer(abstractUser.PinCode), 0);
                    ViewBag.Citys = BindStateCityDropdown(1,ConvertTo.Integer(abstractUser.PinCode), abstractUser.LookupStateId);

                }
                ViewBag.userId = decryptedId;
                }
            return View(abstractUser);
        }
        public ActionResult ManagerPDSettings()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];

            AbstractUser abstractUser = new User();
            ViewBag.userId = 0;
            List<SelectListItem> items = new List<SelectListItem>();
            //ViewBag.States = BindStateDropdown();
            ViewBag.States = items;
            ViewBag.Citys = items;
            int decryptedId = ProjectSession.LoginUserID;
                if (decryptedId > 0)
                {
                    PageParam pageParam = new PageParam();

                    abstractUser = abstractUserServices.User_ById(decryptedId).Item;
                    
                    ViewBag.userId = decryptedId;
                }
            return View(abstractUser);
        }

        public ActionResult USettings()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            PageParam pageParam = new PageParam();
            AbstractUser abstractUser = new User();
            var model = abstractUserServices.ChildUsers_AllByParentId(pageParam, "", ProjectSession.UserID).Values;
            if (model.Count > 0)
            {
                abstractUser.Users = model;
            }
            return View(abstractUser);
        }

        public ActionResult UserInvite()
        {
            
            return View();
        }
        public ActionResult PMDSettings()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            AbstractUser abstractPlanManager = null;
            ViewBag.States = BindStateDropdown();
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000000;
            List<AbstractPlanManager> abstractPlans = new List<AbstractPlanManager>();
            ViewBag.PlanManagerList = abstractUserServices.PlanManager_All(pageParam, "").Values;
            abstractPlans = ViewBag.PlanManagerList;

            if (ProjectSession.UserID > 0)
            {
                int decryptedId = ProjectSession.UserID;
                if (decryptedId > 0)
                {
                    abstractPlanManager = abstractUserServices.User_ById(decryptedId).Item;
                    ViewBag.SelectedPlanManager = abstractPlans.FindAll(x => x.Id == abstractPlanManager.PlanManagerId);
                }
            }
            return View(abstractPlanManager);
        }
        public ActionResult PaymentDetails()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            AbstractUser abstractUser = null;
            ViewBag.States = BindStateDropdown();
            if (ProjectSession.SetupPaymentID > 0)
            {
                int decryptedId = ProjectSession.UserID;
                if (decryptedId > 0)
                {
                    abstractUser = abstractUserServices.UsersPaymentDetails_ById(decryptedId).Item;
                }
            }
            return View(abstractUser);
        }
        public ActionResult CPSettings()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            return View();
        }
       


        public ActionResult UserDetails(string userId = "")
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];

            AbstractUser abstractUser = null;
            ViewBag.userId = 0;
            ViewBag.States = BindStateDropdown();
            if (userId != null)
            {
                int decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(userId));
                if (decryptedId > 0)
                {
                    PageParam pageParam = new PageParam();

                    abstractUser = abstractUserServices.User_ById(decryptedId).Item;
                    //if (ProjectSession.LoginUserID != ProjectSession.UserID)
                    //{
                    //    abstractUser.Users = abstractUserServices.ChildUsers_AllByParentId(pageParam, "", decryptedId).Values;
                    //}
                    //else
                    //{
                    //    abstractUser.Users = abstractUserServices.ParentUsers_All(pageParam, "", decryptedId).Values;
                    //}
                    ViewBag.userId = decryptedId;
                }
            }
            return View(abstractUser);
        }

        public ActionResult Index()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            return View();
        }
        public ActionResult UserProfile(string userId = "")
        {
            AbstractUser abstractUser = null;
            ViewBag.userId = 0;
            if (userId != null)
            {
                int decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(userId));
                if (decryptedId > 0)
                {
                    abstractUser = abstractUserServices.User_ById(decryptedId).Item;
                    //abstractProvider.Phone = abstractProvider.Phone.Remove(0, 4);
                    //if (abstractProvider.ImageUrl != null)
                    //{
                    //    abstractProvider.ImageUrl = Configurations.S3BaseUrl + users.ImageUrl;
                    //}
                    ViewBag.userId = decryptedId;
                }
            }
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            return View(abstractUser);
        }
        public ActionResult EditUserProfile(string userId = "")
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];

            AbstractUser abstractUser = null;
            ViewBag.userId = 0;
            ViewBag.States = BindStateDropdown();
            if (userId != null)
            {
                int decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(userId));
                if (decryptedId > 0)
                {
                    PageParam pageParam = new PageParam();

                    abstractUser = abstractUserServices.User_ById(decryptedId).Item;
                    if (ProjectSession.LoginUserID != ProjectSession.UserID)
                    {
                        abstractUser.Users = abstractUserServices.ChildUsers_AllByParentId(pageParam, "", decryptedId).Values;
                    }
                    else
                    {
                        abstractUser.Users = abstractUserServices.ParentUsers_All(pageParam, "", decryptedId).Values;
                    }
                    //abstractProvider.Phone = abstractProvider.Phone.Remove(0, 4);
                    //if (abstractProvider.ImageUrl != null)
                    //{
                    //    abstractProvider.ImageUrl = Configurations.S3BaseUrl + users.ImageUrl;
                    //}
                    ViewBag.userId = decryptedId;
                }
            }
            return View(abstractUser);
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.AddUsers)]
        public ActionResult AddUsers(User user)
        {
            int Id = 0;
            try
            {
              
                user.CreatedBy = ProjectSession.UserID;
                //HttpClient client = new HttpClient();

                //var stringTask = client.GetStringAsync("https://maps.googleapis.com/maps/api/geocode/json?address=" + Convert.ToInt32(user.PinCode) + ",Aus&key=AIzaSyB97EhOhMCYhK1tZIofUkpKcGW61fdgKjg");
                //var msg = stringTask;

                //var s = JsonConvert.DeserializeObject<Root>(msg.Result);
                //if (s.results.Count > 0)
                //{
                //    user.lat = s.results[0].geometry.location.lat;
                //    user.lng = s.results[0].geometry.location.lng;
                //}
                SuccessResult<AbstractUser> result = new SuccessResult<AbstractUser>();
                if (ProjectSession.SetupUserType == "3")
                {
                     result = abstractUserServices.ManagerUsers_Upsert(user);
                }
                else
                {
                     result = abstractUserServices.User_Upsert(user);
                }
                if (result != null && result.Code == 200 && result.Item != null)
                {
                    Id = result.Item.Id;
                    TempData["displayModal"] = "myModal";
                    //TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), result.Message);
                    return RedirectToAction("PDSettings", Pages.Controllers.User, new { Area = "" });
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "");
            }
            return RedirectToAction("PDSettings", Pages.Controllers.User, new { Area = "" });
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.AddUpdateManagerUsers)]
        public ActionResult AddUpdateManagerUsers(User user)
        {
            int Id = 0;
            try
            {
                user.CreatedBy = ProjectSession.UserID;
                //HttpClient client = new HttpClient();

                //var stringTask = client.GetStringAsync("https://maps.googleapis.com/maps/api/geocode/json?address=" + Convert.ToInt32(user.PinCode) + ",Aus&key=AIzaSyB97EhOhMCYhK1tZIofUkpKcGW61fdgKjg");
                //var msg = stringTask;

                //var s = JsonConvert.DeserializeObject<Root>(msg.Result);
                //if (s.results.Count > 0)
                //{
                //    user.lat = s.results[0].geometry.location.lat;
                //    user.lng = s.results[0].geometry.location.lng;
                //}
                SuccessResult<AbstractUser> result = new SuccessResult<AbstractUser>();
               
                result = abstractUserServices.User_Upsert(user);
                
                if (result != null && result.Code == 200 && result.Item != null)
                {
                    Id = result.Item.Id;
                    TempData["displayModal"] = "myModal";
                    //TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), result.Message);
                    return RedirectToAction("PDSettings", Pages.Controllers.User, new { Area = "" });
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "");
            }
            return RedirectToAction("PDSettings", Pages.Controllers.User, new { Area = "" });
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.AddSubUsers)]
        public ActionResult AddSubUsers(User user)
        {
            int Id = 0;
            try
            {
                user.ParentId = ProjectSession.UserID;
                SuccessResult<AbstractUser> result = abstractUserServices.SubUser_Upsert(user);
                if (result != null && result.Code == 200 && result.Item != null)
                {
                    Id = result.Item.Id;
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), result.Message);
                    return RedirectToAction("USettings", Pages.Controllers.User, new { Area = "" });
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "");
            }
            //return RedirectToAction("EditUserProfile", Pages.Controllers.User, new { Area = "", userId = ConvertTo.Base64Encode(Convert.ToString(ProjectSession.UserID)) });
            return RedirectToAction("USettings", Pages.Controllers.User, new { Area = "" });
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.PersonalDetails)]
        public ActionResult PersonalDetails(PlanManager planManager)
        {
            int Id = 0;
            try
            {
                planManager.CreatedBy = ProjectSession.UserID;
                SuccessResult<AbstractPlanManager> result = abstractUserServices.PlanManager_Upsert(planManager);
                if (result != null && result.Code == 200 && result.Item != null)
                {
                    Id = result.Item.Id;
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), result.Message);
                    return RedirectToAction(Actions.PSettings, Pages.Controllers.User, new { Area = "" });
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "");
            }
            return RedirectToAction(Actions.PSettings, Pages.Controllers.User, new { Area = "" });
        }

         [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.PlanManagerDetails)]
        public ActionResult PlanManagerDetails(PlanManager planManager)
        {
            int Id = 0;
            try
            {
                planManager.CreatedBy = ProjectSession.UserID;
                planManager.Password = "1234567890";
                planManager.Type = 0;
                SuccessResult<AbstractPlanManager> result = abstractUserServices.PlanManager_Upsert(planManager);
                if (result != null && result.Code == 200 && result.Item != null)
                {
                    Id = result.Item.Id;
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), result.Message);
                    return RedirectToAction(Actions.PSettings, Pages.Controllers.User, new { Area = "" });
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "");
            }
            return RedirectToAction(Actions.PSettings, Pages.Controllers.User, new { Area = "" });
        }
        
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.ChangePassword)]
        public ActionResult ChangePassword(User user)
        {
            int Id = 0;
            try
            {
                user.Id = ProjectSession.LoginUserID;
                SuccessResult<AbstractUser> result = abstractUserServices.User_ChangePassword(user);
                if (result != null && result.Code == 200 && result.Item != null)
                {
                    Id = result.Item.Id;
                    TempData["displayModal"] = "myModal";
                    TempData["displayModalMSG"] = "";
                    //TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), result.Message);
                    return RedirectToAction(Actions.CPSettings, Pages.Controllers.User, new { Area = "" });
                }
                else
                {
                    TempData["displayModal"] = "myModal";
                    TempData["displayModalMSG"] = result.Message;
                    //TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "");
            }
            return RedirectToAction(Actions.CPSettings, Pages.Controllers.User, new { Area = "" });
        }
          
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.PaymentDetails)]
        public ActionResult PaymentDetails(User user)
        {
            int Id = 0;
            try
            {
                user.UserId = ProjectSession.UserID;
                SuccessResult<AbstractUser> result = abstractUserServices.UsersPaymentDetails_Upsert(user);
                if (result != null && result.Code == 200 && result.Item != null)
                {
                    Id = result.Item.Id;
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), result.Message);
                    return RedirectToAction(Actions.PSettings, Pages.Controllers.User, new { Area = "" });
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "");
            }
            return RedirectToAction(Actions.PSettings, Pages.Controllers.User, new { Area = "" });
        }


        [HttpGet]
        [ActionName(Actions.DeleteUser)]
        public ActionResult DeleteUser(int UId)
        {
            var result = abstractUserServices.DeleteUser(UId, ProjectSession.UserID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ActionName(Actions.UpdatePlanManager)]
        public JsonResult UpdatePlanManager(int PlanManagerId)
        {
            SuccessResult<AbstractUser> result = abstractUserServices.UpdatePlanManager(ProjectSession.UserID, PlanManagerId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ActionName(Actions.SendUserInvite)]
        public JsonResult SendInvite(string Email)
        {
            SuccessResult<AbstractUser> result = new SuccessResult<AbstractUser>();
            var modal = abstractUserServices.GetUser_ByEmail(Email);
            if (!modal)
            {
                //TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), "Your request has been sent to 32 Providers");
                var sendGridClient = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"]);

                var sendGridMessage = new SendGridMessage();
                sendGridMessage.SetFrom(ConfigurationManager.AppSettings["SendGridFromEmail"], "My Request");
                sendGridMessage.AddTo(Email, "user test"); // user email
                //sendGridMessage.AddTo("anthony@plannedservices.com.au", "user test"); // user email
                sendGridMessage.SetTemplateId("d-17b0c1b2395443c39b22d38ca7d112e4");

                sendGridMessage.SetTemplateData(new EmailObj
                {
                  //  UName = result.Item.FirstName + " " + result.Item.LastName,
                    email = ConvertTo.Base64Encode(Convert.ToString(Email)),
                    //PName = result.Item.ParentName

                });
                var response = sendGridClient.SendEmailAsync(sendGridMessage).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
                {
                    result.Code = 200;
                }
                else
                {
                    result.Code = 400;
                    result.Message = "something went wrong please try again.!";
                }
            }
            else
            {
                result.Code = 400;
                result.Message = "Email already exists";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ActionName(Actions.SendInvite)]
        public JsonResult SendInvite(int Id,string Email)
        {
            var result = abstractUserServices.User_ById(Id);
            if (result != null && result.Code == 200 && result.Item != null)
            {
                //TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), "Your request has been sent to 32 Providers");
                var sendGridClient = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"]);

                var sendGridMessage = new SendGridMessage();
                sendGridMessage.SetFrom(ConfigurationManager.AppSettings["SendGridFromEmail"], "My Request");
                sendGridMessage.AddTo(Email, "user test"); // user email
                //sendGridMessage.AddTo("anthony@plannedservices.com.au", "user test"); // user email
                sendGridMessage.SetTemplateId("d-17b0c1b2395443c39b22d38ca7d112e4");

                sendGridMessage.SetTemplateData(new EmailObj
                {
                    UName = result.Item.FirstName + " " + result.Item.LastName,
                    email = ConvertTo.Base64Encode(Convert.ToString(result.Item.Email)),
                    PName = result.Item.ParentName

                });

                var response = sendGridClient.SendEmailAsync(sendGridMessage).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
                {

                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult VerifyEmail(string Email)
        {
            var modal = abstractUserServices.GetUser_ByEmail(Email);
            SuccessResult<AbstractUser> result = new SuccessResult<AbstractUser>();
            var userdata = abstractUserServices.User_ById(ProjectSession.UserID);
            var isVerify = false;
            if (userdata.Item != null)
            {
                isVerify = userdata.Item.Email == Email ? true : false;
            }
            if (!modal && !isVerify)
            {
                var sendGridClient = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"]);

                var sendGridMessage = new SendGridMessage();
                sendGridMessage.SetFrom(ConfigurationManager.AppSettings["SendGridFromEmail"], "My Request");
                sendGridMessage.AddTo(Email, "user test"); // user email
                //sendGridMessage.AddTo("parthdobariya09@gmail.com", "user test"); // user email
                sendGridMessage.SetTemplateId("d-0bddf5d12367494f8dd47c39f83541c6");

                Random generator = new Random();
                String r = generator.Next(0, 1000000).ToString("D6");

                sendGridMessage.SetTemplateData(new EmailObj
                {
                    UName = ProjectSession.SetupUserName,
                    email = ConvertTo.Base64Encode(Convert.ToString(Email)),
                    Code = r
                });

                var response = sendGridClient.SendEmailAsync(sendGridMessage).Result;

                User user = new User();
                result.Item = user;
                result.Item.ECode = r;
                if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
                {
                    result.Code = 200;
                }
                else
                {
                    result.Code = 400;
                }
                return Json(result, JsonRequestBehavior.AllowGet);

            }
            else
            {
                result.Code = isVerify ? 401 : 403;
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ActionName(Actions.Search)]
        public JsonResult Search(string Name)
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000000;
            var result = abstractUserServices.PlanManager_All(pageParam, Name).Values;
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        [ActionName(Actions.GetState)]
        public ActionResult GetState(int Type, int PostCode, int StateId = 0)
        {
            List<ServicesRequest> items = new List<ServicesRequest>();


            if (Type == 0)
            {
                var model = abstractServices.State_AllByPostCode(PostCode);
                foreach (var item in model.Values)
                {
                    items.Add(new ServicesRequest() { Name = item.Name.ToString(), DrpId = item.Id.ToString() });
                }
            }
            else
            {
                var model = abstractServices.City_AllByPostCode(PostCode, StateId);
                foreach (var item in model.Values)
                {
                    items.Add(new ServicesRequest() { Name = item.Name.ToString(), DrpId = item.Name.ToString() });
                }
            }

            return Json(items, JsonRequestBehavior.AllowGet);
        }
        public IList<SelectListItem> BindStateDropdown()
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 100000;
            var model = abstractLookupStateServices.LookupState_All(pageParam, "").Values;

            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in model)
            {
                items.Add(new SelectListItem() { Text = item.Name.ToString(), Value = item.Id.ToString() });
            }

            return items;
        }
        public IList<SelectListItem> BindStateCityDropdown(int Type, int PostCode, int StateId = 0)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            if (Type == 0)
            {
                var model = abstractServices.State_AllByPostCode(PostCode);
                foreach (var item in model.Values)
                {
                    items.Add(new SelectListItem() { Text = item.Name.ToString(), Value = item.Id.ToString() });
                }
            }
            else
            {
                var model = abstractServices.City_AllByPostCode(PostCode, StateId);
                foreach (var item in model.Values)
                {
                    items.Add(new SelectListItem() { Text = item.Name.ToString(), Value = item.Name.ToString() });
                }
            }

            return items;
        }
        private class EmailObj
        {
            [JsonProperty("username")]
            public string username { get; set; }

            [JsonProperty("email")]
            public string email { get; set; }

            [JsonProperty("UName")]
            public string UName { get; set; }

            [JsonProperty("PName")]
            public string PName { get; set; }

            [JsonProperty("password")]
            public string password { get; set; }
            [JsonProperty("Code")]
            public string Code { get; set; }
        }


    }
}