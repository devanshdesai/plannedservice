﻿using System.Collections.Generic;

namespace PlannedServicesProviderPortal.Pages
{
    public class Controllers
    {
        public const string Authentication = "Authentication";
        public const string AccountSetup = "AccountSetup";
        public const string Account = "Account";
        public const string Dashboard = "Dashboard";
        public const string Provider = "Provider";
        public const string Home = "Home";
        public const string Users = "Users";
        public const string User = "User";
        public const string SalonOwner = "SalonOwner";
        public const string SalonsData = "SalonsData";
        public const string ChangePassword = "ChangePassword";
        public const string MasterData = "MasterData";
        public const string ServiceRequest = "ServiceRequest";
        public const string ServiceRequests = "ServiceRequests";
        public const string Resources = "Resources";
    }
}