﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlannedServices.Common;
using PlannedServices.Common.Paging;
using PlannedServices.Data.Contract;
using PlannedServices.Entities.Contract;
using PlannedServices.Services.Contract;

namespace PlannedServices.Services.V1
{
    public class ServicesServices : AbstractServicesServices
    {
        private AbstractServicesDao abstractServicesDao;

        public ServicesServices(AbstractServicesDao abstractServicesDao)
        {
            this.abstractServicesDao = abstractServicesDao;
        }

        public override SuccessResult<AbstractServices> Services_Upsert(AbstractServices abstractServices)
        {
            return this.abstractServicesDao.Services_Upsert(abstractServices);
        }
        public override SuccessResult<AbstractServices> ServiceRequest_Upsert(AbstractServices abstractServices)
        {
            return this.abstractServicesDao.ServiceRequest_Upsert(abstractServices);
        }
        public override SuccessResult<AbstractServices> ServiceRequestChild_Upsert(AbstractServices abstractServices)
        {
            return this.abstractServicesDao.ServiceRequestChild_Upsert(abstractServices);
        }
        public override SuccessResult<AbstractServices> Services_ById(long Id)
        {
            return this.abstractServicesDao.Services_ById(Id);
        }
        public override SuccessResult<AbstractServices> GetUserInfo_ByServiceRequestId(long Id)
        {
            return this.abstractServicesDao.GetUserInfo_ByServiceRequestId(Id);
        }

        public override PagedList<AbstractServices> Services_All(PageParam pageParam, string search)
        {
            return this.abstractServicesDao.Services_All(pageParam, search);
        }
        public override PagedList<AbstractServices> GetServiceRequestProviderCount(int RId, int UId)
        {
            return this.abstractServicesDao.GetServiceRequestProviderCount(RId, UId);
        }
        public override PagedList<AbstractServices> GetTrialSendRequestProviderCount(int RId, int UId)
        {
            return this.abstractServicesDao.GetTrialSendRequestProviderCount(RId, UId);
        }

        public override PagedList<AbstractServices> SupportCategoryMasterByRegistrationGroupId(PageParam pageParam, string search, string GName, string BudgetName)
        {
            return this.abstractServicesDao.SupportCategoryMasterByRegistrationGroupId(pageParam, search, GName,BudgetName);
        }
        public override PagedList<AbstractServices> RegistrationGroupMasterByCategoryId(PageParam pageParam, string search, string GName, string BudgetName, int CategoryId)
        {
            return this.abstractServicesDao.RegistrationGroupMasterByCategoryId(pageParam, search, GName,BudgetName,CategoryId);
        }

        public override PagedList<AbstractServices> RegistrationGroupMaster_All(PageParam pageParam, string search,  string GName, string Type = "")
        {
            return this.abstractServicesDao.RegistrationGroupMaster_All(pageParam, search,GName,Type);
        }
        public override PagedList<AbstractServices> RegistrationGroupMaster_ByProviderId(int ProviderId)
        {
            return this.abstractServicesDao.RegistrationGroupMaster_ByProviderId(ProviderId);
        }
        public override PagedList<AbstractServices> SupportGoal_AllByBudget(PageParam pageParam, string search, string BudgetName)
        {
            return this.abstractServicesDao.SupportGoal_AllByBudget(pageParam, search, BudgetName);
        }
        public override PagedList<AbstractServices> Reason_All(PageParam pageParam, string search)
        {
            return this.abstractServicesDao.Reason_All(pageParam, search);
        }
        public override PagedList<AbstractServices> BudgetMaster_All(PageParam pageParam, string search)
        {
            return this.abstractServicesDao.BudgetMaster_All(pageParam, search);
        }
        public override PagedList<AbstractServices> State_AllByPostCode(int PostCode)
        {
            return this.abstractServicesDao.State_AllByPostCode(PostCode);
        }
        public override PagedList<AbstractServices> City_AllByPostCode(int PostCode, int StateId)
        {
            return this.abstractServicesDao.City_AllByPostCode(PostCode, StateId);
        }

        public override PagedList<AbstractServices> SupportItemMasterByCategoryName(PageParam pageParam, string search, int CategoryId, int RegistrationGroupId, string GName, string BudgetName)
        {
            return this.abstractServicesDao.SupportItemMasterByCategoryName(pageParam, search, CategoryId, RegistrationGroupId,GName,BudgetName);
        }
        public override PagedList<AbstractServices> ServiceRequestChild_ByServiceRequestId(int ServiceRequestId)
        {
            return this.abstractServicesDao.ServiceRequestChild_ByServiceRequestId(ServiceRequestId);
        }

        public override SuccessResult<AbstractServices> ChangeServiceStatus(int ServiceRequestId, int ProviderId, int Type,int IsApproved, string ReasonId)
        {
            return this.abstractServicesDao.ChangeServiceStatus(ServiceRequestId, ProviderId,Type, IsApproved,ReasonId);
        } 
        public override SuccessResult<AbstractServices> UpdateIsHideContactDetails(int ServiceRequestId, int IsHideContactDetails)
        {
            return this.abstractServicesDao.UpdateIsHideContactDetails(ServiceRequestId, IsHideContactDetails);
        } 
        public override SuccessResult<AbstractServices> UpdateProviderIsCapacity(int ProviderId, int ServiceRequestId, int IsCapacity)
        {
            return this.abstractServicesDao.UpdateProviderIsCapacity(ProviderId,ServiceRequestId, IsCapacity);
        } 
        
        public override bool DeleteServiceRequest(int id)
        {
            return this.abstractServicesDao.DeleteServiceRequest(id);
        }
        
        public override bool DeleteServiceRequestProvider(int id,int pid)
        {
            return this.abstractServicesDao.DeleteServiceRequestProvider(id,pid);
        }
    }
}
