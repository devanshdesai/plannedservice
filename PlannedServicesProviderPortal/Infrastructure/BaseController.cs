﻿using PlannedServices.Common;
using PlannedServicesProviderPortal.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;
using System.Threading;
using System.Data.SqlClient;
using System.Data;
//using PlannedServices.Entities.Contract;
//using PlannedServices.Services.Contract;
//using PlannedServices.Common.Paging;
//using PlannedServices.Entities.V1;

namespace PlannedServicesProviderPortal.Infrastructure
{

    public class BaseController : Controller
    {
      
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
        }


        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                if (ProjectSession.ProviderId == null || ProjectSession.ProviderId == 0)
                {
                    filterContext.Result = new RedirectResult("~/Account/LogIn");
                    return;
                }
                base.OnActionExecuting(filterContext);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                throw ex;
            }
        }

    }
}