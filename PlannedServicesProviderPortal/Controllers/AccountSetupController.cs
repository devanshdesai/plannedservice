﻿using Newtonsoft.Json;
using PlannedServices.Common;
using PlannedServices.Common.Paging;
using PlannedServices.Entities.Contract;
using PlannedServices.Entities.V1;
using PlannedServices.Services.Contract;
using PlannedServicesProviderPortal.Controllers;
using PlannedServicesProviderPortal.Pages;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using static PlannedServicesProviderPortal.Infrastructure.Enums;

namespace PlannedServicesProviderPortal.Controllers
{
    public class AccountSetupController : Controller
    {
        #region Fields
        private readonly AbstractProviderServices abstractProviderServices;
        private readonly AbstractServicesServices abstractServices;
        private readonly AbstractUserServices abstractUserServices;
        private readonly AbstractLookupStateServices abstractLookupStateServices;
        #endregion

        #region Ctor
        public AccountSetupController(AbstractUserServices abstractUserServices,
            AbstractLookupStateServices abstractLookupStateServices,
            AbstractProviderServices abstractProviderServices, AbstractServicesServices abstractServices)
        {
            this.abstractProviderServices = abstractProviderServices;
            this.abstractUserServices = abstractUserServices;
            this.abstractLookupStateServices = abstractLookupStateServices;
            this.abstractServices = abstractServices;
        }
        #endregion

        // GET: AccountSetup
        public ActionResult Index()
        {
            ProjectSession.ProviderId = ProjectSession.SetupUserID;
            ProjectSession.ProviderName = ProjectSession.SetupUserName;

            AbstractProvider abstractProvider = null;
            if (ProjectSession.SetupUserID > 0)
            {
                int decryptedId = ProjectSession.SetupUserID;
                if (decryptedId > 0)
                {
                    abstractProvider = abstractProviderServices.Provider_ById(decryptedId).Item;
                    ViewBag.providerId = decryptedId;
                }
            }
            return View(abstractProvider);

            //return View();
        }
        public ActionResult ProviderInviteServices()
        {
            //if (TempData["openPopup"] != null)
            //    ViewBag.openPopup = TempData["openPopup"];
            PagedList<AbstractServices> Services = new PagedList<AbstractServices>();
            int decryptedId = ProjectSession.SetupUserID;
            if (decryptedId > 0)
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 100000;
                ViewBag.ProviderSData = this.abstractServices.RegistrationGroupMaster_ByProviderId(ProjectSession.ParentId);

                ViewBag.ProviderData = abstractProviderServices.Provider_ById(decryptedId).Item;
                if (ViewBag.ProviderData.RegistrationGroupId != null)
                {
                    ViewBag.RegaArray = ViewBag.ProviderData.RegistrationGroupId.Split(',');
                }
                else
                {
                    ViewBag.RegaArray = "";
                }
            }
            return View(Services);
        }
        public ActionResult ProviderServices()
        {
            //if (TempData["openPopup"] != null)
            //    ViewBag.openPopup = TempData["openPopup"];
            PagedList<AbstractServices> Services = new PagedList<AbstractServices>();
            int decryptedId = ProjectSession.SetupUserID;
            if (decryptedId > 0)
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 100000;
                ViewBag.ProviderSData = this.abstractServices.RegistrationGroupMaster_All(pageParam, "", "", "General Registration Groups");
                ViewBag.ProviderPData = this.abstractServices.RegistrationGroupMaster_All(pageParam, "", "", "Professional Registration Groups");
                ViewBag.ProviderHData = this.abstractServices.RegistrationGroupMaster_All(pageParam, "", "", "Home and Vehicle Modification registration Groups");
                ViewBag.ProviderAData = this.abstractServices.RegistrationGroupMaster_All(pageParam, "", "", "Assistive Technology and Equipment Registration Groups");

                ViewBag.ProviderData = abstractProviderServices.Provider_ById(decryptedId).Item;
                if (ViewBag.ProviderData.RegistrationGroupId != null)
                {
                    ViewBag.RegaArray = ViewBag.ProviderData.RegistrationGroupId.Split(',');
                }
                else
                {
                    ViewBag.RegaArray = "";
                }
            }
            return View(Services);
        }
        public ActionResult ProfessionalServices()
        {
            //if (TempData["openPopup"] != null)
            //    ViewBag.openPopup = TempData["openPopup"];
            PagedList<AbstractServices> Services = new PagedList<AbstractServices>();
            int decryptedId = ProjectSession.SetupUserID;
            if (decryptedId > 0)
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 100000;
                Services = this.abstractServices.RegistrationGroupMaster_All(pageParam, "", "", "Professional Registration Groups");
                ViewBag.ProviderData = abstractProviderServices.Provider_ById(decryptedId).Item;
                if (ViewBag.ProviderData.RegistrationGroupId != null)
                {
                    ViewBag.RegaArray = ViewBag.ProviderData.RegistrationGroupId.Split(',');
                }
                else
                {
                    ViewBag.RegaArray = "";
                }
            }
            return View(Services);
        }
        public ActionResult HomeServices()
        {
            //if (TempData["openPopup"] != null)
            //    ViewBag.openPopup = TempData["openPopup"];
            PagedList<AbstractServices> Services = new PagedList<AbstractServices>();
            int decryptedId = ProjectSession.SetupUserID;
            if (decryptedId > 0)
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 100000;
                Services = this.abstractServices.RegistrationGroupMaster_All(pageParam, "", "", "Home and Vehicle Modification registration Groups");
                ViewBag.ProviderData = abstractProviderServices.Provider_ById(decryptedId).Item;
                if (ViewBag.ProviderData.RegistrationGroupId != null)
                {
                    ViewBag.RegaArray = ViewBag.ProviderData.RegistrationGroupId.Split(',');
                }
                else
                {
                    ViewBag.RegaArray = "";
                }
            }
            return View(Services);
        }
        public ActionResult AssistiveServices()
        {
            //if (TempData["openPopup"] != null)
            //    ViewBag.openPopup = TempData["openPopup"];

            PagedList<AbstractServices> Services = new PagedList<AbstractServices>();
            int decryptedId = ProjectSession.SetupUserID;
            if (decryptedId > 0)
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 100000;
                Services = this.abstractServices.RegistrationGroupMaster_All(pageParam, "", "", "Assistive Technology and Equipment Registration Groups");
                ViewBag.ProviderData = abstractProviderServices.Provider_ById(decryptedId).Item;
                if (ViewBag.ProviderData.RegistrationGroupId != null)
                {
                    ViewBag.RegaArray = ViewBag.ProviderData.RegistrationGroupId.Split(',');
                }
                else
                {
                    ViewBag.RegaArray = "";
                }
            }
            return View(Services);
        }
        public ActionResult ProviderServicesArea()
        {
            //if (TempData["openPopup"] != null)
            //    ViewBag.openPopup = TempData["openPopup"];

            AbstractProvider abstractProvider = null;
            int decryptedId = ProjectSession.SetupUserID;
            if (decryptedId > 0)
            {
                abstractProvider = abstractProviderServices.Provider_ById(decryptedId).Item;
            }
            return View(abstractProvider);
        }

        public ActionResult PDNewInviteSettings(string stremail = "")
        {
            //if (TempData["openPopup"] != null)
            //    ViewBag.openPopup = TempData["openPopup"];
            ViewBag.ErrorMSG = TempData["errorMSG"];
            AbstractProvider abstractProvider = new Provider();
            ViewBag.userId = 0;
            List<SelectListItem> items = new List<SelectListItem>();
            //ViewBag.States = BindStateDropdown();
            ViewBag.States = items;
            ViewBag.Citys = items;

            string decryptedEmail = !string.IsNullOrWhiteSpace(stremail) ? stremail : "";
            if (!string.IsNullOrWhiteSpace(decryptedEmail))
            {
                abstractProvider.Email = decryptedEmail;
            }

            int decryptedId1 = ProjectSession.SetupUserID;
            if (decryptedId1 > 0)
            {
                PageParam pageParam = new PageParam();

                abstractProvider = abstractProviderServices.Provider_ById(decryptedId1).Item;
                ViewBag.userId = decryptedId1;
                ViewBag.States = BindStateCityDropdown(0, ConvertTo.Integer(abstractProvider.PinCode), 0);
                ViewBag.Citys = BindStateCityDropdown(1, ConvertTo.Integer(abstractProvider.PinCode), abstractProvider.LookupStateId);
            }
            return View(abstractProvider);
        }
        
        public ActionResult PDInviteSettings(string stremail = "", string strid = "")
        {
            var email = stremail;
            return RedirectToAction("PDNewInviteSettings", Pages.Controllers.AccountSetup, new { Area = "", stremail = email });
            //if (TempData["openPopup"] != null)
            //    ViewBag.openPopup = TempData["openPopup"];

            //ViewBag.ErrorMSG = TempData["errorMSG"];
            //AbstractProvider abstractProvider = new Provider();
            //ViewBag.userId = 0;
            //List<SelectListItem> items = new List<SelectListItem>();
            ////ViewBag.States = BindStateDropdown();
            //ViewBag.States = items;
            //ViewBag.Citys = items;

            //string decryptedEmail = !string.IsNullOrWhiteSpace(stremail) ? stremail : "";
            //int decryptedId = !string.IsNullOrWhiteSpace(strid) ? Convert.ToInt32(strid) : 0;
            //if (!string.IsNullOrWhiteSpace(decryptedEmail))
            //{
            //    abstractProvider.Email = decryptedEmail;
            //    abstractProvider.ParentId = decryptedId;
            //    ProjectSession.ParentId = decryptedId;

            //}

            //int decryptedId1 = ProjectSession.SetupUserID;
            //if (decryptedId1 > 0)
            //{
            //    PageParam pageParam = new PageParam();

            //    abstractProvider = abstractProviderServices.Provider_ById(decryptedId1).Item;
            //    ViewBag.userId = decryptedId1;
            //    ViewBag.States = BindStateCityDropdown(0, ConvertTo.Integer(abstractProvider.PinCode), 0);
            //    ViewBag.Citys = BindStateCityDropdown(1, ConvertTo.Integer(abstractProvider.PinCode), abstractProvider.LookupStateId);
            //}
            //return View(abstractProvider);
        }
          
        public ActionResult PDInvite(string stremail = "", string strid = "")
        {

            //if (TempData["openPopup"] != null)
            //    ViewBag.openPopup = TempData["openPopup"];

            ViewBag.ErrorMSG = TempData["errorMSG"];
            AbstractProvider abstractProvider = new Provider();
            ViewBag.userId = 0;
            List<SelectListItem> items = new List<SelectListItem>();
            //ViewBag.States = BindStateDropdown();
            ViewBag.States = items;
            ViewBag.Citys = items;

            string decryptedEmail = Convert.ToString(ConvertTo.Base64Decode(stremail)); 
        
            int decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(strid));
            if (!string.IsNullOrWhiteSpace(decryptedEmail))
            {
                abstractProvider.Email = decryptedEmail;
                abstractProvider.ParentId = decryptedId;
                ProjectSession.ParentId = decryptedId;

            }

            int decryptedId1 = ProjectSession.SetupUserID;
            if (decryptedId1 > 0)
            {
                PageParam pageParam = new PageParam();

                abstractProvider = abstractProviderServices.Provider_ById(decryptedId1).Item;
                ViewBag.userId = decryptedId1;
                ViewBag.States = BindStateCityDropdown(0, ConvertTo.Integer(abstractProvider.PinCode), 0);
                ViewBag.Citys = BindStateCityDropdown(1, ConvertTo.Integer(abstractProvider.PinCode), abstractProvider.LookupStateId);
            }
            return View(abstractProvider);
        }

        public ActionResult PDSettings()
        {
            //if (TempData["openPopup"] != null)
            //    ViewBag.openPopup = TempData["openPopup"];
            ViewBag.ErrorMSG = TempData["errorMSG"];
            AbstractProvider abstractProvider = new Provider();
            ViewBag.userId = 0;
            ViewBag.States = BindStateDropdown();

            int decryptedId = ProjectSession.SetupUserID;
            if (decryptedId > 0)
            {
                PageParam pageParam = new PageParam();

                abstractProvider = abstractProviderServices.Provider_ById(decryptedId).Item;
                ViewBag.userId = decryptedId;
            }
            return View(abstractProvider);
        }


        public ActionResult PMDSettings()
        {
            //if (TempData["openPopup"] != null)
            //    ViewBag.openPopup = TempData["openPopup"];
            AbstractProvider abstractPlanManager = null;
            List<SelectListItem> items = new List<SelectListItem>();
            //ViewBag.States = BindStateDropdown();
            ViewBag.States = items;
            ViewBag.Citys = items;
            if (ProjectSession.SetupUserID > 0)
            {
                int decryptedId = ProjectSession.SetupUserID;
                if (decryptedId > 0)
                {
                    abstractPlanManager = abstractProviderServices.Provider_ById(decryptedId).Item;
                    ViewBag.ProviderData = abstractPlanManager;
                    ViewBag.LocationData = abstractProviderServices.ProviderLocation_All(decryptedId).Values;
                }
            }
            return View(abstractPlanManager);
        }

        public ActionResult UserList()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];

            AbstractProvider abstractProvider = new Provider();
            if (ProjectSession.SetupUserID > 0)
            {
                int decryptedId = ProjectSession.SetupUserID;
                if (decryptedId > 0)
                {
                    PageParam pageParam = new PageParam();
                    var model = abstractProviderServices.ChildProvider_AllByParentId(pageParam, "", decryptedId).Values;
                    if (model.Count > 0)
                    {
                        abstractProvider.Provider = model;
                    }
                }
            }
            return View(abstractProvider);
        }

        public ActionResult AddUser()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            return View();
        }

        //public ActionResult PaymentDetails()
        //{
        //    if (TempData["openPopup"] != null)
        //        ViewBag.openPopup = TempData["openPopup"];

        //    AbstractUser abstractUser = null;
        //    if (ProjectSession.SetupUserID > 0)
        //    {
        //        int decryptedId = ProjectSession.SetupUserID;
        //        if (decryptedId > 0)
        //        {
        //            abstractUser = abstractUserServices.UsersPaymentDetails_ById(decryptedId).Item;
        //        }
        //    }
        //    return View(abstractUser);
        //}

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.AddProvider)]
        public ActionResult AddProvider(Provider provider)
        {
            int Id = 0;
            try
            {
                provider.CreatedBy = ProjectSession.SetupUserID;
                //HttpClient client = new HttpClient();

                //var stringTask = client.GetStringAsync("https://maps.googleapis.com/maps/api/geocode/json?address=" + provider.AddressLine1 + "&key=AIzaSyB97EhOhMCYhK1tZIofUkpKcGW61fdgKjg");
                //var msg = stringTask;

                //var s = JsonConvert.DeserializeObject<Root>(msg.Result);
                //if (s.results.Count > 0)
                //{
                //    provider.Lat = ConvertTo.String(s.results[0].geometry.location.lat);
                //    provider.Long = ConvertTo.String(s.results[0].geometry.location.lng);
                //}
                //provider.Password = "1234567890";
                var providerdata = abstractProviderServices.Provider_ByEmail(provider.Email);
                if (providerdata.Item != null)
                {
                    provider.Id = providerdata.Item.Id;
                }
                SuccessResult<AbstractProvider> result = abstractProviderServices.Provider_Upsert(provider);
                if (result != null && result.Code == 200 && result.Item != null)
                {
                    var emailVerifiedProviderData = abstractProviderServices.Provider_EmailVerified(provider.Email);

                    Id = result.Item.Id;
                    ProjectSession.SetupUserID = result.Item.Id;
                    ProjectSession.SetupUserName = result.Item.Name;
                    //TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), result.Message);

                    return RedirectToAction(Actions.PMDSettings, Pages.Controllers.AccountSetup, new { Area = "" });

                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                    TempData["errorMSG"] = result.Message;
                }
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "");
            }
            return RedirectToAction(Actions.PDSettings, Pages.Controllers.AccountSetup, new { Area = "", UserType = ProjectSession.SetupUserType });
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.PlanManagerDetails)]
        public ActionResult PlanManagerDetails(Provider planManager)
        {
            int Id = 0;
            try
            {
                planManager.Id = ProjectSession.SetupUserID;
                //planManager.Password = "1234567890";
                //planManager.Type = 1;
                HttpClient client = new HttpClient();

                var stringTask = client.GetStringAsync("https://maps.googleapis.com/maps/api/geocode/json?address=" + planManager.AddressLine1 + ",Aus&key=AIzaSyB97EhOhMCYhK1tZIofUkpKcGW61fdgKjg");
                var msg = stringTask;

                var s = JsonConvert.DeserializeObject<Root>(msg.Result);
                if (s.results.Count > 0)
                {
                    planManager.Lat = ConvertTo.String(s.results[0].geometry.location.lat);
                    planManager.Long = ConvertTo.String(s.results[0].geometry.location.lng);
                }
                SuccessResult<AbstractProvider> result = abstractProviderServices.Provider_Upsert(planManager);
                if (result != null && result.Code == 200 && result.Item != null)
                {
                    Id = result.Item.Id;
                    ProjectSession.SetupPlanManagerID = result.Item.Id;
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), result.Message);
                    return RedirectToAction(Actions.ProviderServices, Pages.Controllers.AccountSetup, new { Area = "" });
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "");
            }
            return RedirectToAction(Actions.PMDSettings, Pages.Controllers.AccountSetup, new { Area = "" });
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.AddSubUsers)]
        public ActionResult AddSubUsers(User user)
        {
            int Id = 0;
            try
            {
                //user.ParentId = ProjectSession.SetupUserID;
                SuccessResult<AbstractUser> result = abstractUserServices.SubUser_Upsert(user);
                if (result != null && result.Code == 200 && result.Item != null)
                {
                    Id = result.Item.Id;
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), result.Message);
                    return RedirectToAction(Actions.UserList, PlannedServicesProviderPortal.Pages.Controllers.AccountSetup, new { Area = "" });
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "");
            }
            //return RedirectToAction("EditUserProfile", Pages.Controllers.User, new { Area = "", userId = ConvertTo.Base64Encode(Convert.ToString(ProjectSession.UserID)) });
            return RedirectToAction("AddUser", Pages.Controllers.AccountSetup, new { Area = "" });
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.AddSubProvider)]
        public ActionResult AddSubProvider(Provider provider)
        {
            int Id = 0;
            try
            {
                SuccessResult<AbstractProvider> result = abstractProviderServices.SubProvider_Upsert(provider);
                if (result != null && result.Code == 200 && result.Item != null)
                {
                    Id = result.Item.Id;
                    ProjectSession.SetupUserID = result.Item.Id;
                    ProjectSession.SetupUserName = result.Item.Name;
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), result.Message);
                    return RedirectToAction("ProviderInviteServices", PlannedServicesProviderPortal.Pages.Controllers.AccountSetup, new { Area = "" });
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                }
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "");
            }
            return RedirectToAction("ProviderInviteServices", Pages.Controllers.AccountSetup, new { Area = "" });
        }

        //[HttpPost]
        //[ActionName(Actions.AddProviderService)]
        //public JsonResult AddProviderService(string Ids,int Type)
        //{
        //    var result = abstractProviderServices.ProviderService_Upsert(Ids,ProjectSession.SetupUserID,Type);
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}
        [HttpPost]
        [ActionName(Actions.AddProviderService)]
        public JsonResult AddProviderService(string Ids, string PIds, string HIds, string AIds)
        {
            var result = abstractProviderServices.ProviderService_Upsert(Ids, ProjectSession.SetupUserID, 1);
            var result2 = abstractProviderServices.ProviderService_Upsert(PIds, ProjectSession.SetupUserID, 2);
            var result3 = abstractProviderServices.ProviderService_Upsert(HIds, ProjectSession.SetupUserID, 3);
            var result4 = abstractProviderServices.ProviderService_Upsert(AIds, ProjectSession.SetupUserID, 4);
            if (result4 != null && result4.Code == 200 && result4.Item != null)
            {
                if (ProjectSession.SetupUserID > 0)
                {
                    ProjectSession.UserID = ProjectSession.SetupUserID;
                    ProjectSession.LoginUserID = ProjectSession.SetupUserID;
                    ProjectSession.UserName = ProjectSession.SetupUserName;
                }
                else
                {
                    result4.Code = 400;
                }
            }
            return Json(result4, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ActionName(Actions.AddProviderServiceArea)]
        public JsonResult AddProviderServiceArea(int Type, string value = "")
        {
            var result = abstractProviderServices.ProviderServiceArea_Upsert(Type, value, ProjectSession.SetupUserID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName(Actions.DeleteProvider)]
        public ActionResult DeleteProvider(int UId)
        {
            var result = abstractProviderServices.DeleteProvider(UId, ProjectSession.SetupUserID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName(Actions.DeleteProviderLocation)]
        public ActionResult DeleteProviderLocation(int LId)
        {
            var result = abstractProviderServices.DeleteProviderLocation(LId, ProjectSession.SetupUserID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName(Actions.AddLocation)]
        public ActionResult AddLocation(int LId, int SId, string PinCode, string City)
        {
            Provider location = new Provider();
            HttpClient client = new HttpClient();

            var stringTask = client.GetStringAsync("https://maps.googleapis.com/maps/api/geocode/json?address=" + PinCode + ",Australia&key=AIzaSyB97EhOhMCYhK1tZIofUkpKcGW61fdgKjg");
            var msg = stringTask;

            var s = JsonConvert.DeserializeObject<Root>(msg.Result);
            if (s.results.Count > 0)
            {
                location.Lat = ConvertTo.String(s.results[0].geometry.location.lat);
                location.Long = ConvertTo.String(s.results[0].geometry.location.lng);
            }

            location.ProviderId = ProjectSession.SetupUserID;
            location.Id = LId;
            location.LookupStateId = SId;
            location.PinCode = PinCode;
            location.City = City;

            var result = abstractProviderServices.AddLocation(location);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName(Actions.AddProviderLocation)]
        public ActionResult AddProviderLocation(string ProviderProfileAndServices ="")
        {
            var providerdata = abstractProviderServices.Provider_ById(ProjectSession.SetupUserID).Item;
            string Email = providerdata.Email;
            providerdata.ProviderProfileAndServices = ProviderProfileAndServices;
            var model = abstractProviderServices.ProviderLocation_All(ProjectSession.SetupUserID).Values;
            SuccessResult<AbstractProvider> result2 = abstractProviderServices.Provider_Upsert(providerdata);

            for (int i = 0; i < model.Count; i++)
            {
                abstractProviderServices.ProviderLocationPostCodeMapping_Upsert(model[i].Id);
            }
            var sendGridClient = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"]);

            var sendGridMessage = new SendGridMessage();
            sendGridMessage.SetFrom(ConfigurationManager.AppSettings["SendGridFromEmail"], "My Request");
            sendGridMessage.AddTo(Email, "user test"); // user email
            //sendGridMessage.AddTo("parthdobariya09@gmail.com", "user test"); // user email
            sendGridMessage.SetTemplateId("d-a33ffd065ba54f179c1bf7faa9730eda");

            sendGridMessage.SetTemplateData(new EmailObj
            {
                UName = ProjectSession.SetupUserName,
                email = ConvertTo.Base64Encode(Convert.ToString(Email)),

            });

            var response = sendGridClient.SendEmailAsync(sendGridMessage).Result;
            SuccessResult<AbstractUser> result = new SuccessResult<AbstractUser>();
            if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
            {
                result.Code = 200;
                ProjectSession.ProviderId = ProjectSession.SetupUserID;
                ProjectSession.ProviderName = ProjectSession.SetupUserName;
            }
            else
            {
                result.Code = 400;
            }
            var modal = abstractProviderServices.Provider_Active(Email);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ActionName(Actions.VerifyEmail)]
        public JsonResult VerifyEmail(string Email)
        {
            var modal = abstractProviderServices.Provider_IsEmailExist(Email);
            SuccessResult<AbstractUser> result = new SuccessResult<AbstractUser>();
            var userdata = abstractProviderServices.Provider_ByEmail(Email);
            var isVerify = false;
            if (userdata.Item != null)
            {
                isVerify = userdata.Item.IsEmailVerified;
            }
            if (!modal && !isVerify)
            {
                var sendGridClient = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"]);

                var sendGridMessage = new SendGridMessage();
                sendGridMessage.SetFrom(ConfigurationManager.AppSettings["SendGridFromEmail"], "My Request");
                sendGridMessage.AddTo(Email, "user test"); // user email
                //sendGridMessage.AddTo("parthdobariya09@gmail.com", "user test"); // user email
                sendGridMessage.SetTemplateId("d-0bddf5d12367494f8dd47c39f83541c6");

                Random generator = new Random();
                String r = generator.Next(0, 1000000).ToString("D6");

                sendGridMessage.SetTemplateData(new EmailObj
                {
                    UName = ProjectSession.SetupUserName,
                    email = ConvertTo.Base64Encode(Convert.ToString(Email)),
                    Code = r
                });

                var response = sendGridClient.SendEmailAsync(sendGridMessage).Result;

                User user = new User();
                result.Item = user;
                result.Item.ECode = r;
                if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
                {
                    result.Code = 200;
                }
                else
                {
                    result.Code = 400;
                }
                return Json(result, JsonRequestBehavior.AllowGet);

            }
            else
            {
                result.Code = isVerify ? 401 : 403;
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [ActionName(Actions.GetState)]
        public ActionResult GetState(int Type, int PostCode, int StateId = 0)
        {
            List<ServicesRequest> items = new List<ServicesRequest>();


            if (Type == 0)
            {
                var model = abstractServices.State_AllByPostCode(PostCode);
                foreach (var item in model.Values)
                {
                    items.Add(new ServicesRequest() { Name = item.Name.ToString(), DrpId = item.Id.ToString() });
                }
            }
            else
            {
                var model = abstractServices.City_AllByPostCode(PostCode, StateId);
                foreach (var item in model.Values)
                {
                    items.Add(new ServicesRequest() { Name = item.Name.ToString(), DrpId = item.Name.ToString() });
                }
            }

            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName(Actions.LogIn)]
        public ActionResult LogIn()
        {
            var providerdata = abstractProviderServices.Provider_ById(ProjectSession.SetupUserID).Item;
            SuccessResult<AbstractProvider> userData = abstractProviderServices.Provider_Login(providerdata);
            if (userData != null && userData.Code == 200 && userData.Item != null)
            {
                Session.Clear();

                if (userData.Item.UserType == 1)
                {

                    PageParam pageParam = new PageParam();
                    pageParam.Offset = 0;
                    pageParam.Limit = 10000;
                    var parent = abstractProviderServices.CheckSubProvider(pageParam, ConvertTo.Integer(userData.Item.CreatedBy)).Values;
                    if (parent.Count == 1)
                    {
                        ProjectSession.ProviderId = userData.Item.Id;
                        ProjectSession.LoginUserName = userData.Item.Name + " " + userData.Item.LastName;
                        ProjectSession.ParentId = ConvertTo.Integer(userData.Item.CreatedBy);
                        ProjectSession.ProviderName = parent[0].Name + " " + parent[0].LastName;
                        ProjectSession.Email = parent[0].Email;
                        ProjectSession.SetupUserType = Convert.ToString(parent[0].UserType);
                    }
                    else
                    {
                        ProjectSession.ProviderId = userData.Item.Id;
                        ProjectSession.ParentId = userData.Item.Id;
                        ProjectSession.Email = userData.Item.Email;
                        ProjectSession.ProviderName = userData.Item.Name + " " + userData.Item.LastName;
                    }
                }
                else
                {
                    ProjectSession.ParentId = userData.Item.Id;
                    ProjectSession.LoginUserName = userData.Item.Name + " " + userData.Item.LastName;
                    ProjectSession.ProviderId = userData.Item.Id;
                    ProjectSession.ProviderName = userData.Item.Name + " " + userData.Item.LastName;
                    ProjectSession.Email = userData.Item.Email;
                }

                //ProjectSession.ProviderId = userData.Item.Id;
                //ProjectSession.ProviderName = userData.Item.Name;
                //ProjectSession.ParentId = userData.Item.Id;
                //ProjectSession.ParentId = userData.Item.ParentId;
                HttpCookie cookie = new HttpCookie("ProviderLogin");
                cookie.Values.Add("Id", providerdata.Id.ToString());
                cookie.Values.Add("UserName", ProjectSession.ProviderName);
                cookie.Values.Add("Email", ProjectSession.Email);
                //cookie.Values.Add("UserName", providerdata.Id.ToString());
                //cookie.Values.Add("Password", providerdata.Id.ToString());
                cookie.Expires = DateTime.Now.AddHours(1);

                Response.Cookies.Add(cookie);
                //return RedirectToAction(Actions.Index, Pages.Controllers.Dashboard);
            }
            else
            {
                ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), userData.Message);
            }
            return Json(userData, JsonRequestBehavior.AllowGet);
        }

        public IList<SelectListItem> BindStateDropdown()
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 100000;
            var model = abstractLookupStateServices.LookupState_All(pageParam, "").Values;

            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in model)
            {
                items.Add(new SelectListItem() { Text = item.Name.ToString(), Value = item.Id.ToString() });
            }

            return items;
        }
        public IList<SelectListItem> BindStateCityDropdown(int Type, int PostCode, int StateId = 0)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            if (Type == 0)
            {
                var model = abstractServices.State_AllByPostCode(PostCode);
                foreach (var item in model.Values)
                {
                    items.Add(new SelectListItem() { Text = item.Name.ToString(), Value = item.Id.ToString() });
                }
            }
            else
            {
                var model = abstractServices.City_AllByPostCode(PostCode, StateId);
                foreach (var item in model.Values)
                {
                    items.Add(new SelectListItem() { Text = item.Name.ToString(), Value = item.Name.ToString() });
                }
            }

            return items;
        }
        private class EmailObj
        {
            [JsonProperty("username")]
            public string username { get; set; }

            [JsonProperty("email")]
            public string email { get; set; }

            [JsonProperty("UName")]
            public string UName { get; set; }

            [JsonProperty("PName")]
            public string PName { get; set; }

            [JsonProperty("password")]
            public string password { get; set; }

            [JsonProperty("Code")]
            public string Code { get; set; }
        }

    }
}