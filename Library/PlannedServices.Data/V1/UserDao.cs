﻿using Dapper;
using PlannedServices.Common;
using PlannedServices.Common.Paging;
using PlannedServices.Data.Contract;
using PlannedServices.Entities.Contract;
using PlannedServices.Entities.V1;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlannedServices.Data.V1
{
    public class UserDao : AbstractUserDao
    {
        public override SuccessResult<AbstractUser> User_Upsert(AbstractUser abstractUser)
        {
            SuccessResult<AbstractUser> Users = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractUser.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@FirstName", abstractUser.FirstName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LastName", abstractUser.LastName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Email", abstractUser.Email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Password", abstractUser.Password, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@MobileNo", abstractUser.MobileNo, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PhoneNumber", abstractUser.PhoneNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DOB", abstractUser.DOB, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Gender", abstractUser.Gender, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AddressLine1", abstractUser.AddressLine1, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AddressLine2", abstractUser.AddressLine2, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LookupCountryId", abstractUser.LookupCountryId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@LookupStateId", abstractUser.LookupStateId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@LookupCityId", abstractUser.LookupCityId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@PinCode", abstractUser.PinCode, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UserProfileImage", abstractUser.UserProfileImage, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@NDISReferenceNumber", abstractUser.NDISReferenceNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractUser.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractUser.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@lat", abstractUser.lat, dbType: DbType.Double, direction: ParameterDirection.Input);
            param.Add("@lng", abstractUser.lng, dbType: DbType.Double, direction: ParameterDirection.Input);
            param.Add("@Surburb", abstractUser.City, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ManagerType", abstractUser.ManagerType, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Age", abstractUser.Age, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@NomineeEmail", abstractUser.NomineeEmail, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@NomineeFirstName", abstractUser.NomineeFirstName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@NomineeLastName", abstractUser.NomineeLastName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@NomineeTitle", abstractUser.NomineeTitle, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@NomineeMobileNo", abstractUser.NomineeMobileNo, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsUserMultiple", abstractUser.IsUserMultiple, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UserType", abstractUser.UserType, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@IsPlan", abstractUser.IsPlan, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_Upsert, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractUser>>().SingleOrDefault();
                Users.Item = task.Read<User>().SingleOrDefault();
            }

            return Users;
        }
        public override SuccessResult<AbstractUser> ManagerUsers_Upsert(AbstractUser abstractUser)
        {
            SuccessResult<AbstractUser> Users = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractUser.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UserId", abstractUser.UserId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@FirstName", abstractUser.FirstName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LastName", abstractUser.LastName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DOB", abstractUser.DOB, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LookupStateId", abstractUser.LookupStateId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@PinCode", abstractUser.PinCode, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractUser.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractUser.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@lat", abstractUser.lat, dbType: DbType.Double, direction: ParameterDirection.Input);
            param.Add("@lng", abstractUser.lng, dbType: DbType.Double, direction: ParameterDirection.Input);
            param.Add("@City", abstractUser.City, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Age", abstractUser.Age, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@NomineeEmail", abstractUser.NomineeEmail, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@NomineeFirstName", abstractUser.NomineeFirstName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@NomineeLastName", abstractUser.NomineeLastName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@NomineeTitle", abstractUser.NomineeTitle, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@NomineeMobileNo", abstractUser.NomineeMobileNo, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsUserMultiple", abstractUser.IsUserMultiple, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ManagerUsers_Upsert, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractUser>>().SingleOrDefault();
                Users.Item = task.Read<User>().SingleOrDefault();
            }

            return Users;
        }
        public override SuccessResult<AbstractPlanManager> PlanManager_Upsert(AbstractPlanManager abstractPlanManager)
        {
            SuccessResult<AbstractPlanManager> Users = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractPlanManager.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CompanyName", abstractPlanManager.CompanyName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ABN", abstractPlanManager.ABN, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Email", abstractPlanManager.Email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Password", abstractPlanManager.Password, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PhoneNumber", abstractPlanManager.PhoneNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AddressLine1", abstractPlanManager.AddressLine1, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LookupStateId", abstractPlanManager.LookupStateId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@PinCode", abstractPlanManager.PinCode, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@NDIS", abstractPlanManager.NDISReferenceNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractPlanManager.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractPlanManager.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Surburb", abstractPlanManager.Surburb, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@WebSite", abstractPlanManager.WebSite, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Type", abstractPlanManager.Type, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@lat", abstractPlanManager.lat, dbType: DbType.Double, direction: ParameterDirection.Input);
            param.Add("@lng", abstractPlanManager.lng, dbType: DbType.Double, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.PlanManager_Upsert, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractPlanManager>>().SingleOrDefault();
                Users.Item = task.Read<PlanManager>().SingleOrDefault();
            }

            return Users;
        }
         public override SuccessResult<AbstractUser> SubUser_Upsert(AbstractUser abstractUser)
        {
            SuccessResult<AbstractUser> Users = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractUser.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@ParentId", abstractUser.ParentId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@FirstName", abstractUser.FirstName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LastName", abstractUser.LastName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Email", abstractUser.Email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Password", abstractUser.Password, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Title", abstractUser.Title, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.SubUsers_Upsert, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractUser>>().SingleOrDefault();
                Users.Item = task.Read<User>().SingleOrDefault();
            }

            return Users;
        }

        public override SuccessResult<AbstractUser> UsersPaymentDetails_Upsert(AbstractUser abstractUser)
        {
            SuccessResult<AbstractUser> Users = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractUser.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UserId", abstractUser.UserId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CardNumber", abstractUser.CardNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@NameOnCard", abstractUser.NameOnCard, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ExpiryDate", abstractUser.ExpiryDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CVVCode", abstractUser.CVVCode, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AddressLine1", abstractUser.AddressLine1, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LookupStateId", abstractUser.LookupStateId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@PinCode", abstractUser.PinCode, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@City", abstractUser.City, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Email", abstractUser.EmailForInvoice, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@TransactionId", abstractUser.TransactionId, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@TransactionDate", abstractUser.TransactionDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@TransactionEndDate", abstractUser.TransactionEndDate, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UsersPaymentDetails_Upsert, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractUser>>().SingleOrDefault();
                Users.Item = task.Read<User>().SingleOrDefault();
            }

            return Users;
        }

        public override PagedList<AbstractUser> CheckSubUser(AbstractUser abstractUser)
        {
            PagedList<AbstractUser> Users = new PagedList<AbstractUser>();
            var param = new DynamicParameters();

            param.Add("@Id", abstractUser.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CheckSubUser, param, commandType: CommandType.StoredProcedure);
                Users.Values.AddRange(task.Read<User>());
                Users.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return Users;
        }

        public override PagedList<AbstractUser> ParentUsers_All(PageParam pageParam, string search, int ParentId)
        {
            PagedList<AbstractUser> Users = new PagedList<AbstractUser>();
            var param = new DynamicParameters();

            param.Add("@search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@ParentId", ParentId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ParentUsers_All, param, commandType: CommandType.StoredProcedure);
                Users.Values.AddRange(task.Read<User>());
                Users.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Users;
        }  
        public override PagedList<AbstractUser> GetUserByCreatedDate(string SDate, string EDate)
        {
            PagedList<AbstractUser> Users = new PagedList<AbstractUser>();
            var param = new DynamicParameters();

            param.Add("@SDate", SDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@EDate", EDate, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.GetUserByCreatedDate, param, commandType: CommandType.StoredProcedure);
                Users.Values.AddRange(task.Read<User>());
                Users.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Users;
        }  
        public override PagedList<AbstractUser> GetUserServiceRequestData(string SDate, string EDate)
        {
            PagedList<AbstractUser> Users = new PagedList<AbstractUser>();
            var param = new DynamicParameters();

            param.Add("@SDate", SDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@EDate", EDate, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.GetUserServiceRequestData, param, commandType: CommandType.StoredProcedure);
                Users.Values.AddRange(task.Read<User>());
                Users.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Users;
        }  
        public override PagedList<AbstractUser> UsersPaymentDetails_All()
        {
            PagedList<AbstractUser> Users = new PagedList<AbstractUser>();
            var param = new DynamicParameters();


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UsersPaymentDetails_All, param, commandType: CommandType.StoredProcedure);
                Users.Values.AddRange(task.Read<User>());
                Users.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Users;
        }   
        public override PagedList<AbstractUser> ManagerUser_byUserId(int UserId)
        {
            PagedList<AbstractUser> Users = new PagedList<AbstractUser>();
            var param = new DynamicParameters();
            param.Add("@UserId", UserId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ManagerUser_byUserId, param, commandType: CommandType.StoredProcedure);
                Users.Values.AddRange(task.Read<User>());
                Users.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Users;
        }  
        
        public override PagedList<AbstractUser> ChildUsers_AllByParentId(PageParam pageParam, string search, int ParentId)
        {
            PagedList<AbstractUser> Users = new PagedList<AbstractUser>();
            var param = new DynamicParameters();

            param.Add("@search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@ParentId", ParentId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ChildUsers_AllByParentId, param, commandType: CommandType.StoredProcedure);
                Users.Values.AddRange(task.Read<User>());
                Users.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Users;
        }
        public override PagedList<AbstractPlanManager> PlanManager_All(PageParam pageParam, string search)
        {
            PagedList<AbstractPlanManager> Users = new PagedList<AbstractPlanManager>();
            var param = new DynamicParameters();

            param.Add("@search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.PlanManager_All, param, commandType: CommandType.StoredProcedure);
                Users.Values.AddRange(task.Read<PlanManager>());
                Users.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Users;
        }
        public override SuccessResult<AbstractUser> User_ById(int Id)
        {
            SuccessResult<AbstractUser> User = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_ById, param, commandType: CommandType.StoredProcedure);
                User = task.Read<SuccessResult<AbstractUser>>().SingleOrDefault();
                User.Item = task.Read<User>().SingleOrDefault();
            }

            return User;
        }
        public override SuccessResult<AbstractUser> Users_Active(string Email)
        {
            SuccessResult<AbstractUser> User = null;
            var param = new DynamicParameters();

            param.Add("@Email", Email, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_Active, param, commandType: CommandType.StoredProcedure);
                User = task.Read<SuccessResult<AbstractUser>>().SingleOrDefault();
                User.Item = task.Read<User>().SingleOrDefault();
            }

            return User;
        }
        public override SuccessResult<AbstractUser> Users_EmailVerified(string Email)
        {
            SuccessResult<AbstractUser> User = null;
            var param = new DynamicParameters();

            param.Add("@Email", Email, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_EmailVerified, param, commandType: CommandType.StoredProcedure);
                User = task.Read<SuccessResult<AbstractUser>>().SingleOrDefault();
                User.Item = task.Read<User>().SingleOrDefault();
            }

            return User;
        }

        public override bool Users_IsEmailExist(string Email)
        {
            bool result = false;
            var param = new DynamicParameters();

            param.Add("@Email", Email, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.Users_IsEmailExist, param, commandType: CommandType.StoredProcedure);
                result = task.SingleOrDefault<bool>();
            }

            return result;
        }
        public override bool GetUser_ByEmail(string Email)
        {
            bool result = false;
            var param = new DynamicParameters();

            param.Add("@Email", Email, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.GetUser_ByEmail, param, commandType: CommandType.StoredProcedure);
                result = task.SingleOrDefault<bool>();
            }

            return result;
        }
      
        public override SuccessResult<AbstractUser> User_ByEmail(string Email, string Password, int Type = 0)
        {
            SuccessResult<AbstractUser> User = null;
            var param = new DynamicParameters();

            param.Add("@Email", Email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Password", Password, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Type", Type, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_ByEmail, param, commandType: CommandType.StoredProcedure);
                User = task.Read<SuccessResult<AbstractUser>>().SingleOrDefault();
                User.Item = task.Read<User>().SingleOrDefault();
            }

            return User;
        }
        
        public override SuccessResult<AbstractUser> UsersPaymentDetails_ById(int Id)
        {
            SuccessResult<AbstractUser> User = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UsersPaymentDetails_ById, param, commandType: CommandType.StoredProcedure);
                User = task.Read<SuccessResult<AbstractUser>>().SingleOrDefault();
                User.Item = task.Read<User>().SingleOrDefault();
            }

            return User;
        }
        public override SuccessResult<AbstractPlanManager> PlanManager_ById(int Id, int type)
        {
            SuccessResult<AbstractPlanManager> planManager = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Type", type, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.PlanManager_ById, param, commandType: CommandType.StoredProcedure);
                planManager = task.Read<SuccessResult<AbstractPlanManager>>().SingleOrDefault();
                planManager.Item = task.Read<PlanManager>().SingleOrDefault();
            }

            return planManager;
        }
        public override SuccessResult<AbstractPlanManager> UpdatePlanType(int Id, int type)
        {
            SuccessResult<AbstractPlanManager> planManager = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Type", type, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UpdatePlanType, param, commandType: CommandType.StoredProcedure);
                planManager = task.Read<SuccessResult<AbstractPlanManager>>().SingleOrDefault();
                planManager.Item = task.Read<PlanManager>().SingleOrDefault();
            }

            return planManager;
        }
        public override SuccessResult<AbstractUser> UpdatePlanManager(int Id, int PlanManagerId, int Type = 0)
        {
            SuccessResult<AbstractUser> planManager = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@PlanManagerId", PlanManagerId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Type", Type, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UpdatePlanManager, param, commandType: CommandType.StoredProcedure);
                planManager = task.Read<SuccessResult<AbstractUser>>().SingleOrDefault();
                planManager.Item = task.Read<User>().SingleOrDefault();
            }

            return planManager;
        }
        public override SuccessResult<AbstractUser> User_Login(AbstractUser abstractUser)
        {
            SuccessResult<AbstractUser> User = null;
            var param = new DynamicParameters();

            param.Add("@Email", abstractUser.Email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Password", abstractUser.Password, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_Login, param, commandType: CommandType.StoredProcedure);
                User = task.Read<SuccessResult<AbstractUser>>().SingleOrDefault();
                User.Item = task.Read<User>().SingleOrDefault();
            }
            return User;
        }

        public override SuccessResult<AbstractUser> User_ChangePassword(AbstractUser abstractUser)
        {
            SuccessResult<AbstractUser> User = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractUser.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@OldPassword", abstractUser.CurrentPassword, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@NewPassword", abstractUser.NewPassword, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_ChangePassword, param, commandType: CommandType.StoredProcedure);
                User = task.Read<SuccessResult<AbstractUser>>().SingleOrDefault();
                User.Item = task.Read<User>().SingleOrDefault();
            }

            return User;
        }
        public override SuccessResult<AbstractUser> User_RestPassword(AbstractUser abstractUser)
        {
            SuccessResult<AbstractUser> User = null;
            var param = new DynamicParameters();

            param.Add("@Email", abstractUser.Email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Password", abstractUser.Password, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.User_RestPassword, param, commandType: CommandType.StoredProcedure);
                User = task.Read<SuccessResult<AbstractUser>>().SingleOrDefault();
                User.Item = task.Read<User>().SingleOrDefault();
            }

            return User;
        }

        public override bool User_Logout(int Id)
        {
            bool result = false;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.Users_Logout, param, commandType: CommandType.StoredProcedure);
                result = task.SingleOrDefault<bool>();
            }

            return result;
        }

        public override bool DeleteUser(int Id,int Deletedby)
        {
            bool result = false;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", Deletedby, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.DeleteUser, param, commandType: CommandType.StoredProcedure);
                result = task.SingleOrDefault<bool>();
            }

            return result;
        }
    }
}
