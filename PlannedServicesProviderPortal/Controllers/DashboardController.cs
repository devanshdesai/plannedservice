﻿using PlannedServices.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PlannedServicesProviderPortal.Infrastructure;
using PlannedServices.Common;
using PlannedServices.Entities.V1;
using PlannedServices.Common.Paging;

namespace PlannedServicesProviderPortal.Controllers
{
    public class DashboardController : BaseController
    {
        #region Fields
        private readonly AbstractProviderServices _abstractProviderServices;
        private readonly AbstractServicesRequestServices abstractServicesRequest;

        #endregion

        #region Ctor
        public DashboardController(AbstractProviderServices abstractProviderServices, AbstractServicesRequestServices abstractServicesRequest)
        {
            this._abstractProviderServices = abstractProviderServices;
            this.abstractServicesRequest = abstractServicesRequest;
        }
        #endregion
        // GET: Dashboard
        public ActionResult Index()
        {
            //TempData["openPopup"] = null;
            //var model = abstractServicesRequest.GetProviderChartData(ProjectSession.ProviderId);
            //ViewBag.CData = model.Values;
            //return View();

            ServicesRequest servicesRequest = new ServicesRequest();
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000000;
            servicesRequest.ServicesRequests = abstractServicesRequest.ServicesRequest_All(pageParam, "", ProjectSession.ProviderId).Values;
            if (servicesRequest.ServicesRequests.Count > 0)
            {
                servicesRequest.ServicesRequests = servicesRequest.ServicesRequests.Where(x => x.Status == 0 && x.PServiceStatus != 2).ToList();
            }
            return View(servicesRequest);

        }
        public ActionResult Help()
        {
            return View();
        }
    }
}