﻿using PlannedServices.Common;
using PlannedServices.Common.Paging;
using PlannedServices.Data.Contract;
using PlannedServices.Entities.Contract;
using PlannedServices.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlannedServices.Services.V1
{
    public class UserServices : AbstractUserServices
    {
        private AbstractUserDao abstractUserDao;

        public UserServices(AbstractUserDao abstractUserDao)
        {
            this.abstractUserDao = abstractUserDao;
        }

        public override SuccessResult<AbstractUser> User_Upsert(AbstractUser abstractUser)
        {
            return this.abstractUserDao.User_Upsert(abstractUser);
        }
        public override SuccessResult<AbstractUser> ManagerUsers_Upsert(AbstractUser abstractUser)
        {
            return this.abstractUserDao.ManagerUsers_Upsert(abstractUser);
        }
        public override SuccessResult<AbstractUser> UsersPaymentDetails_Upsert(AbstractUser abstractUser)
        {
            return this.abstractUserDao.UsersPaymentDetails_Upsert(abstractUser);
        }
        public override SuccessResult<AbstractPlanManager> PlanManager_Upsert(AbstractPlanManager abstractPlanManager)
        {
            return this.abstractUserDao.PlanManager_Upsert(abstractPlanManager);
        }
        
        public override SuccessResult<AbstractUser> SubUser_Upsert(AbstractUser abstractUser)
        {
            return this.abstractUserDao.SubUser_Upsert(abstractUser);
        }
        public override PagedList<AbstractUser> CheckSubUser(AbstractUser abstractUser)
        {
            return this.abstractUserDao.CheckSubUser(abstractUser);
        }
        public override PagedList<AbstractUser> ParentUsers_All(PageParam pageParam, string search, int ParentId)
        {
            return this.abstractUserDao.ParentUsers_All(pageParam, search, ParentId);
        }
        public override PagedList<AbstractUser> GetUserByCreatedDate(string SDate, string EDate)
        {
            return this.abstractUserDao.GetUserByCreatedDate(SDate,EDate);
        }
        public override PagedList<AbstractUser> GetUserServiceRequestData(string SDate, string EDate)
        {
            return this.abstractUserDao.GetUserServiceRequestData(SDate,EDate);
        }
        public override PagedList<AbstractUser> UsersPaymentDetails_All()
        {
            return this.abstractUserDao.UsersPaymentDetails_All();
        }
        public override PagedList<AbstractUser> ManagerUser_byUserId(int UserId)
        {
            return this.abstractUserDao.ManagerUser_byUserId(UserId);
        }
        public override PagedList<AbstractUser> ChildUsers_AllByParentId(PageParam pageParam, string search, int ParentId)
        {
            return this.abstractUserDao.ChildUsers_AllByParentId(pageParam, search, ParentId);
        }
        public override SuccessResult<AbstractUser> User_ById(int Id)
        {
            return this.abstractUserDao.User_ById(Id);
        } 
        public override SuccessResult<AbstractUser> Users_Active(string Email)
        {
            return this.abstractUserDao.Users_Active(Email);
        } 
        public override SuccessResult<AbstractUser> Users_EmailVerified(string Email)
        {
            return this.abstractUserDao.Users_EmailVerified(Email);
        } 
        public override bool Users_IsEmailExist(string Email)
        {
            return this.abstractUserDao.Users_IsEmailExist(Email);
        }  
        public override bool GetUser_ByEmail(string Email)
        {
            return this.abstractUserDao.GetUser_ByEmail(Email);
        } 
        public override SuccessResult<AbstractUser> User_ByEmail(string Email, string Password, int Type = 0)
        {
            return this.abstractUserDao.User_ByEmail(Email,Password,Type);
        } 
        public override SuccessResult<AbstractUser> UsersPaymentDetails_ById(int Id)
        {
            return this.abstractUserDao.UsersPaymentDetails_ById(Id);
        }
        public override SuccessResult<AbstractPlanManager> PlanManager_ById(int Id, int type)
        {
            return this.abstractUserDao.PlanManager_ById(Id,type);
        }  
        public override SuccessResult<AbstractPlanManager> UpdatePlanType(int Id, int type)
        {
            return this.abstractUserDao.UpdatePlanType(Id,type);
        }
        public override SuccessResult<AbstractUser> UpdatePlanManager(int Id, int PlanManagerId, int Type = 0)
        {
            return this.abstractUserDao.UpdatePlanManager(Id, PlanManagerId,Type);
        }
        public override PagedList<AbstractPlanManager> PlanManager_All(PageParam pageParam, string search)
        {
            return this.abstractUserDao.PlanManager_All(pageParam, search);
        }
        public override SuccessResult<AbstractUser> User_Login(AbstractUser abstractUser)
        {
            return this.abstractUserDao.User_Login(abstractUser);
        }

        public override SuccessResult<AbstractUser> User_ChangePassword(AbstractUser abstractUser)
        {
            return this.abstractUserDao.User_ChangePassword(abstractUser);
        }
        public override SuccessResult<AbstractUser> User_RestPassword(AbstractUser abstractUser)
        {
            return this.abstractUserDao.User_RestPassword(abstractUser);
        }

        public override bool User_Logout(int Id)
        {
            return this.abstractUserDao.User_Logout(Id);
        }

        public override bool DeleteUser(int id, int Deletedby)
        {
            return this.abstractUserDao.DeleteUser(id,Deletedby);
        }
    }
}
