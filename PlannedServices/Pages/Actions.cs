﻿namespace PlannedServices.Pages
{
    public class Actions
    {
        public const string ServiceManager = "ServiceManager";
        public const string Index = "Index";


        #region Authentication
        public const string SignIn = "SignIn";
        public const string SignUp = "SignUp";
        public const string SignUpPlan = "SignUpPlan";
        public const string SignUpSuccess = "SignUpSuccess";
        public const string RecoverPassword = "RecoverPassword";
        #endregion
    }
}