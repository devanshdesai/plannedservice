﻿using PlannedServices.Common;
using PlannedServices.Entities.Contract;
using PlannedServices.Entities.V1;
using PlannedServices.Pages;
using PlannedServices.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static PlannedServices.Infrastructure.Enums;
namespace PlannedServices.Controllers
{
    public class AuthenticationController : Controller
    {
        private readonly AbstractUserServices userService;        
        public AuthenticationController(AbstractUserServices _userService)
        {
            userService = _userService;
        }
        [HttpGet]
        public ActionResult SignUp()
        {
            //string EncodedID = Request.QueryString["id"];
            //string id = Base64Decode(EncodedID);
            //if (id != null || id != "")
            //{
            //    // find the user and
            //}
            //else
            //{
            //    //error msg
            //}
            return View();
        }


        [HttpPost]
        public ActionResult SignUp(User model)
        {
            string fullName = model.FirstName;
            var names = fullName.Split(' ');
            model.FirstName = names[0];
            model.LastName = names[1];

            SuccessResult<AbstractUser> result = userService.User_Upsert(model);
            if (result != null && result.Code == 200 && result.Item != null)
            {
                Session.Clear();
                ProjectSession.UserID = result.Item.Id;
                ProjectSession.UserName = result.Item.FirstName + " " + result.Item.LastName;
                ProjectSession.Email = result.Item.Email;


                HttpCookie cookie = new HttpCookie("UserLogin");
                cookie.Values.Add("Id", result.Item.Id.ToString());
                cookie.Values.Add("UserName", ProjectSession.UserName);
                cookie.Values.Add("Email", ProjectSession.Email);
                cookie.Expires = DateTime.Now.AddHours(1);
                Response.Cookies.Add(cookie);
                return RedirectToAction(Actions.SignUpPlan, Pages.Controllers.Authentication);
            }
            else
            {
                ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
            }
            return View();
        }
        [HttpGet]
        public ActionResult SignUpPlan()
        {
            return View();
        }


        [HttpPost]
        public ActionResult SignUpPlan(string email, string password)
        {
            return View();
        }
        [HttpGet]
        public ActionResult SignUpSuccess()
        {
            return View();
        }


        [HttpPost]
        public ActionResult SignUpSuccess(string email, string password)
        {
            return View();
        }

        [HttpGet]
        [ActionName(Actions.SignIn)]
        public ActionResult SignIn()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.SignIn)]
        public ActionResult SignIn(User model)
        {
            SuccessResult<AbstractUser> result = userService.User_Login(model);
            if (result != null && result.Code == 200 && result.Item != null)
            {
                Session.Clear();
                ProjectSession.UserID = result.Item.Id;
                ProjectSession.UserName = result.Item.FirstName + " " + result.Item.LastName;
                ProjectSession.Email = result.Item.Email;


                HttpCookie cookie = new HttpCookie("UserLogin");
                cookie.Values.Add("Id", result.Item.Id.ToString());
                cookie.Values.Add("UserName", ProjectSession.UserName);
                cookie.Values.Add("Email", ProjectSession.Email);
                cookie.Expires = DateTime.Now.AddHours(1);
                Response.Cookies.Add(cookie);
                return RedirectToAction(Actions.Index, Pages.Controllers.Dashboard);
            }
            else
            {
                ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
            }
            return View();
        }
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}